<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnSalessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('on_sales', function (Blueprint $table) {
            $table->integer('id_product')->unsigned()->index();
            $table->integer('id_sale')->unsigned()->index();
            
            $table->double('price');
            $table->date('starting_date');
            $table->date('ending_date');
            
            $table->primary(['id_product', 'id_sale']);
            
            $table->foreign('id_product')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('id_sale')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('on_sales');
    }
}
