<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
        	$table->integer('id_product')->unsigned()->index();
        	$table->integer('id_warehouse')->unsigned()->index();
        	
        	$table->integer('num_of_items');
        	$table->integer('sold_items');
        	$table->date('adding_date');
        	
        	$table->foreign('id_product')->references('id')->on('products')->onDelete('cascade');
        	$table->foreign('id_warehouse')->references('id')->on('warehouses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
