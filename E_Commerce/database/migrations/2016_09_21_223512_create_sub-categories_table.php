<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_categories', function (Blueprint $table) {
        	$table->integer('id_category')->unsigned()->index();
        	$table->integer('id_subcategory')->unsigned()->index();
        	
        	$table->integer('position');
        	
        	$table->primary(['id_category', 'id_subcategory']);
        	
        	$table->foreign('id_category')->references('id')->on('categories')->onDelete('cascade');
        	$table->foreign('id_subcategory')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_categories');
    }
}
