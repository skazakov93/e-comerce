<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description', 9999)->nullable();
            $table->string('specifications', 9999)->nullable();
            $table->double('price');
            $table->double('num_of_visits');
            $table->integer('recomended')->nullable();
            $table->string('slug');
            
            $table->integer('id_brand')->unsigned()->index();
            $table->integer('id_category')->unsigned()->index();
            
            $table->timestamps();
            
            $table->foreign('id_brand')->references('id')->on('brands')->onDelete('cascade');
            $table->foreign('id_category')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
