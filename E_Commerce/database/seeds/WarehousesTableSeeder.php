﻿<?php

use Illuminate\Database\Seeder;
use App\Modules\Warehouses\Warehouse;

class WarehousesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//1
        Warehouse::create([
	        'name' => 'Магацин 1',
	        'address' => 'Мирче Ацев 123',
	        'phone' => '075234356',
        ]);
    }
}
