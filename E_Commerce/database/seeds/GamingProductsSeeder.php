﻿<?php

use Illuminate\Database\Seeder;
use App\Modules\Products\Product;
use App\Modules\ProductImages\ProductImage;
use App\Modules\Stocks\Stock;

class GamingProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//GamePads
  	
    	////////////
        $product = Product::create([
	        'name' => "Game Pad Logitech F710 Wireless",
	        'description' => "A wireless, familiar-feeling gamepad with broad game support and dual vibration feedback lets you feel every hit, crash and explosion in your PC games.",
	        'specifications' => "XInput mode:
				-A, B, X, Y buttons
				-LB, RB buttons
				-Left and right analog triggers
				-Start and Back buttons
				-Two clickable analog mini-joysticks
				-8-way D-pad
				-Home button
				-Sports mode
				-Vibration on/off button
				
				DirectInput mode:
				-10 programmable buttons*
				-Programmable left and right triggers*
				-Two programmable analog mini-joysticks*
				-8-way programmable D-pad*
				-Sports mode
				-Vibration on/off button
				
				Package Contents
				-Gamepad
				-Plug-and-Forget Nano-receiver
				-2 AA batteries
				-Range-extending receiver cable
				-Software CD
				-User documentation",
	        'price' => "3240",
	        'num_of_visits' => 0,
	        'recomended' => 1,
	        'id_brand' => 1,
	        'id_category' => 35,
        ]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 4,
	        'sold_items' => 0,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\f710.jpg.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////////
        $product = Product::create([
	        'name' => "Wireless Controller for XBox ONE Black",
	        'description' => "Features
				The greatest gamepad – now even better
				The Xbox One Wireless Controller features over 40 improvements to the award-winning Xbox 360 Wireless Controller.
				More immersive
				Feel the action like never before with Impulse Triggers. New vibration motors in the triggers provide precise fingertip feedback bringing weapons, crashes, and jolts to life for a whole new level of gaming realism.*
				New expansion port with high speed data transfer enables clearer chat audio when using a compatible headset.**
				More precise
				Newly designed D-pad is responsive to both sweeping and directional movements.
				Thumbstick improvements enable better grip and accuracy.
				Trigger and bumpers are designed for quicker access.
				More comfortable
				Grips and contours have been designed to fit a wider range of hand sizes comfortably.
				Batteries fit inside the controller body, allowing your hands more room to naturally grip the controller.",
	        'specifications' => "Contents: Wireless Controller and AA Batteries (2).
				Up to 30ft wireless range.
				Connect up to 8 Wireless Controllers at once to your console.
				Menu and View buttons for easy navigation.
				Seamless profile and controller pairing. Infrared LEDs in the controller can be sensed by the Kinect sensor.
				Expansion port for add-on devices like the Chat Headset.
				Compatible with Xbox One Play and Charge Kit and Xbox One Chat Headset.",
	        'price' => "3990",
	        'num_of_visits' => 0,
	        'recomended' => 0,
	        'id_brand' => 2,
	        'id_category' => 35,
        ]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 16,
	        'sold_items' => 0,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\xboxonecontroler.jpg.jpg',
	        'id_product' => $product->id,
        ]);
        
        /////////////////////
        $product = Product::create([
	        'name' => "Game Pad Stratus XL for Windows + Android Gaming Bluetooth Wireless Black",
	        'description' => "Play over 2000 of your favorite full controller supported games through your Steam account with the new Stratus XL Wireless Gaming Controller for Windows and Android™.

				* Features
				GREATNESS IN THE PALMS OF YOUR HANDS
				INSTA-WIN RIGHT OUT OF THE BOX
				STEAM ENABLED
				THE ONLY ITEM YOU'LL NEED IN YOUR INVENTORY
				
				DESIGNED AROUND THE BEST CONTROLLERS ON THE MARKET
				COMFORT IS KING
				THIS IS THE CONTROLLER YOU'RE LOOKING FOR
				NO STRINGS ATTACHED!
				JOYSTICKS, HOME, AND SHOULDERS BUTTONS, OH MY!
				KNOWING IS HALF THE BATTLE
				UNLIMITED POWER!...ALMOST.
				
				TAKE CONTROL OVER THE BATTLEFIELD
				ACTION-PACKED CUSTOMIZATION
				FLICK SHOTS AND NO SCOPES",
	        'specifications' => "DESIGN
				Connector Type: Bluetooth
				Battery Type: 2 AA, Included
				Clickable Joysticks
				Battery Life: 40+ Hours
				LED Display Indicators: 4
				Weight: 288 g, 0.635 lbs
				Height: 115 mm, 4.528 in
				Width: 150 mm, 5.906 in",
	        'price' => "2790",
	        'num_of_visits' => 5,
	        'recomended' => 1,
	        'id_brand' => 3,
	        'id_category' => 35,
        ]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 7,
	        'sold_items' => 0,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\69050_1.jpg',
	        'id_product' => $product->id,
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\69050_2.jpg',
	        'id_product' => $product->id,
        ]);
        
        /////////////////////
        $product = Product::create([
	        'name' => "Game Pad Sony DualShock Wireless PS3",
	        'description' => "- Color: Black
				- Pressure sensors that rumble with each action making every impact feel like you're right in the game
				- Sixaxis highly sensitive motion technology senses your every move
				- Features Bluetooth technology for wireless game play
				- The PlayStation 3 system can support up to seven wireless controllers at one time
				- Can be charged at any time through the PlayStation 3 system using the controller's USB cable",
	        'price' => "2990",
	        'num_of_visits' => 0,
	        'recomended' => 0,
	        'id_brand' => 4,
	        'id_category' => 35,
        ]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 3,
	        'sold_items' => 0,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\dualshockps3_1.jpg',
	        'id_product' => $product->id,
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\dualshockps3_2.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "Game Pad Logitech F310 Dark Blue",
        	'specifications' => "XInput mode:
				A, B, X, Y buttons 
				LB, RB buttons 
				Left and right analog triggers 
				Start and Back buttons 
				Two clickable analog mini-joysticks 
				8-way D-pad 
				Home button 
				Sports mode 
				
				DirectInput mode:
				10 programmable buttons* 
				Programmable left and right triggers* 
				Two programmable analog mini-joysticks* 
				8-way programmable D-pad* 
				Sports mode
				
				System Requirements:
				Windows® XP, Windows Vista® or Windows® 7 
				USB port 
				CD-ROM drive 
				
				Package Contents:
				Gamepad 
				Software CD 
				User documentation",
        	'price' => "1840",
        	'num_of_visits' => 0,
        	'recomended' => 0,
        	'id_brand' => 1,
        	'id_category' => 35,
        ]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 4,
	        'sold_items' => 0,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\940000111.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "Game Pad Omega Astero XBOX 360 USB Black",
        	'description' => "Omega Astero wired Gamepad for XBOX 360! 
				Play any kind of game with this versatile controller
				Feel the precision of two analog joysticks with digital buttons that have smooth 360-degree action
				Take full control with the familiar design and layout of 14 programmable buttons, which includes four triggers and digital buttons under the analog joysticks.
				Be prepared for whatever comes your way with the trusty 8-way d-pad
				Play longer thanks to the comfortable shape and grip
				nce
				
				1. Wired controller for Xbox 360 
				2. USB 2.0 interface
				3. 14 buttons
				4. 2 high precision
				5. Force vibration system
				6 Compatible with: PS2/Win98/2000/ME/XP/Win7/Win8 system",
        	'price' => "1290",
        	'num_of_visits' => 34,
        	'recomended' => 0,
        	'id_brand' => 5,
        	'id_category' => 35,
        ]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 10,
	        'sold_items' => 0,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\ogpxbwir.jpg',
	        'id_product' => $product->id,
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\ogpxbwir-2.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "Game Pad Wireless JPDST04W Dual Vibration for PC /PS2/PS3 Rechargable",
        	'description' => "-2.4 GHz Wireless dual vibration gamepad
				-PS2-style action buttons and 4-way D-pad
				-4 programmable buttons
				-Built-in Lithium battery, rechargeable via USB
				-Play up to 30 hours (with vibration) or 100 hours (without vibration) on one battery charge
				-Rubber grip for long comfortable gaming
				-Works with both PC and PlayStation 2",
        	'specifications' => "Interface: USB 2.0 / PS2
				-Dimensions: 160 mm x 110 mm x 70 mm (gamepad), 65 mm x 50 mm x 10 mm (receiver)
				-Operating distance: up to 7 m
				-Charging time: 3 hrs
				-Net weight gamepad: 210 g
				-USB cable length: 105 cm
				
				System requirements
				-Windows XP/Vista/Windows 7 and a free USB port
				-PlayStation 2 or 3 console",
        	'price' => "990",
        	'num_of_visits' => 54,
        	'recomended' => 0,
        	'id_brand' => 6,
        	'id_category' => 35,
        ]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 2,
	        'sold_items' => 0,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\jpdst04w.jpg',
	        'id_product' => $product->id,
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\jpdst04w-2.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "Game Pad JPDST03 Dual Set, Dual Vibration, Force Feedback, 2 on 1 USB Port",
        	'description' => "Double vibration gamepads

				FOR TRUE GAMERS
				2 gamepads on 1 USB port: perfect for multi-player games
				Force feedback for realistic gaming experience
				Ergonomic design for hours of comfortable gaming
				
				Features
				-Set of 2 USB vibration gamepads for PC
				-4-way D-pad and 2 analog joysticks
				-8 programmable buttons
				-Turbo function
				-Rubber grip for long comfortable gaming
				-Works with all PC games that support gamepad input",
        	'specifications' => "-Interface: USB 2.0
				-Dimensions: 155 mm x 110 mm x 60 mm
				-Net weight: 0.43 kg
				-Cable length: 148 cm (gamepad to USB plug), 85 cm (between gamepads)",
        	'price' => "650",
        	'num_of_visits' => 7,
        	'recomended' => 0,
        	'id_brand' => 6,
        	'id_category' => 35,
        ]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 14,
	        'sold_items' => 0,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\jpdst03.jpg',
	        'id_product' => $product->id,
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\jpdst03-2.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "Game Pad Omega Warrior 3in1 PS3/PS2/PC USB",
        	'description' => "Omega Warrior Game Pad USB 3 in 1 for PC, PS2 and PS3! 
				Play any kind of game with this versatile controller
				Feel the precision of two analog joysticks with digital buttons that have smooth 360-degree action
				Take full control with the familiar design and layout of 12 programmable buttons, which includes four triggers and digital buttons under the analog joysticks.
				Be prepared for whatever comes your way with the trusty 8-way d-pad
				Play longer thanks to the comfortable shape and grip
				Start playing right away with plug-and-play convenience
				1.Compatible with: PS2/PS3/Win98/2000/ME/XP/Win7/Win8 system 2. Wired controller for: PS2/PS3/PC 3. USB 2.0 interface 4. 12 bittons 5. 2 high precision anologue sticks, 360 degree control 6. Force vibration system",
        	'price' => "650",
        	'num_of_visits' => 0,
        	'recomended' => 0,
        	'id_brand' => 5,
        	'id_category' => 35,
       	]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 4,
	        'sold_items' => 4,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\ogp3w1usb.jpg',
	        'id_product' => $product->id,
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\ogp3w1usb-2.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "Game Pad Gembird JPDST02 Dual Set, Vibration, 2 on 1 USB Port",
        	'description' => "-Set of 2 USB vibration gamepads for PC
				-4-way D-pad and 2 analog joysticks
				-10 programmable buttons
				-Works with all PC games that support gamepad input",
        	'specifications' => "-Interface: USB 2.0
				-Dimensions: 155 mm x 90 mm x 60 mm
				-Net weight single gamepad: 190 g
				-Cable length: 140 cm (gamepad to USB plug), 90 cm (between gamepads)
				
				System requirements
				-Windows XP/Vista/Windows 7
				-A free USB port",
        	'price' => "620",
        	'num_of_visits' => 0,
        	'recomended' => 0,
        	'id_brand' => 6,
        	'id_category' => 35,
       	]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 2,
	        'sold_items' => 2,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\jpd-st02.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "Game Pad Omega Phantom Pro USB",
        	'description' => "Omega PHANTOM Game Pad PC USB 
				Play any kind of game with this versatile controller
				Feel the precision of two analog joysticks with digital buttons that have smooth 360-degree action
				Take full control with the familiar design and layout of 12 programmable buttons, which includes four triggers and digital buttons under the analog joysticks.
				Be prepared for whatever comes your way with the trusty 8-way d-pad
				Play longer thanks to the comfortable shape and grip
				Start playing right away with plug-and-play convenience
				Compatible with games on PC platform, under OS Windows XP/ME/Vista/7.0.",
        	'specifications' => "Cable's length: 170cm Tech spec: 12 programmable buttons
				2 analog joystics
				4 triggers
				8-way d-pad
				Vibration Force
				USB Connection",
        	'price' => "330",
        	'num_of_visits' => 0,
        	'recomended' => 0,
        	'id_brand' => 5,
        	'id_category' => 35,
       	]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 9,
	        'sold_items' => 5,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\ogp03.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "Game Pad Natec Genesis P10 USB",
        	'description' => "GAMEPAD FOR PC GAMERS
				Natec Genesis P10 is a gamepad dedicated to gamers, whose primary gaming platform is a PC.
				
				EIGHT-WAY CONTROLLER
				Convenient and reliable, cross-shaped, operating in eight directions d-pad gives you full control over the action.
				
				GAMING DESIGN
				Thanks to ergonomic, gaming design, that provides comfortable grip, P10 Gamepad is perfect for long gaming sessions.
				
				PLUG&PLAY
				Easy installation with no additional drivers required. You just plug and play!",
        	'price' => "220",
        	'num_of_visits' => 12,
        	'recomended' => 0,
        	'id_brand' => 7,
        	'id_category' => 35,
       	]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 5,
	        'sold_items' => 1,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\genesisp10.jpg',
	        'id_product' => $product->id,
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\genesisp10-2.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "Wireless Game Pad Controller Sony DUALSHOCK 4 for PS4 Black",
        	'description' => "Precision Control
				The feel, shape, and sensitivity of the DualShock®4’s analog sticks and trigger buttons have been enhanced to offer players absolute control for all games on PlayStation®4.
				
				New ways to Play
				Revolutionary features like the touch pad, integrated light bar, and built-in speaker offer exciting new ways to experience and interact with your games and its 3.5mm audio jack offers a practical personal audio solution for gamers who want to listen to their games in private.
				
				Charge Efficiently
				The DualShock®4 Wireless Controller can be easily be recharged by plugging it into your PlayStation®4 system, even when on standby, or with any standard charger with a micro-USB port.",
        	'specifications' => "2 Point Touch Pad, Click Mechanism, Capacitive Type

				Six-axis motion sensing system (three-axis gyroscope, three-axis accelerometer)
				
				Built-in Lithium-ion Rechargeable Battery",
        	'price' => "3990",
        	'num_of_visits' => 0,
        	'recomended' => 0,
        	'id_brand' => 8,
        	'id_category' => 35,
       	]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 4,
	        'sold_items' => 3,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\130320151245261.jpg',
	        'id_product' => $product->id,
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\130320151245292.jpg',
	        'id_product' => $product->id,
        ]);
        
        //Gaming consoles
        
        ////////////////////
        $product = Product::create([
        	'name' => "Sony PlayStation 4 1TB Black",
        	'specifications' => "CPU: low power x86-64 AMD \"Jaguar\", 8 cores
				GPU: 1.84 TFLOPS, AMD next-generation Radeon™ based graphics engine
				Memory GDDR5 8GB
				Storage size 1TB  Hard disk drive.
				
				BD/DVD Drive (Read Only) 
				BD x 6 CAV
				DVD x 8 CAV
				
				Input / Output 
				Super-Speed USBx (USB 3.0) port x 2
				AUX port x 1
				
				Networking 
				Ethernet (10BASE-T, 100BASE-TX, 1000BASE-T) x 1
				IEEE 802.11 b/g/n
				Bluetooth® 2.1 (EDR)
				
				AV Output 
				HDMI out port
				DIGITAL OUTPUT (OPTICAL) port",
        	'price' => "24990",
        	'num_of_visits' => 0,
        	'recomended' => 0,
        	'id_brand' => 8,
        	'id_category' => 34,
       	]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 6,
	        'sold_items' => 0,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\906209831ee1816aa69240e19a8d9a96.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "XBOX 360 4GB Console MODDED with Kinect+Wireless Controller+Game Adventures",
        	'description' => "The Xbox 360 Console with Kinect. Kinect brings games and entertainment to life in extraordinary new ways?no controller required. Easy to use and instantly fun, Kinect gets everyone off the couch moving, laughing, and cheering. See a ball? Kick it. Control a HD movie with the wave of the hand. Want to join a friend in the fun? Simply jump in. Wi-Fi is built-in for easier connection to the world of entertainment on Xbox LIVE, where HD movies and TV stream in an instant. Xbox 360 is more games, entertainment, and fun.

				The Xbox 360 Console with Kinect. Kinect brings games and entertainment to life in extraordinary new ways—no controller required. Easy to use and instantly fun, Kinect gets everyone off the couch moving, laughing, and cheering. See a ball? Kick it. Control a HD movie with the wave of the hand. Want to join a friend in the fun? Simply jump in. Wi-Fi is built-in for easier connection to the world of entertainment on Xbox LIVE, where HD movies and TV stream in an instant. Xbox 360 is more games, entertainment, and fun.",
        	'specifications' => "New stylish design
				Built-in WiFi
				5 USB ports
				The new cooling system is virtually silent
				Includes Kinect Sensor
				Includes wireless controller
				Includes Kinect: Adventures!",
        	'price' => "24980",
        	'num_of_visits' => 15,
        	'recomended' => 1,
        	'id_brand' => 2,
        	'id_category' => 34,
       	]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 4,
	        'sold_items' => 0,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\130320151648311.jpg',
	        'id_product' => $product->id,
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\1303201516482722.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "XBOX ONE 1TB w/Wir. Controller and Headset",
        	'specifications' => "XBOX ONE 1TB w/Wir. Controller and Headset",
        	'price' => "22490",
        	'num_of_visits' => 0,
        	'recomended' => 0,
        	'id_brand' => 2,
        	'id_category' => 34,
       	]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 1,
	        'sold_items' => 0,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\xboxone-1.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "Sony PlayStation 4 500GB Black",
        	'description' => "Ultra-fast customized processors and 8GB of high-performance unified system memory

				Shared game experiences which enhances social spectating by enabling you to broadcast your gameplay in real-time with the SHARE button on the controller
				
				Personalized, curated content with the ability to learn about your preferences with the new PS4™ system
				
				The PS4™ system enables players to enjoy the actual gameplay while game is being downloaded in the background
				
				Remote Play on the PS4™ system fully unlocks the PlayStation®Vita system’s potential, making it the ultimate companion device",
        	'specifications' => "Main Processor	 Single-chip custom processor
				CPU	 x86-64 AMD \"Jaguar\", 8 cores
				GPU	 1.84 TFLOPS, AMD next-generation Radeon™ based graphics engine
				Memory	 GDDR5 8GB
				Storage size	 500GB Hard disk drive
				External dimensions (approx.)	 Approx.  275 × 53 × 305 mm (width × height × length) (excludes largest projection)
				Mass​	 Approx. 2.8 kg
				BD/ DVD drive (read only)	 BD × 6 CAV / DVD × 8 CAV
				Power​	 AC 100-240V, 50/60Hz; AC 110V/220-240V, 50/60 Hz
				Power consumption	 Max. 250W
				Operating Temperature​	 5ºC – 35ºC
				
				 Input / Output
				Super-Speed USB (USB 3.0) port	 2
				AUX port	 1
				
				 Networking​
				Ethernet	 Yes (10BASE-T, 100BASE-TX, 1000BASE-T) ×1
				IEEE 802.11 b/g/n	 YES
				Bluetooth® 2.1 (EDR)	 YES
				
				 AV output​
				HDMI out port	 Yes x1
				DIGITAL OUT (OPTICAL) port	 Yes x1",
        	'price' => "21990",
        	'num_of_visits' => 0,
        	'recomended' => 1,
        	'id_brand' => 8,
        	'id_category' => 34,
       	]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 9,
	        'sold_items' => 1,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\150720141645461.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "XBOX 360 4GB Console + Wir. Controller",
        	'description' => "The new console  Xbox 360 4GB  is built-in Wi-Fi, a black wireless controller, composite cable A / V (for standard definition), and attached to it is a one-month Xbox Live Gold card. With Kinect console does not require the use of a controller - not only play, but also you are the game. Xbox 360 is more games, fun and entertainment.",
        	'specifications' => "Platform	Xbox 360
				Disk capacity	4 GB
				Motion controller	Not
				Special Features	
				- Playing a CD-ROM, CD-R / RW, DVD + R / RW, DVD-R / RW, DVD-ROM, 
				- Supported video signal of 1080i, 720p (16: 9), 480p, 
				- Plays back images in JPG, 
				- video playback MPEG-4, DVD Video 
				- Built-in Dolby Digital 5.1, DTS 5.1, 
				- supported in sound formats MP3, WMA
				Wireless communication   - WiFi
				Connectors:
				USB 2.0
				HDMI
				3.5mm minijack
				RJ45
				Power: 230-240V / 50-60Hz
				Color: Black
				Dimensions
				- Width: 75 mm 
				- Height: 270 mm 
				- Depth: 264 mm
				Weight - 2.9 kg
				Other parameters 
				- Cooperation with Windows Media Center 
				- Possibility of connecting to the network Xbox Live Marketplace 
				- Upgradeable firmware console 
				- Number gamepad Facilities included: 1 pcs. 
				- Type of gamepad-wireless 
				- Number of control knobs (rods) \"2 
				- Vibration function: yes",
        	'price' => "11990",
        	'num_of_visits' => 31,
        	'recomended' => 0,
        	'id_brand' => 2,
        	'id_category' => 34,
       	]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 5,
	        'sold_items' => 4,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\l9v-00011-1.jpg',
	        'id_product' => $product->id,
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\l9v-l9v-00011.jpg',
	        'id_product' => $product->id,
        ]);
        
        //Volani
        
        ////////////////////
        $product = Product::create([
        	'name' => "Steering Wheel Logitech G29 Driving Force PS3/PS4/PC",
        	'description' => "FOR PLAYSTATION 3 AND PLAYSTATION 4
				DUAL-MOTOR FORCE FEEDBACK
				QUALITY CONSTRUCTION
				HELICAL GEARING WITH ANTI-BACKLASH
				EASY-ACCESS GAME CONTROLS
				RESPONSIVE FLOOR PEDAL UNIT
				MOUNTS SECURELY
				900° STEERING
				STEERING WHEEL STRIPE
				WORKS WITH DRIVING FORCE SHIFTER ATTACHMENT
				GAME COMPATIBILITY",
        	'specifications' => "PHYSICAL SPECIFICATIONS
				Wheel
				Height: 270 mm (10.63 in)
				Width: 260 mm (10.24 in)
				Length: 278 mm (10.94 in)
				Weight without cables: 2.25 kg (4.96 lb)
				Pedal
				Height: 167 mm (6.57 in)
				Width: 428.5 mm (16.87 in)
				Depth: 311 mm (12.24 in)
				Weight without cables: 3.1 kg (6.83 lb)
				TECHNICAL SPECIFICATIONS
				Software Support (at release): Logitech Gaming Software
				Connection Type: USB
				USB VID_PID: 046D_C24F (PC Mode)
				USB Protocol: USB 2.0
				USB Speed: Full Speed
				Indicator Lights (LED): Yes
				Wheel
				Rotation: 900 degrees lock-to-lock
				Hall-effect steering sensor
				Dual-Motor Force Feedback
				Overheat safeguard
				
				Pedals
				Nonlinear brake pedal
				Patented carpet grip system
				Textured heel grip
				Self-calibrating
				
				Materials
				Wheel spokes: Anodized aluminum
				Wheel cover Hand-stitched leather
				Steering shaft: Steel
				Shifter paddles: Brushed stainless steel
				Mounting clamps: Glass-filled nylon
				Pedal frames and arms: cold rolled steel
				Pedal faces: Brushed stainless steel
				Pedal piston sleeves: Polyoxymethylene thermoplastic (POM)",
        	'price' => "21990",
        	'num_of_visits' => 0,
        	'recomended' => 1,
        	'id_brand' => 1,
        	'id_category' => 36,
       	]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 1,
	        'sold_items' => 0,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\941000112.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "Steering Wheel Logitech G27 Racing",
        	'description' => "The Logitech G27 Racing Wheel was designed to deliver the definitive sim racing experience. Whether sliding sideways around a gravelly curve, or screaming through the streets of Monaco, the world's greatest circuits feel closer than ever.

				Performance
				-Dual-motor force feedback mechanism with helical gearing: Experience traction loss, weight shift, and road feel recreated smoothly and accurately, while enjoying the exceptionally quiet steering action.
				-RPM/shift indicator LEDs: Keep an eye on the indicator panel to know just when to shift gears for maximum torque (in supported games).
				-Sixteen programmable buttons plus D-pad: Map useful game functions to locations that make sense to you-whether on the wheel itself or on the shift module.
				-900-degree wheel rotation: Turn 2.5 times around lock-to-lock, just as you would behind the wheel of many real cars.
				-Ultra-precise optical encoding: A sensor constantly tracks the wheel, ensuring that any movement you make is reflected in-game, without dead zones or lag.
				-Carpet grip system: Retractable teeth flip down to keep the pedals from sliding around on carpeting.
				
				Realism
				-Realistic 11\" (28cm) wheel with leather-wrapped rim: Enjoy true-to-life racing experience from the start line to the checkered flag.
				-Six-speed shifter with push-down reverse gear: Quickly select exactly the right gear for the turn or the straightaway.
				-Steel gas, brake, and clutch pedals: Sturdy true-to-life pedals deliver precise throttle, braking, and shifting control for heightened realism.
				
				Durability
				-Steel ball bearings: Two sets of solid steel ball bearings support the wheel shaft, for superb performance and long-term reliability.
				-Hard points: All three components (wheel console, pedals, and shifter module) can be bolted to a table other surface for a solid racing experience.
				
				Compatibility
				-Platform and game support: Works with PC, PlayStation2, and PLAYSTATION3 racing games, including Gran Turismo 5 Prologue.",
        	'price' => "16990",
        	'num_of_visits' => 0,
        	'recomended' => 0,
        	'id_brand' => 1,
        	'id_category' => 36,
       	]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 0,
	        'sold_items' => 0,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\logitechg27.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "Steering Wheel Logitech Driving Force GT PC/PS2/PS3",
        	'specifications' => "-24-position realtime adjustment dial: Fine-tune brake bias, TCS, and damper settings on the fly for unprecedented control over your car's performance.
				-900-degree wheel rotation: Go 2.5 times around lock to lock, just as you would behind the wheel of many real cars.
				-Force feedback technology: Feel every inch of the road for maximum control and the ultimate racing experience.
				-Gas and brake pedals: Get precise throttle and brake response with true-to-life pedals.
				-Sequential stick shift: Go through the gears for the ultimate in control.
				
				System Requirements
				-Windows-based PC
				-PlayStation®2 computer entertainment System
				-PLAYSTATION®3 computer entertainment System
				
				Package Contents
				-Force feedback steering wheel
				-Gas and brake pedals
				-Power supply
				-User documentation",
        	'price' => "7990",
        	'num_of_visits' => 0,
        	'recomended' => 0,
        	'id_brand' => 1,
        	'id_category' => 36,
       	]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 3,
	        'sold_items' => 0,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\941000021.jpg',
	        'id_product' => $product->id,
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\941000021-2.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "Steering Wheel w/Force Feedback Gembird STR-FFB3",
        	'description' => "STR-FFB3 USB Force feedback steering wheel	
				This is the one of new multi-function racing wheels with force feedback. It has programmable functions and dual shock which brings the intensity of force feedback which give you realistic driving sense. Special designed for PC and works with all the software of designed for steering wheels.
				
				Features	
				-Force feedback: realistic driving sense!
				-Supports dual motor vibration
				-Programmable functions
				-Heavy immovable construction
				-Foot pedals with realistic spring braking and accelerating control
				-Rubber comfortable grips",
        	'specifications' => "-Spring pedals with acceleration and brake functions
				-USB connection
				-DirectX-compatible force feedback
				-180 degrees free rotation for steering wheel
				-Wheel diameter 10”
				-2.0 m steering wheel cable
				-1.6 m foot pedal unit cable
				-Power consumption: 24 VDC up to 0.7 A
				-Product size: pedal unit 232 x 180 x 140 mm (LxWxH), steering wheel 268 x 330 x 270 mm (LxWxH); fixing bracket 155 x 125 x 140 mm (LxWxH)
				-Total net weight: up to 2.9 kg
				
				Package Contents	
				-Force feedback wheel and pedals unit
				-Steady rest and screw
				-Power adapter
				-Driver CD
				-User’s manual",
        	'price' => "2890",
        	'num_of_visits' => 9,
        	'recomended' => 1,
        	'id_brand' => 6,
        	'id_category' => 36,
       	]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 2,
	        'sold_items' => 1,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\str-ffb3.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "Steering Wheel 4in1 ShockForce 2xVibration PS2/PC",
        	'description' => "STR-SHOCKFORCE-M multi-interface 4-in-1 racing wheel and pedals with two vibration motors supporting PlayStation 2, Nintendo GameCube, XBOX game consoles and PC (USB interface) 
 
				STR-SHOCKFORCE-M
				Multi-interface 4-in-1 racing wheel and pedals with two vibration motors supporting PlayStation 2, Nintendo GameCube, XBOX game consoles and PC (USB interface) 
				
				Features: 
				* 4 interfaces in one wheel: PlayStation 2, Nintendo GameCube, XBOX and PC (USB)!
				* Two vibration motors inside for realistic driving feelings!
				* 3 modes: digital, analogue, Negcon
				* Sturdy heavy construction
				* Gear stick and foot pedals providing brakes and accelerator functions
				* Comfortable rubber grip",
        	'specifications' => "* Steering wheel with two vibration motors and gear stick
				* Spring-loaded pedals with accelerator and brakes functions
				* Supports PlayStation 2, Nintendo GameCube, XBOX game consoles and USB interfaces
				* DirectX-compatible force feedback
				* 180 degrees steering wheel free rotation 
				* Wheel diameter 10”
				* 1.9m steering wheel cable
				* 1.5m foot pedal unit cable
				* Product size: 345x314x260mm (LxHxW)
				* Net weight: 1.8kg
				
				Package contents:
				* Steering wheel 
				* Foot pedals 
				* Driver CD
				* User’s manual
				
				PC system requirements:
				* Pentium-166 MMX CPU (depends on the game)
				* USB port
				* 32 MB of system memory
				* 40 MB free of HDD space
				* CD-ROM drive
				* Windows 98, ME, 2000 or XP
				* Direct X 7.0 or newer",
        	'price' => "2090",
        	'num_of_visits' => 0,
        	'recomended' => 0,
        	'id_brand' => 6,
        	'id_category' => 36,
       	]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 5,
	        'sold_items' => 1,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\str-shockforce-m.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "Steering Wheel USB Raceforce Dual vibration",
        	'specifications' => "-Dual vibration
				-180 degree free rotation
				-Choice of gas and brake hand paddles or foot pedals
				-Special anti-skid design
				-Soft rubber for optimal grip
				-Digital/Analog mode choice
				-10 function buttons and 4 direction HAT switch
				-USB interface",
        	'price' => "1250",
        	'num_of_visits' => 0,
        	'recomended' => 0,
        	'id_brand' => 6,
        	'id_category' => 36,
       	]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 2,
	        'sold_items' => 0,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\str-raceforce.jpg',
	        'id_product' => $product->id,
        ]);
        
        ////////////////////
        $product = Product::create([
        	'name' => "Steering Wheel USB Motion Sensor Dual Vibration",
        	'description' => "-USB dual vibration racing wheel with motion sensors
				-8 programmable action buttons and multi-directional D-pad
				-Quick calibration button
				-Works with all racing games that support steering wheel input",
        	'specifications' => "-Interface: USB 2.0
				-Dimensions: 200 mm x 200 mm x 44 mm
				-Net weight: 315 g
				-Cable length: 2.4 m
				
				System requirements
				-Windows XP/Vista/Windows7
				-A free USB port",
        	'price' => "950",
        	'num_of_visits' => 19,
        	'recomended' => 1,
        	'id_brand' => 6,
        	'id_category' => 36,
       	]);
        
        Stock::create([
	        'id_product' => $product->id,
	        'id_warehouse' => 1,
	        'num_of_items' => 3,
	        'sold_items' => 0,
	        'adding_date' => '2016-09-24',
        ]);
        
        ProductImage::create([
	        'url' => 'uploads\images\str-ms01.jpg',
	        'id_product' => $product->id,
        ]);
    }
}
