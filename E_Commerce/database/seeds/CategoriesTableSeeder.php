﻿<?php

use Illuminate\Database\Seeder;
use App\Modules\Categories\Category;
use App\Modules\SubCategories\SubCategory;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	/////////////
        $main1 = Category::create([
        	'name' => 'Лаптопи и Опрема',
        	'main_category' => '1',
        ]);
        
        $sub1 = Category::create([
        	'name' => "Лаптопи"
        ]);
        
        $sub2 = Category::create([
	        'name' => "Опрема"
	   	]);
        
        $sub3 = Category::create([
        	'name' => "Подлоги и Ладилници"
        ]);
        
        $sub4 = Category::create([
        	'name' => "Чанти и Ранци"
        ]);
        
        $sub5 = Category::create([
        	'name' => "Батерии"
        ]);
        
        SubCategory::create([
	        'id_category' => $main1->id,
	        'id_subcategory' => $sub1->id,
	        'position' => 1,
        ]);
        
        SubCategory::create([
	        'id_category' => $main1->id,
	        'id_subcategory' => $sub2->id,
	        'position' => 2,
        ]);
        
        SubCategory::create([
	        'id_category' => $main1->id,
	        'id_subcategory' => $sub3->id,
	        'position' => 3,
        ]);
        
        SubCategory::create([
	        'id_category' => $main1->id,
	        'id_subcategory' => $sub4->id,
	        'position' => 4,
        ]);
        
        SubCategory::create([
	        'id_category' => $main1->id,
	        'id_subcategory' => $sub5->id,
	        'position' => 5,
        ]);
        
        //////////////////
        $main2 = Category::create([
        	'name' => "Таблети",
        	'main_category' => '1',
        ]);
        
        $sub1 = Category::create([
        	'name' => "Таблети"
        ]);
        
        $sub2 = Category::create([
        	'name' => "Футроли и Торбици"
        ]);
        
        SubCategory::create([
	        'id_category' => $main2->id,
	        'id_subcategory' => $sub1->id,
	        'position' => 1,
        ]);
        
        SubCategory::create([
	        'id_category' => $main2->id,
	        'id_subcategory' => $sub2->id,
	        'position' => 2,
        ]);
        
        //////////////////
        $main3 = Category::create([
        	'name' => "Мобилни телефони",
        	'main_category' => '1',
        ]);
        
        $sub1 = Category::create([
        	'name' => "Паметни телефони"
        ]);
        
        $sub2 = Category::create([
        	'name' => "Футроли и фолии"
        ]);
        
        SubCategory::create([
	        'id_category' => $main3->id,
	        'id_subcategory' => $sub1->id,
	        'position' => 1,
        ]);
        
        SubCategory::create([
	        'id_category' => $main3->id,
	        'id_subcategory' => $sub2->id,
	        'position' => 2,
        ]);
        
        //////////////////
        $main4 = Category::create([
        	'name' => "Телевизори",
        	'main_category' => '1',
        ]);
        
        //////////////////
        $main5 = Category::create([
        	'name' => "Desktop компоненти",
        	'main_category' => '1',
        ]);
        
        $sub1 = Category::create([
        	'name' => "Матични плочи"
        ]);
        
        $sub2 = Category::create([
        	'name' => "Графички картички"
        ]);
        
        $sub3 = Category::create([
        	'name' => "RAM мемории"
        ]);
        
        $sub4 = Category::create([
        	'name' => "Intel процесори"
        ]);
        
        $sub5 = Category::create([
        	'name' => "AMD процесори"
        ]);
        
        $sub6 = Category::create([
        	'name' => "Куќишта"
        ]);
        
        $sub7 = Category::create([
        	'name' => "Напојувања"
        ]);
        
        $sub8 = Category::create([
        	'name' => "Вентилатори и ладилници"
        ]);
        
        SubCategory::create([
	        'id_category' => $main5->id,
	        'id_subcategory' => $sub1->id,
	        'position' => 1,
        ]);
        
        SubCategory::create([
	        'id_category' => $main5->id,
	        'id_subcategory' => $sub2->id,
	        'position' => 2,
        ]);
        
        SubCategory::create([
	        'id_category' => $main5->id,
	        'id_subcategory' => $sub3->id,
	        'position' => 3,
        ]);
        
        SubCategory::create([
	        'id_category' => $main5->id,
	        'id_subcategory' => $sub4->id,
	        'position' => 4,
        ]);
        
        SubCategory::create([
	        'id_category' => $main5->id,
	        'id_subcategory' => $sub5->id,
	        'position' => 5,
        ]);
        
        SubCategory::create([
	        'id_category' => $main5->id,
	        'id_subcategory' => $sub6->id,
	        'position' => 6,
        ]);
        
        SubCategory::create([
	        'id_category' => $main5->id,
	        'id_subcategory' => $sub7->id,
	        'position' => 7,
        ]);
        
        SubCategory::create([
	        'id_category' => $main5->id,
	        'id_subcategory' => $sub8->id,
	        'position' => 8,
        ]);
        
        /////////////////
        $main6 = Category::create([
        	'name' => "Дискови",
        	'main_category' => '1',
        ]);
        
        $sub1 = Category::create([
        	'name' => "Хард дискови"
        ]);
        
        $sub2 = Category::create([
        	'name' => "SSD дискови"
        ]);
        
        $sub3 = Category::create([
        	'name' => "Екстерни дискови"
        ]);
        
        $sub4 = Category::create([
        	'name' => "Кутии и Адаптери"
        ]);
        
        SubCategory::create([
	        'id_category' => $main6->id,
	        'id_subcategory' => $sub1->id,
	        'position' => 1,
        ]);
        
        SubCategory::create([
	        'id_category' => $main6->id,
	        'id_subcategory' => $sub2->id,
	        'position' => 2,
        ]);
        
        SubCategory::create([
	        'id_category' => $main6->id,
	        'id_subcategory' => $sub3->id,
	        'position' => 3,
        ]);
        
        SubCategory::create([
	        'id_category' => $main6->id,
	        'id_subcategory' => $sub4->id,
	        'position' => 4,
        ]);
        
        ////////////////
        $main7 = Category::create([
        	'name' => "Мултимедија",
        	'main_category' => '1',
        ]);
        
        $sub1 = Category::create([
        	'name' => "Жичани Слушалки"
        ]);
        
        $sub2 = Category::create([
        	'name' => "Безжични слушалки (Bluetooth)"
        ]);
        
        $sub3 = Category::create([
        	'name' => "Earphones"
        ]);
        
        $sub4 = Category::create([
        	'name' => "MP3 и MP4 плеери"
        ]);
        
        SubCategory::create([
	        'id_category' => $main7->id,
	        'id_subcategory' => $sub1->id,
	        'position' => 1,
        ]);
        
        SubCategory::create([
	        'id_category' => $main7->id,
	        'id_subcategory' => $sub2->id,
	        'position' => 2,
        ]);
        
        SubCategory::create([
	        'id_category' => $main7->id,
	        'id_subcategory' => $sub3->id,
	        'position' => 3,
        ]);
        
        SubCategory::create([
	        'id_category' => $main7->id,
	        'id_subcategory' => $sub4->id,
	        'position' => 4,
        ]);
        
        /////////////////
        $main8 = Category::create([
        	'name' => "Конзоли и опрема",
        	'main_category' => '1',
        ]);
        
        $sub1 = Category::create([
        	'name' => "Конзоли за играње"
        ]);
        
        $sub2 = Category::create([
        	'name' => "Gamepads"
        ]);
        
        $sub3 = Category::create([
        	'name' => "Волани"
        ]);
        
        SubCategory::create([
	        'id_category' => $main8->id,
	        'id_subcategory' => $sub1->id,
	        'position' => 1,
        ]);
        
        SubCategory::create([
	        'id_category' => $main8->id,
	        'id_subcategory' => $sub2->id,
	        'position' => 2,
        ]);
        
        SubCategory::create([
	        'id_category' => $main8->id,
	        'id_subcategory' => $sub3->id,
	        'position' => 3,
        ]);
        
        /////////////////
        $main9 = Category::create([
        	'name' => "Периферни уреди",
        	'main_category' => '1',
        ]);
        
        $sub1 = Category::create([
        	'name' => "Монитори"
        ]);
        
        $sub2 = Category::create([
        	'name' => "Тастатури"
        ]);
        
        $sub3 = Category::create([
        	'name' => "Глуфчиња"
        ]);
        
        $sub4 = Category::create([
        	'name' => "Печатачи и скенери"
        ]);
        
        SubCategory::create([
	        'id_category' => $main9->id,
	        'id_subcategory' => $sub1->id,
	        'position' => 1,
        ]);
        
        SubCategory::create([
	        'id_category' => $main9->id,
	        'id_subcategory' => $sub2->id,
	        'position' => 2,
        ]);
        
        SubCategory::create([
	        'id_category' => $main9->id,
	        'id_subcategory' => $sub3->id,
	        'position' => 3,
        ]);
        
        SubCategory::create([
	        'id_category' => $main9->id,
	        'id_subcategory' => $sub4->id,
	        'position' => 4,
        ]);
    }
}
