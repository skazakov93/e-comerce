<?php

use Illuminate\Database\Seeder;
use App\Modules\Products\Product;
use App\Modules\Stocks\Stock;
use App\Modules\ProductImages\ProductImage;

class MultimideaProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//Zicani slusalki
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Headphones Sony MDR-ZX310 Blue",
    		'specifications' => "Folding mechanism of its own that can be compact size Quick folding mechanism adopted simple, easy to carry
				Model: Dynamic Sealed Driver diameter: 30mm Playback frequency: 10 ~ 24000Hz
				Impedance: 24 Output sound pressure level (sensitivity): 98dB Plug: 3.5mm stereo mini plug
				Code: 1.2m Weight: about 125g I (except cable)",
    		'price' => "1490",
    		'num_of_visits' => 0,
    		'recomended' => 1,
    		'id_brand' => 8,
    		'id_category' => 29,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 10,
	    	'sold_items' => 5,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\09062016163150111.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Headphones Wireless MH2001 w/FM",
    		'description' => "-Black color
				-Comfortable, padded earcups
				-Five applications in one device
				-Wireless Headphone: for private listening
				-FM Radio: Scan for your favorite FM channels 
				-Compatible with PC, MP3 players, TV, CD or DVD players
				-Wireless Net Chat: Emitter comes with built-in microphone for Internet chatting
				-Wired Headphone: With the audio cable, it can be used as wired headphones
				-Wireless Monitoring: Place the Emitter near the people you want to monitor (children, elderly people, etc.) and it will pick up audio 
				-Lead-free components (RoHS compliant) ",
    		'specifications' => "Receiver (Headphone) Specifications:
				-Distortion: ≤ 2%
				-Reception Mode: FM
				-Frequency Range: 86 - 108 MHz  
				-Power Supply: Two (2) AAA batteries (not included)
				
				Emitter Specifications:
				-Modulation: FM
				-Microphone jack 
				-Built-in microphone 
				-Emission Distance: ≥ 8m  
				-Emission Frequency: 86 ± 0.56 MHz
				-Power Supply: Two (2) AAA batteries (not included)",
    		'price' => "850",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 4,
    		'id_category' => 29,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 2,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\mh2001-unit.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\mh2001-box.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Headphones w/Mic MHS-001 Glossy Black",
    		'description' => "Stereo headset with rotating microphone boom
				 Practical in-line control for volume, mute and microphone
				 Large comfortable ear cushions
				 Comfortable, adjustable headband",
    		'specifications' => "Sensitivity (at 1KHz) : 105 +/-3 dB
				Impedance : 32 Ohm +/-15%
				Speakers: 40 mm diameter, 16u mylar
				Frequency response: 20 - 20000 Hz
				Input power: up to 100 mW max
				Distortions: < 3 % at 1KHz 40mW
				Microphone: condenser omni-directional; sensitivity:-58 ± 3 dB(0 dB=1V/uBar at 1KHz), -38dB (0 dB=1V/Pa at 1KHz), 30 - 16000 Hz
				Net weight: 0.2 kg
				Cord length: 1.8 m",
    		'price' => "490",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 6,
    		'id_category' => 29,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 56,
	    	'sold_items' => 12,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\mhs-001.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Headphones w/Mic MHS-903 Compact",
    		'description' => "-Compact stereo headphones with microphone
				-Comfortable, adjustable headband
				-Soft ear cushions
				-3.5 mm stereo plug",
    		'specifications' => "-Impedance: 32 ohm
				-Frequency response: 20 - 20000 Hz
				-Sensitivity: 108 dB
				-Cord length: 1.5 m",
    		'price' => "420",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 6,
    		'id_category' => 29,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 26,
	    	'sold_items' => 6,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\mhp-903.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Headphones w/Mic MHS-401",
    		'description' => "-Standard PC headset with rotating gooseneck microphone
				-Adjustable headband and comfortable large ear cushions
				-Suitable for use with notebooks or desktop PC's
				-Practical in-line volume control",
    		'specifications' => "-Volume control
				-Driver unit: 40mm
				-Impedance: 32 Ohms ±10%
				-Frequency response: 20-20000Hz
				-Sensitivity: 105 dB ± 3 dB S.P.L at 1 kHz
				-Max. input power: 100 mW
				-Microphone: electret, omni-directional
				-Frequency response: 20-16000 Hz
				-Sensitivity: -54 dB ± 3dB
				-Connectors: stereo 3.5 mm plug x 2 pcs
				-Cord length: 1.8 m ± 0.2 m",
    		'price' => "420",
    		'num_of_visits' => 26,
    		'recomended' => 0,
    		'id_brand' => 6,
    		'id_category' => 29,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 19,
	    	'sold_items' => 10,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\mhs-401.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Headphones Omega Freestyle FH-3920 Black w/Microphone",
    		'description' => "Elegant looks, excellent sound quality and impressive quality. This is the product for demanding customers.
				Perfect solution for smartphones and tablets< br> Main advantages:
				Extra Bass System. 
				Comfortable headband. 
				Modern design. 
				High quality workmanship.",
    		'specifications' => "Speaker driver unit: 30mm
				Frequency response: 20-20KHz 
				Sensitivity: 116±3dB
				Impedance: 32Ohm ±15%
				Maximum input power : 100mw
				Connectors: mini jack 3,5mm",
    		'price' => "330",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 5,
    		'id_category' => 29,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 32,
	    	'sold_items' => 6,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\fh3920b.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Headphones w/Mic MHS-123",
    		'description' => "-Soft foam ear cushion
				-Flexible microphone holder
				-Integrated volume control
				-Compatible with any 3.5 mm audio plug device",
    		'specifications' => "-Impedance: 32 Ohm
				-Frequency response: 20 - 20000 Hz
				-Sensitivity: 103 +/- 3 dB
				-Max. input power: 60 mW
				-Cord length: 1.8 m
				-Omnidirectional microphone:
				-Frequency response: 30 – 16000 Hz
				-Impedance: > 2.2 KOhm
				-Sensitivity: -58 +/- 3 dB",
    		'price' => "230",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 6,
    		'id_category' => 29,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 18,
	    	'sold_items' => 6,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\mhs101b.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	//BezZicni slusalki
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Headphones Sony Stereo Bluetooth Headset SBH60 Black",
    		'description' => "Stereo Bluetooth® Headset SBH60

				Make and take calls Enjoy your music
				Sound satisfaction
				Versatile
				Style",
    		'specifications' => "Dimensions: Headband width: 9 mm
 
				Weight: 125 grams
				 
				Kit contents
				Stereo Bluetooth® Headset SBH60
				Micro USB Cable EC300
				3.5 mm cable, L-shaped
				Startup guide
				
				Controls
				Answer/end/reject call
				Volume Up/Down
				Play/Pause music key
				Next/Previous track keys
				Supported Bluetooth profiles
				Handsfree profile (HFP) v1.2
				Handsfree profile (HFP) v1.6
				Advanced Audio Distribution Profile (A2DP) 1.0
				Audio Video Remote Control Profile (AVRCP) v1.4 – Control and Target role
				
				Connectors
				Micro USB charger connector
				3.5 mm headphone connector for non-Bluetooth® device
				
				Battery capacity
				Standby time: up to 24 days
				Streaming audio time: up to 13 hours
				Talk time: up to 13 hours
				Battery: 225 mAh
				Acoustical – Microphone
				Speaker type: 30 mm dynamic
				Speaker frequency range: 30Hz - 18000Hz
				Electret Condenser, Omnidirectional, Effective range: 100 - 80000Hz
				Compatible
				Bluetooth® enabled smartphones, tablets and computers with support for Bluetooth® profiles listed above.
				Various wired devices",
    		'price' => "3980",
    		'num_of_visits' => 17,
    		'recomended' => 1,
    		'id_brand' => 8,
    		'id_category' => 30,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 6,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\sbh60black_1.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\sbh60black_2.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Headphones AKG K912 Wireless w/ Rechargable Stand",
    		'description' => "Enjoy the complete freedom of living wirelessly at home
				Optimised for movies, games and music
				Designed and conceived for lengthy listening sessions
				Soft, breathable cloth ear pads for long-lasting comfort
				Award-winning studio-quality from AKG",
    		'price' => "2990",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 9,
    		'id_category' => 30,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 11,
	    	'sold_items' => 6,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\akgk912-800x800.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Headphones Wireless Bluetooth w/Microphone, FM Tuner & Micro SD Slot Black",
    		'specifications' => "Black color
				On-ear style headphones
				Foldable design
				Bluetooth wireless technology
				TF card slot
				Plays WMA/MP3 audio
				Built-in FM tuner
				Hands-free calling function when paired with smartphones
				Built-in rechargeable battery
				Charges via USB
				
				Control Buttons:
				Power/Play/Pause
				Skip >>/Vol +
				Skip <",
    		'price' => "1350",
    		'num_of_visits' => 12,
    		'recomended' => 0,
    		'id_brand' => 4,
    		'id_category' => 30,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 8,
	    	'sold_items' => 3,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\071157-blk-unit.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\071157-blk-soft.jpg',
	    	'id_product' => $product->id,
    	]);	
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Bluetooth Headset GMB BHP-BER-BK Stereo Berlin Black",
    		'description' => "-Bluetooth stereo headphones with built-in microphone
				-Advanced DSP noise reduction technology
				-Up to 250 hours of standby time and 10 hours of listening time
				-Automatically mutes your music in case of incoming calls
				-Multifunction button, volume control and low battery indication
				-Voice prompt status updates (power on/off, pairing, incoming call)
				-Comfortable ear cushions and adjustable headband",
    		'specifications' => "-Interface: Bluetooth v.3.0 + EDR
				-Transmitting power: max 5 dB
				-Supported profiles: HSP, HFP, A2DP and AVRCP
				-Rechargeable 320 mAh Li-ion battery
				-Recharging time: 3 hrs
				-Operation distance: up to 10 m in the open air
				-Speaker diameter: 40 mm
				-Speaker frequency response: 20 Hz - 20000 Hz
				-Sensitivity: 93 dB
				-Impedance: 32 Ohm
				-Microphone: 360 degrees omni-directional
				-Input voltage: 5 V DC
				-Dimensions: 170 x 175 x 70 mm (L x W x D)
				-USB charging cable length: 1 m
				-Net weight: 130 g
				
				System requirements
				-Bluetooth enabled mobile phone, PDA, laptop or PC
				-Apple devices running IOS 6 or higher will show the battery status of the headset on their display. (no special app required)",
    		'price' => "1350",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 6,
    		'id_category' => 30,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 9,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\bhpberbk.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\bhpberbk-2.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
	    	'name' => "Bluetooth Headset Sony MBH20 V3.0+ EDR Black",
	    	'description' => "Mono Bluetooth® Headset MBH20
				Easy calling. 
				Media enabled.
				Multi-compatible wireless headset.
					    			Go wireless with style
				The MBH20 earpiece is designed to fit comfortably in your ear over long periods.
				Easy calling
				Downtown or on the road? Enjoy the comfort of easy handsfree calling. When a call comes in, press once to answer. The MBH20 has impressive battery power. Talk as long as you want.
				Wireless media enabled
				When you’re driving, you can use your Bluetooth® Headset MBH20 to listen to turn-by-turn navigation directions from your phone. You can also use it to listen to other media such as web radio, music and other audio.
				Downtown or on the road? Enjoy the comfort of easy handsfree calling.
				Comfort
				The Bluetooth® Headset MBH20 earpiece is designed to fit comfortably in your ear over long periods. It’s so light you hardly know it’s there until a call comes in. Go wireless. It’s easy.",
	    	'specifications' => "Dimensions - 45,5 x 17 x 8,5 mm
				Weight - 8 grams
				Controls: Multi-function key – power/answer/reject/mode shift
				Connectors - Micro USB charger connector
				Battery capacity:
				Standby time: (up to) 300 hours
				Talk time: (up to) 7 hours
				Compatible - Bluetooth enabled smartphones, tablets and computers
				Colour - Black",
	    	'price' => "1290",
	    	'num_of_visits' => 0,
	    	'recomended' => 0,
	    	'id_brand' => 8,
	    	'id_category' => 30,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 8,
	    	'sold_items' => 6,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\mbh20.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Bluetooth Headset GMB BTHS-006 classII V2.1+ EDR",
    		'description' => "Bluetooth headset: for safe hands free calls
				Compatible with most Bluetooth enabled phones, computers, tablets, etc. 
				One-button-control: all functions at the touch of a button
				Long battery life - up to 72 hours standby and 3 hours talking time
				Up to 10 meter reach",
    		'specifications' => "Standard: Bluetooth v.3.0, Class 2
				Power rating: receive sensitivity -90dBm, TX power max 4 dBm
				Frequency band: 2.42GHz—2.48 GHz ISM band
				Talk/standby time: up to 3 hours/ up to 72 hours; 90 days if not paired
				Supporting profile: headset & handsfree
				Power supply: 5 V DC up to 0.15 A, 100-240 VAC adapter, charging time up to 1.5 h
				Rechargeable 55 mAh Li-Polymer battery
				Operation temperature: -10...55 Celsius degrees
				Storing temperature: -20...60 Celsius degrees
				Weight: 6.8 g
				Dimensions: 40 x 15 x 25 mm
				Indicator: red & blue LED",
    		'price' => "590",
    		'num_of_visits' => 0,
    		'recomended' => 1,
    		'id_brand' => 6,
    		'id_category' => 30,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 4,
	    	'sold_items' => 4,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\bths006.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	//EarPhones
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Earphones AKG Y20U w/Microphone Black",
    		'description' => "ULTRA COMFORTABLE IN-EAR HEADPHONES THAT DELIVERS AKG SIGNATURE SOUND.
				Long celebrated for designing headphones for studio professionals, AKG has developed an in-ear stereo headphone for those who know what quality sounds like when they hear it and feel it – regardless of how many listening hours they’ve logged.
				Not only do the AKG Y20’s powerful 8 mm drivers deliver high qualitysound, but their tilted ear-tips and super-soft, multi-size silicone sleeves are crafted to keep attached to the ear in almost any situation – making them ideal for people who like to keep on the move. Their polycarbonate composition is both lightweight and durable, only adding to their striking outward appearance.",
    		'price' => "1790",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 9,
    		'id_category' => 31,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 3,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\88.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Earphones Micro Verbatim Active w/Microphone Red",
    		'description' => "Take calls on your smartphone with the built in microphone
				Sound isolating in-ear design
				Flat earphone cable for tangle free use
				Use with smartphones, iPods, iPads, and portable media players
				Available in red and in blue",
    		'specifications' => "Speaker Diameter (Driver): 9mm 
				Cable Length: 1.2 meters 
				Plug Type (Jack): 3.5mm gold plated
				Frequency Range: 20Hz - 20KHz 
				Impedance: 16 Ohm 
				Sensitivity: 96dB 
				Pack dimensions: 88mm x 130mm x 27mm (W x H x D)
				Product weight: 17 grams",
    		'price' => "750",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 10,
    		'id_category' => 31,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 6,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\49110.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Earphones Verbatim Sound Isolating Red w/Volume Control",
    		'description' => "Silicon ear bud provides comfort
				In-ear seal created by silicon ear buds block external noise
				Neodymium speaker driver provides superb sound quality
				3 sizes of ear buds included in the pack for the perfect fit
				Available in black and red",
    		'price' => "640",
    		'num_of_visits' => 4,
    		'recomended' => 0,
    		'id_brand' => 10,
    		'id_category' => 31,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 4,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\49114.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Earphones Freestyle Zip FH2111 w/Microphone Orange",
    		'description' => "In-ear headphones with zipper cable shaped, with the possibility of zip and zip-off. Available in 5 colors. High quality sound, enhanced bass. Compatible with mobile phones, smartphones, tablets, and other MP3/MP4 ",
    		'specifications' => "- Speaker diameter: 10mm 
				- Impedance: 16 ohm 
				- Sensitivity: 110 dB / m at 1KHz 
				- Frequency response: 20 Hz - 20K Hz 
				- Input Power: 3 mW 
				- Cable length: 1.25 m 
				- Connector: 3,5 mm 
				- Color: Black",
    		'price' => "420",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 5,
    		'id_category' => 31,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 32,
	    	'sold_items' => 16,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\fh2100o_low-res_04.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\fh2100o_low-res_01.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Earphones Trevi JS657 Sport Pink",
    		'description' => "Ergonomic Sports Headphones Trevi JS 657, comfortable and ultra lightweight!

				Ear, ideal for jogging, biking or at the gym. External noise reduced thanks to the silicone rings.",
    		'specifications' => "Cable length 1.2 m Connector 3.5 mm. and weight of only 5 grams!

				Connector Ø 3.5 mm;
				Weight: 5 gr.",
    		'price' => "390",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 11,
    		'id_category' => 31,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 6,
	    	'sold_items' => 6,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\09062016163333image_thumb_250.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Earphones Gembird MHS-EP-002 Metal w/Microphone Black & Blue",
    		'description' => "-Metal stereo earphones
				-High quality bass driven sound for optimal music experience
				-In-line microphone and volume control
				-Comfortable soft silicone ear cushions",
    		'specifications' => "-Frequency response: 50-18000 Hz
				-3.5 mm stereo audio plug
				-Cord length: 0.9 m
				-Dimensions 42x16.4x15 mm
				-Net weight: 12 g
				-Color box packing",
    		'price' => "300",
    		'num_of_visits' => 17,
    		'recomended' => 0,
    		'id_brand' => 6,
    		'id_category' => 31,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 14,
	    	'sold_items' => 3,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\mhs-ep-002.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Earphones Gembird MHS-EP-001 Metal w/Microphone Black",
    		'description' => "-Metal stereo earphones
				-High quality bass driven sound for optimal music experience
				-In-line microphone
				-Comfortable soft silicone ear cushions",
    		'specifications' => "-Frequency response: 50-18000 Hz
				-3.5 mm stereo audio plug
				-Cord length: 0.9 m
				-Dimensions 42x16.4x15 mm
				-Net weight: 12 g
				-Color box packing",
    		'price' => "280",
    		'num_of_visits' => 13,
    		'recomended' => 0,
    		'id_brand' => 6,
    		'id_category' => 31,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 8,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\mhs-ep-001.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Earphones Philips SHE1350",
    		'price' => "220",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 12,
    		'id_category' => 31,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 20,
	    	'sold_items' => 15,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\she1350.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Earphones Hantol Gum Blue",
    		'description' => "ITEM CODE: HEA18B
				BUBBLE GUM EARPHONE BLUE
				
				High quality speaker
				design providing clear sound on
				your notebook, netbook, MP3
				player",
    		'price' => "200",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 13,
    		'id_category' => 31,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 15,
	    	'sold_items' => 3,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\hea18bl.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Earphones Gembird MHP-EP-001-W Candy White",
    		'description' => "-Stereo earphones
				-High quality bass driven sound for optimal music experience
				-Comfortable soft silicone ear cushions",
    		'specifications' => "-Frequency response: 50-18000 Hz
				-3.5 mm stereo audio plug
				-Cord length: 0.9 m
				-Dimensions 42x16.4x15 mm
				-Net weight: 12 g
				-Color box packing",
    		'price' => "140",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 6,
    		'id_category' => 31,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 36,
	    	'sold_items' => 9,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\mhpep001w.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	//MP3 i MP4
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "MP3 Player 4GB SPEEDO AquaBeat Waterproof White/Gray",
    		'description' => "Speedo AquaBeat 1.0 4GB
				Waterproof Sport MP3 Player",
    		'specifications' => "Internal Memory: 4GB flash up to 2000 titles
				Waterproof: 3M
				Flotation: Yes
				Music File Formats: VMR, MP3 and WMA
				Case: Soft touch plastic /chlorinated water and sea salt resistant
				Color: White/Grey
				Earphones: In the Ear sealed with interchangeable plugs
				Maximum power: Headphones output strenght 10MW (320 Ohm)
				Signal/Noise Ratio: 90db
				Frequency Range: 20Hz ~ 20KHz
				USB Key Function: Yes
				Min/Max Temperatures: From 0 to 60 degrees Celsius
				Power: Lithium-ion Battery
				Autonomy: Up to 9 hours
				Interface: USB2.0
				Dimensions (mm) and weight: 
				* 60,96 (L) x 45,72 (H) x 20,32 (D)
				* 35 grams
				Content: Player, Headphones, Case, Manual, USB Cable, Audio Extension Cable
				Certifications: CE/FCC",
    		'price' => "2980",
    		'num_of_visits' => 8,
    		'recomended' => 1,
    		'id_brand' => 14,
    		'id_category' => 32,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 7,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\26335_1.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\26335_2.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "MP3 Player Mediacom BMX 600R for Bike w/Speaker/Flashlight",
    		'description' => "Portable Radio with MP3 player, speaker and flashlight 
				MP3 / WMA player with speaker for your bike 
				Plays MP3 / WMA files from microSD / SDHC card 
				flashlight LED integrated 
				equalizer with 7 functions",
    		'specifications' => "Technical specifications
				Main features
				digital playback standards: WMA, MP3
				Source media: Flash memory card
				Product description: MEDIACOM MP3 Bike Speaker BMX 600R - digital player - flash memory card
				product type: digital music player based on flash
				Dimensions (WxDxH): 2.5 cm x 6.5 cm x 2.1 cm
				Weight: 22 g
				Battery: Player - rechargeable - Lithium Ion
				speakers: 1 x speaker - built-in - 3 Watt
				Flash Memory Cards: microSDHC, microSD - up to 32 GB",
    		'price' => "1190",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 15,
    		'id_category' => 32,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 4,
	    	'sold_items' => 3,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\bmx-600r.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\m-bmx600r.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "MP4 Player 4GB Eclipse Touch Pro MP3/Video Player & Voice Rec. w/2.4\" LCD Touchscreen Cobalt",
    		'specifications' => "4 GB storage capacity
				2.4-inch touchscreen
				USB 2.0 interface
				Video & music player
				Digital photo viewer
				FM radio function
				Ebook reader
				Built-in Li-ion battery
				
				Supported Media:
				Audio: MP3
				Photo: JPG
				Video: AVI",	
    		'price' => "1090",
    		'num_of_visits' => 7,
    		'recomended' => 0,
    		'id_brand' => 4,
    		'id_category' => 32,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 8,
	    	'sold_items' => 2,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\eclps-tpro4g.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "MP3 Player LCD w/FM/Micro SD/Mic Silver",
    		'description' => "Small and lightweight
				Powerful and clear sound in unique design
				FM radio function
				Clip design, easy to carry
				Enjoy your music anywhere, anytime
				Micro SD card reading, support MP3 format music file
				Large capacity lithium battery to ensure longer playing time",
    		'specifications' => "Display: Mini LCD screen
				Shell material: Metal
				Audio format: MP3
				Memory: Micro SD card (Not included)
				Support FM radio
				Interface: Mini USB2.0
				Earphone jack: 3.5mm plug
				Language: English, Chinese 
				Power supply: Bulit in Lithium battery (Please Charge it <2 hours) 
				Play time: About 3-5 hours
				Product weight: 22 g
				Product size (L x W x H): 5.0 x 3.0 x 1.5 cm",
    		'price' => "570",
    		'num_of_visits' => 0,
    		'recomended' => 1,
    		'id_brand' => 4,
    		'id_category' => 32,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 23,
	    	'sold_items' => 20,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\mp3playersilver.jpg',
	    	'id_product' => $product->id,
    	]);
    }
}
