<?php

use Illuminate\Database\Seeder;
use App\Modules\Users\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
    	User::create([
        	'firstName' => 'Slavko',
        	'lastName' => 'KAZAKOV',
        	'email' => 'slavce_caki@yahoo.com',
        	'password' => Hash::make('123456'),
        	'provider' => 'local',
        	'role' => 'customer',
        	'photo_url' => '',
        	'phone_number' => '075236344',
    	]);
    	
        //2
    	User::create([
        	'firstName' => 'Slavko',
        	'lastName' => 'KAZAKOV',
        	'email' => 'slavce.kazakov@gmail.com',
        	'password' => Hash::make('123456m'),
        	'provider' => 'local',
        	'role' => 'customer',
        	'photo_url' => '',
        	'phone_number' => '075236344',
    	]);
    	
        //3
    	User::create([
        	'firstName' => 'Slavko',
        	'lastName' => 'KAZAKOV',
        	'email' => 'kazakov.slavce@gmail.com',
        	'password' => Hash::make('123456mm'),
        	'provider' => 'local',
        	'role' => 'admin',
        	'photo_url' => '',
        	'phone_number' => '075236344',
    	]);

        //4
        User::create([
            'firstName' => 'Kiro',
            'lastName' => 'Andreev',
            'email' => 'kiro.andreev@gmail.com',
            'password' => Hash::make('kiro-andreev'),
            'provider' => 'local',
            'role' => 'customer',
            'photo_url' => '',
            'phone_number' => '075236344',
        ]);

        //5
        User::create([
            'firstName' => 'Klimentina',
            'lastName' => 'Perkovo',
            'email' => 'klimentina.perkova@gmail.com',
            'password' => Hash::make('klimentinaPerkova'),
            'provider' => 'local',
            'role' => 'customer',
            'photo_url' => '',
            'phone_number' => '075236344',
        ]);

        //6
        User::create([
            'firstName' => 'Dejan',
            'lastName' => 'Filipov',
            'email' => 'filipov.dejan@gmail.com',
            'password' => Hash::make('dejanfilipovski'),
            'provider' => 'local',
            'role' => 'customer',
            'photo_url' => '',
            'phone_number' => '075236344',
        ]);

        //7
        User::create([
            'firstName' => 'Andrea',
            'lastName' => 'Nikolova',
            'email' => 'andrea.nikolova@gmail.com',
            'password' => Hash::make('andrea_nikolova'),
            'provider' => 'local',
            'role' => 'customer',
            'photo_url' => '',
            'phone_number' => '075236344',
        ]);

        //8
        User::create([
            'firstName' => 'Ana',
            'lastName' => 'Stojcevska',
            'email' => 'ana.stojcevska@gmail.com',
            'password' => Hash::make('anaStojcevska'),
            'provider' => 'local',
            'role' => 'customer',
            'photo_url' => '',
            'phone_number' => '075236344',
        ]);

        //9
        User::create([
            'firstName' => 'Pere',
            'lastName' => 'Mitev',
            'email' => 'pere_mitev@gmail.com',
            'password' => Hash::make('peremitev'),
            'provider' => 'local',
            'role' => 'customer',
            'photo_url' => '',
            'phone_number' => '075236344',
        ]);

        //10
        User::create([
            'firstName' => 'Filip',
            'lastName' => 'Kostovski',
            'email' => 'filip.kostov@gmail.com',
            'password' => Hash::make('filip-kostov'),
            'provider' => 'local',
            'role' => 'customer',
            'photo_url' => '',
            'phone_number' => '075236344',
        ]);
    }
}
