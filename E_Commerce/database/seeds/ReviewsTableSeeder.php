<?php

use Illuminate\Database\Seeder;
use App\Modules\Reviews\Review;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	/////////////////////
        Review::create([
        	'id_product' => 5,
			'id_user' => 1,
			'rating' => 3,
			'description' => 'Продуктот е со добар квалитет!!!',
        ]);

        Review::create([
        	'id_product' => 5,
			'id_user' => 4,
			'rating' => 5,
			'description' => 'За цената производот е топ',
        ]);

        Review::create([
        	'id_product' => 5,
			'id_user' => 2,
			'rating' => 4,
			'description' => 'Не ми се допаѓа пластиката!!!',
        ]);

        Review::create([
        	'id_product' => 5,
			'id_user' => 6,
			'rating' => 3,
			'description' => 'Можеле и подобро',
        ]);

        Review::create([
        	'id_product' => 5,
			'id_user' => 10,
			'rating' => 4,
			'description' => 'Топ, топ',
        ]);

        ///////////////////////
        Review::create([
        	'id_product' => 18,
			'id_user' => 4,
			'rating' => 3,
			'description' => 'За џабе фрлени пари!!',
        ]);

        Review::create([
        	'id_product' => 18,
			'id_user' => 9,
			'rating' => 2,
			'description' => 'Ова е навистина ко мала играчка за деца!!!',
        ]);

        /////////////////////
        Review::create([
        	'id_product' => 23,
			'id_user' => 4,
			'rating' => 5,
			'description' => 'Воланот е мн добар, но цената ...',
        ]);

        Review::create([
        	'id_product' => 23,
			'id_user' => 1,
			'rating' => 4,
			'description' => 'Само цената не ми се допаѓа :)',
        ]);

        Review::create([
        	'id_product' => 23,
			'id_user' => 9,
			'rating' => 4,
			'description' => 'Интересна играчка',
        ]);

        Review::create([
        	'id_product' => 23,
			'id_user' => 3,
			'rating' => 3,
			'description' => 'Се е ОДЛИЧНО, освен цената',
        ]);

        ////////////////
        Review::create([
        	'id_product' => 35,
			'id_user' => 4,
			'rating' => 5,
			'description' => 'Мн добри слушалки',
        ]);

        Review::create([
        	'id_product' => 35,
			'id_user' => 2,
			'rating' => 3,
			'description' => 'За цената се ептен добри!!!',
        ]);

        Review::create([
        	'id_product' => 35,
			'id_user' => 6,
			'rating' => 4,
			'description' => 'Добар квалитет',
        ]);

        Review::create([
        	'id_product' => 35,
			'id_user' => 8,
			'rating' => 4,
			'description' => 'Добри се',
        ]);

        Review::create([
        	'id_product' => 35,
			'id_user' => 10,
			'rating' => 4,
			'description' => 'Мене ми се супер',
        ]);

        ////////////////////
        Review::create([
        	'id_product' => 53,
			'id_user' => 4,
			'rating' => 5,
			'description' => 'Мислам дека ова е најдобриот монитор',
        ]);

        Review::create([
        	'id_product' => 53,
			'id_user' => 1,
			'rating' => 5,
			'description' => 'Најдобар монитор кој го имам користено!!',
        ]);

        Review::create([
        	'id_product' => 53,
			'id_user' => 9,
			'rating' => 4,
			'description' => 'Не ми се допаѓа рамката. Се друго е ТОП',
        ]);

        ///////////////////
        Review::create([
        	'id_product' => 59,
			'id_user' => 1,
			'rating' => 3,
			'description' => 'Црната боја не е на ниво!!',
        ]);

        Review::create([
        	'id_product' => 59,
			'id_user' => 2,
			'rating' => 4,
			'description' => 'За цената, не вреди 5 ѕвезди',
        ]);

        //////////////////
        Review::create([
        	'id_product' => 71,
			'id_user' => 1,
			'rating' => 4,
			'description' => 'Солидно глуфче, за мали пари.',
        ]);

        Review::create([
        	'id_product' => 71,
			'id_user' => 5,
			'rating' => 2,
			'description' => 'Скролот не добар.',
        ]);

        Review::create([
        	'id_product' => 71,
			'id_user' => 6,
			'rating' => 3,
			'description' => 'Немам некое посебно мислење',
        ]);

        //////////////////
        Review::create([
        	'id_product' => 83,
			'id_user' => 9,
			'rating' => 4,
			'description' => 'Бааѓи прецизно глуфче.',
        ]);

        Review::create([
        	'id_product' => 83,
			'id_user' => 4,
			'rating' => 5,
			'description' => 'За мене е прилично прецизно.',
        ]);

        Review::create([
        	'id_product' => 83,
			'id_user' => 9,
			'description' => 'Па да, прецизно е, но не е најпрецизното глуфче.',
        ]);

        Review::create([
        	'id_product' => 83,
			'id_user' => 8,
			'description' => 'Да се слагам и јас. Го имам користено кај другар, и не е баш најпрецизно, доколку го споредува со некое А4 bludy на пример.',
        ]);

        //////////////////
        Review::create([
        	'id_product' => 90,
			'id_user' => 1,
			'rating' => 5,
			'description' => 'Го користам веќе две години секојдневно, и сеуште се нема запушено.',
        ]);

        ////////////////
        Review::create([
        	'id_product' => 93,
			'id_user' => 10,
			'rating' => 4,
			'description' => 'Мн ефтино принтање, но првата страна се чека долго додека се испринта.',
        ]);

        Review::create([
        	'id_product' => 93,
			'id_user' => 7,
			'rating' => 5,
			'description' => 'Има мн ефтини тонери за него.',
        ]);

        Review::create([
        	'id_product' => 93,
			'id_user' => 10,
			'description' => 'јас ги полнам тонерите. Тебе не ти се исплати?',
        ]);

        Review::create([
        	'id_product' => 93,
			'id_user' => 7,
			'description' => 'Па не ми се допаѓа квалитетот на принтот. Со заменските добивам подобар принт.',
        ]);
    }
}
