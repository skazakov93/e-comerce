<?php

use Illuminate\Database\Seeder;
use App\Modules\OnSales\OnSale;

class ProductsOnSaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OnSale::create([
        	'id_product' => 4,
			'id_sale' => 4,
			'price' => '2700',
			'starting_date' => '2016-09-27',
			'ending_date' => '2016-10-27',
        ]);

        OnSale::create([
        	'id_product' => 11,
			'id_sale' => 1,
			'price' => '310',
			'starting_date' => '2016-09-20',
			'ending_date' => '2016-09-27',
        ]);

        OnSale::create([
        	'id_product' => 17,
			'id_sale' => 17,
			'price' => '21100',
			'starting_date' => '2016-09-27',
			'ending_date' => '2016-10-07',
        ]);

        OnSale::create([
        	'id_product' => 21,
			'id_sale' => 21,
			'price' => '7690',
			'starting_date' => '2016-09-30',
			'ending_date' => '2016-10-27',
        ]);

        OnSale::create([
        	'id_product' => 29,
			'id_sale' => 29,
			'price' => '400',
			'starting_date' => '2016-09-28',
			'ending_date' => '2016-10-13',
        ]);

        OnSale::create([
        	'id_product' => 33,
			'id_sale' => 33,
			'price' => '3650',
			'starting_date' => '2016-09-29',
			'ending_date' => '2016-10-30',
        ]);

        OnSale::create([
        	'id_product' => 41,
			'id_sale' => 41,
			'price' => '590',
			'starting_date' => '2016-09-27',
			'ending_date' => '2016-10-06',
        ]);

        OnSale::create([
        	'id_product' => 53,
			'id_sale' => 53,
			'price' => '26990',
			'starting_date' => '2016-10-01',
			'ending_date' => '2016-10-30',
        ]);

        OnSale::create([
        	'id_product' => 59,
			'id_sale' => 59,
			'price' => '7950',
			'starting_date' => '2016-09-27',
			'ending_date' => '2016-10-11',
        ]);

        OnSale::create([
        	'id_product' => 65,
			'id_sale' => 65,
			'price' => '5100',
			'starting_date' => '2016-09-29',
			'ending_date' => '2016-10-29',
        ]);

        OnSale::create([
        	'id_product' => 77,
			'id_sale' => 77,
			'price' => '1690',
			'starting_date' => '2016-09-27',
			'ending_date' => '2016-10-02',
        ]);

        OnSale::create([
        	'id_product' => 83,
			'id_sale' => 83,
			'price' => '150',
			'starting_date' => '2016-09-27',
			'ending_date' => '2016-10-20',
        ]);

        OnSale::create([
        	'id_product' => 89,
			'id_sale' => 89,
			'price' => '9990',
			'starting_date' => '2016-09-27',
			'ending_date' => '2016-10-22',
        ]);

        OnSale::create([
        	'id_product' => 95,
			'id_sale' => 95,
			'price' => '8590',
			'starting_date' => '2016-09-27',
			'ending_date' => '2016-10-05',
        ]);

        OnSale::create([
        	'id_product' => 101,
			'id_sale' => 101,
			'price' => '13550',
			'starting_date' => '2016-09-25',
			'ending_date' => '2016-10-05',
        ]);

        OnSale::create([
        	'id_product' => 105,
			'id_sale' => 105,
			'price' => '26990',
			'starting_date' => '2016-09-22',
			'ending_date' => '2016-10-12',
        ]);

        OnSale::create([
        	'id_product' => 113,
			'id_sale' => 113,
			'price' => '9190',
			'starting_date' => '2016-09-27',
			'ending_date' => '2016-10-20',
        ]);

        OnSale::create([
        	'id_product' => 119,
			'id_sale' => 119,
			'price' => '2590',
			'starting_date' => '2016-09-27',
			'ending_date' => '2016-10-22',
        ]);

        OnSale::create([
        	'id_product' => 125,
			'id_sale' => 125,
			'price' => '9590',
			'starting_date' => '2016-09-14',
			'ending_date' => '2016-10-23',
        ]);
    }
}
