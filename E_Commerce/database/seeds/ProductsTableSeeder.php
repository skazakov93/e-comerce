﻿<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(GamingProductsSeeder::class);
    	$this->call(MultimideaProductsSeeder::class);
    	$this->call(InputAndOutputProductsSeeder::class);
    	$this->call(TVProductsSeeder::class);
        $this->call(DriveProductsSeeder::class);
    }
}
