<?php

use Illuminate\Database\Seeder;
use App\Modules\Products\Product;
use App\Modules\Stocks\Stock;
use App\Modules\ProductImages\ProductImage;

class DriveProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//Hard-Diskovi

        ////////////////////
        $product = Product::create([
            'name' => "HDD 3.5\" 4TB Seagate Barracuda SATA3 5900RPM 64MB",
            'specifications' => "Disk specification:
Capacity: 4TB
4TB (ST4000DM000)
8 heads, 4 disks
Interface: SATA 6Gb/s
Spindle: 5,900 RPM
Cache: 64MB
Throughput Max: 180/MBs
Average Data Rate: 146MB/s
Average Latency: 5.16ms
  
Power:
 Typical Idle Operating 5W
 Average Operating 7.5W
 
 Environmental:
 Operating Temperature 0° to 60°C
 Non-operating Temperature -40° to 70°C
 
Dimensions (LxWxH): 146.99mm x 101.60mm x 26.11mm
 Weight: 1.345lb (610g)",
            'price' => "8190",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 28,
            'id_category' => 24,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 48,
            'sold_items' => 12,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\st2000dl003.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "HDD 3.5\" 4TB Western Digital SATA3 64MB WD40PURX Purple Surveillance",
            'specifications' => "Product Specifications
Model number: WD40PURX
Formatted capacity: 4 TB
Form factor: 3.5-inch
Advanced Format (AF): Yes
RoHS compliant: Yes

Performance

Data transfer rate (max)
  Buffer to host: 6 Gb/s
  Host to/from drive (sustained): 150 MB/s
Cache (MB): 64
Rotational speed (RPM): IntelliPower

Reliability/Data Integrity
Load/unload cycles: 300,000
Non-recoverable read errors per bits read: < 1 in 10^15

Power Management
Average power requirements (W)
  Read/Write: 5.1
  Idle: 4.5
  Standby and Sleep: 0.4

Environmental Specifications
Temperature (°C, on the base casting)
  Operating: 0 to 65
  Non-operating: -40 to 70

Shock (Gs)
  Operating (2 ms, read/write): 30
  Operating (2 ms, read): 65
  Non-operating (2 ms): 250

Acoustics (dBA)
  Idle: 25
  Seek (average)
: 26

Physical Dimensions
Height: 25.4mm 
Length: 147mm 
Width: 101.6mm 
Weight: 0.68kg",
            'price' => "9410",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 29,
            'id_category' => 24,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 6,
            'sold_items' => 0,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\wd40purx_1.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "HDD 3.5\" 3TB Western Digital SATA3 64MB WD30PURX Purple Surveillance",
            'specifications' => "Product Specifications - Interface - SATA 6 Gb/s
Performance Specifications:
 - Rotational Speed - IntelliPower *
 - Buffer Size - 64 MB
 - Load/unload Cycles - 300,000 minimum
Transfer Rates - Buffer To Host (Serial ATA) - 6 Gb/s (Max)
Physical Specifications:
 - Capacity - 3 TB
 - Form Factor - 3.5 Inch
 - User Sectors Per Drive - 5,860,533,168
Physical Dimensions:
 - Height - 26.1 mm
 - Depth - 147 mm
 - Width - 101.6 mm
 - Weight - 0.64 kg
Environmental Specifications:
Shock
 - Operating Shock (Read) - 65G, 2 ms
 - Non-operating Shock - 250G, 2 ms
Acoustics
 - Idle Mode - 23 dBA (average)
 - Seek Mode 0 - 24 dBA (average)
Temperature (English)
 - Operating - 32° F to 149° F
 - Non-operating - -40° F to 158° F
Temperature (Metric)
 - Operating - -0° C to 65° C
 - Non-operating - -40° C to 70° C
Electrical Specifications: 
Current Requirements
 - Power Dissipation
 - Read/Write - 4.40 Watts
 - Idle - 4.10 Watts
 - Standby - 0.60 Watts
 - Sleep - 0.60 Watts
Compatibility - Operating System - Windows/Mac",
            'price' => "6560",
            'num_of_visits' => 0,
            'recomended' => 1,
            'id_brand' => 29,
            'id_category' => 24,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 0,
            'sold_items' => 0,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\wd30purx.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "HDD 3.5\" 2TB Western Digital Red SATA3 64MB Cache IntelliPower",
            'specifications' => "Model
Brand:WD
Series:Red
Model:WD20EFRX
Packaging:Bare Drive

Performance
Interface:SATA 6.0Gb/s
Capacity:2TB
RPM:IntelliPower
Cache:64MB

Physical Spec
Form Factor:3.5\"",
            'price' => "5855",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 29,
            'id_category' => 24,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 7,
            'sold_items' => 3,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\wd20efrx.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "HDD 2.5\" 500GB + 8GB SSD Hybrid Seagate SATA3 64MB ST500LM000",
            'description' => "SSD-like Speed. Large HDD Capacity. An Affordable Price.

- Boots and performs like an SSD
- All-in-one design for simplicity and ease of installation
- Installs and works just like a traditional hard drive in any laptop, PC, Mac or game console, and with any OS and application
- SATA 6Gb/s with NCQ for interface speed
- Self-Encrypting Drive (SED) versions available to maximize data protection and minimize the risk of confidential information breaches",
            'specifications' => "Performance
MLC NAND 8GB
Capacity 500GB at 5,400 RPM
DRAM Cache 64MB
SATA 6Gb/s
Average Data Throughput 100 MB/s
PC Mark Vantage Average HDD Score: 19,838
Average Windows 8 Boot Time <10secs
Seek Average, Read <12ms
Seek Average, Write <14ms

Versus 2.5-in. 5400RPM/7200RPM HDD
Game Load Test: 140% Faster/50% Faster
Application Load Tes: 450% Faster/300% Faster
Windows 7 Boot Time: 35% Faster/25% Faster



Configuration/Organization
Heads/Disks: 2/1
Bytes per Sector: 4096

Reliability/Data Integrity
QuietStep Ramp Load
Load/Unload Cycles: 600,000
Norecoverable Read Errors per Bits Read, Max 1 per 10E14
Predicted Annualized Failure Rate (AFR) 0.48%

7mm height

Size (LxWxH): 3.951in (100.35mm) x 2.75in (69.85mm) x .275in (7mm)
Weight: 0.209lb (95g)

Power - Idle, Typical 0.9 - Seek, Typical 2.5

Environmental
Acoustics (bels-sound power) - Idle, Typical 2.0 - Seek, Typical 2.2
Shock, Operating, 2ms (Read/Write) (Gs): 70/40
Shock, Non-operating, 1ms and 2ms (Gs): 300
Operating Temperature: 0°-60C°
Nonoperating Temperature: -40° to 70°C",
            'price' => "4290",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 28,
            'id_category' => 24,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 4,
            'sold_items' => 0,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\st500lm000_1.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "HDD 2.5\" 1TB Seagate Momentus Spinpoint SATA2 8MB 5400rpm",
            'specifications' => "• ATA Security Mode Feature Set
• SATA Native Command Queuing Feature • ATA S.M.A.R.T. Feature Set
• TuMR/PMR head with FOD technology • SilentSeek™ 
• Serial ATA 3.0Gbps Interface Support • NoiseGuard™
• Load/Unload Head Technology

Capacity 1TB 
Interface SATA 3.0 Gbps (1.5Gbps) 

Temperature
Operating 5 ~ 55 
Non-operating -40 ~ 70 

Buffer DRAM Size 8 MB 
Rotational Speed 5,400 ",
            'price' => "3670",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 28,
            'id_category' => 24,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 16,
            'sold_items' => 16,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\230520141409421.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "HDD 3.5\" 1TB Western Digital SATA3 64MB Caviar Blue 7200rpm WD10EZEX",
            'specifications' => "* 	Interface: SATA 6 Gb/s

Performance Specifications	
* 	Rotational Speed	: 7,200 RPM (nominal)
* 	Buffer Size	: 64 MB
* 	Load/unload Cycles	: 300,000 minimum

Transfer Rates
* 	Transfer Rate (Buffer To Disk)	: 150 MB/s (Sustained)

Physical Specifications	
* 	Formatted Capacity	: 1,000,204 MB
* 	Capacity	: 1 TB
* 	User Sectors Per Drive	: 1,953,525,169

Physical Dimensions	
* 	Height	: 26.1 mm
* 	Depth	: 147 mm
* 	Width	: 101.6 mm
* 	Weight	: 0.44 kg

Environmental Specifications	
* Shock
* 	Operating Shock (Read)	: 30G, 2 ms
* 	Non-operating Shock	: 350G, 2 ms

Acoustics
* 	Idle Mode	29 dBA (average)
* 	Seek Mode 0	30 dBA (average)

Temperature (Metric)
* 	Operating	-0° C to 60° C
* 	Non-operating	-40° C to 70° C


Electrical Specifications	
* Current Requirements
 * 	Power Dissipation
  * 	Read/Write	: 6.80 Watts
  * 	Idle	: 6.10 Watts
  * 	Standby	: 1.20 Watts
  * 	Sleep	: 1.20 Watts",
            'price' => "3015",
            'num_of_visits' => 0,
            'recomended' => 1,
            'id_brand' => 29,
            'id_category' => 24,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 48,
            'sold_items' => 29,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\wd10ezex.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "HDD 3.5\" 1TB Toshiba SATA3 7200rpm 32MB",
            'description' => "Environmental Parameters
Min Operating Temperature
* 0 °C
Max Operating Temperature
* 60 °C
Shock Tolerance
* 70 g @ 2ms half-sine pulse (operating) / 350 g @ 2ms half-sine pulse (non-operating)
Vibration Tolerance
* 0.67 g @ 5-500 Hz (operating) / 1.04 g @ 2-200 Hz (non-operating)
Sound Emission
* 25 dB",
            'specifications' => "General
Device Type
* Hard drive - internal
Capacity
* 1 TB
Form Factor
* 3.5\" x 1/3H
Interface
* Serial ATA-600
Buffer Size
* 32 MB
Bytes per Sector
* 4096
Features
* Halogen Free, Advanced Format technology , S.M.A.R.T.
Width
* 10.16 cm
Depth
* 14.7 cm
Height
* 2.61 cm
Weight
* 450 g

Performance
Drive Transfer Rate
* 600 MBps (external)
Seek Time
* 0.6 ms (max)
Spindle Speed
* 7200 rpm

Reliability
Non-Recoverable Errors
* 1 per 10^14

Expansion & Connectivity
Interfaces
* 1 x Serial ATA-600 - 7 pin Serial ATA
Compatible Bays
* 1 x internal - 3.5\" x 1/3H

Power
Power Consumption (Active)
* 6.4 Watt
Power Consumption (Idle)
* 1 Watt

Miscellaneous
Compliant Standards
* RoHS",
            'price' => "2790",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 27,
            'id_category' => 24,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 23,
            'sold_items' => 23,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\dt01aca100.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "HDD 3.5\" 1TB Hitachi SATA-2 32MB HUA721010KLA330",
            'specifications' => "1 Terabyte (1 TB) formatted capacity
Serial ATA interface
300 MB/s transfer rate
7200 RPM spindle speed
32 MB data buffer
8.2 ms average seek time
4.17 ms average latency
3.5-inch form factor
Advanced PMR heads & media
Self-Protection Throttling (SPT)
Thermal Fly-height Control (TFC)
Fluid Dynamic Bearing (FDB) Motor
Rotational Vibration Safeguard (RVS)",
            'price' => "2750",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 30,
            'id_category' => 24,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 16,
            'sold_items' => 14,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\hitachi1tb.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "HDD 3.5\" 500GB Toshiba SATA3 7200rpm 32MB",
            'specifications' => "MODEL: DT01ACA050		

FEATURES
* Formatted capacity: 500 GByte		
* Form factor: 3.5 inch
* Interface type: Serial ATA

Supported interface standards: 
* ATA-8
* Serial ATA 3.0

S.M.A.R.T.
* The SMART command set is supported.

PHYSICAL PARAMETERS
* Bytes/sector (Host): 512
* Bytes/sector (Disk): 4096 kByte

ACCESS TIMES
* Single track seek time (read): 0.6 ms
* Single track seek time (write): 0.8 ms

TRANSFER RATES
* SATA (Host): max. 6.0 Gbit/s

ROTATIONAL SPEED
* Rotational speed: 7,200 rpm

BUFFER
* Buffer size: 32 MByte		

RELIABILITY SPECIFICATIONS
* Load/Unload: 300,000 Times
* Non-recoverable error rate: 1 error per 1014 read

POWER CONSUMPTION
* Read (Random): 6.4 W		
* Write (Random): 6.4 W		
* Idle: 3.7 W		
* Stand-by: 1.0 W
* Sleep: 1.0 W
* Energy consumption efficiency/Category: 0.0074/A		

POWER SUPPLY
* Allowable voltage
* +5 VDC (+/- 5 %)
* +12 VDC (+/- 10 %)

MECHANICAL SPECIFICATIONS
* Drive width: 101.6 mm
* Drive height: 26.1 mm
* Drive depth: 147 mm
* Drive weight(max.) 0.450 kg

TEMPERATURE
* Operating: From 0 °C to 60 °C
* Non-operating: From -40 °C to 70 °C

VIBRATION
* Operating: 0.67 G, with 5 - 500 Hz
* Non-operating (Maximum): 1.04 G, with 2 - 200 Hz

SHOCK
* Operating: 70 G, with 2 ms half sine wave
* Non-operating (Maximum): 350 G, with 2 ms half sine wave		

ACOUSTIC NOISE
* Idle (typ.): 25 dB		
* Seek: 26 dB",
            'price' => "2555",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 27,
            'id_category' => 24,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 4,
            'sold_items' => 4,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\dt01acaxxx.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "HDD 3.5\" 500GB Hitachi SATA-2 16MB HCS721050CLA362",
            'specifications' => "General Features:
500 GB storage capacity
Serial ATA interface
SATA/300
7200 RPM spindle speed
16 MB buffer
3.5-inch form factor

Power Specifications:
5V, 450mA
12V, 700mA",
            'price' => "2480",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 30,
            'id_category' => 24,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 8,
            'sold_items' => 2,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\hd-0a35415_lg.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "HDD 2.5\" 320GB Toshiba SATA2 5400RPM MK-3265 GSX",
            'specifications' => "* Manufacturer's Part Number: MK-3265 GSX
* Capacity: 320 GB
* Form Factor: 2.5\" x 1/8H
* Interface Type: Serial ATA-300
* Dimensions (WxDxH): 7 cm x 10 cm x 9.5 mm
* Data Transfer Rate: 3 GBps
* Weight: 97 g
* Type: Hard drive - internal
* Buffer Size: 8 MB
* Spindle Speed: 5400 rpm
* Average Seek Time: 12 ms (average) / 22 ms (max)
* Reliability: MTBF 600,000 hour(s) 
* Track-to-Track Seek Time: 2 ms
* Average Latency: 5.55 ms

Miscellaneous
* Compatible with Windows 7\" software and devices carry Microsoft’s assurance that these products have passed tests for compatibility and reliability with 32-bit and 64-bit Windows 7.

* Compliant Standards: RoHS

Environmental Parameters
* Min Operating Temperature: 5 °C
* Max Operating Temperature: 55 °C
* Shock Tolerance: 400 g 2ms (operating) / 900 g 1ms (non-operating)
* Vibration Tolerance: 1 g 5-500 Hz (operating) / 5 g 15-500 Hz (non-operating)
* Sound Emission: 20 dB",
            'price' => "2250",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 27,
            'id_category' => 24,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 9,
            'sold_items' => 5,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\mk3265gsx.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "HDD 3.5\" 320GB Western Digital SATA2 WD3200AAJS",
            'specifications' => "Capacity: 320 GB
Spindle Speed: 7200 rpm
Interface: SATA 3 Gb/s
Cache: 8 MB
3.5 inch Hard Drive",
            'price' => "1795",
            'num_of_visits' => 0,
            'recomended' => 1,
            'id_brand' => 29,
            'id_category' => 24,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 4,
            'sold_items' => 0,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\wd3200aajs.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "HDD 3.5\" 4TB Western Digital Red SATA3 64MB WD40EFRX",
            'specifications' => "Model
Brand:WD
Series:Red
Model:WD40EFRX
Packaging:Bare Drive

Performance
Interface:SATA 6.0Gb/s
Capacity:4TB
RPM:IntelliPower
Cache:64MB

Physical Spec
Form Factor:3.5\"",
            'price' => "10075",
            'num_of_visits' => 0,
            'recomended' => 1,
            'id_brand' => 29,
            'id_category' => 24,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 2,
            'sold_items' => 0,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\wd40efrx.jpg',
            'id_product' => $product->id,
        ]);

        //SSD diskovi

        ////////////////////
        // $product = Product::create([
        //     'name' => "",
        //     'description' => "",
        //     'specifications' => "",
        //     'price' => "",
        //     'num_of_visits' => 0,
        //     'recomended' => 0,
        //     'id_brand' => 1,
        //     'id_category' => 24,
        // ]);
         
        // Stock::create([
        //     'id_product' => $product->id,
        //     'id_warehouse' => 1,
        //     'num_of_items' => 4,
        //     'sold_items' => 0,
        //     'adding_date' => '2016-09-27',
        // ]);
         
        // ProductImage::create([
        //     'url' => 'uploads\images\69050_2',
        //     'id_product' => $product->id,
        // ]);
    }
}
