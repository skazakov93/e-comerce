<?php

use Illuminate\Database\Seeder;
use App\Modules\Products\Product;
use App\Modules\Stocks\Stock;
use App\Modules\ProductImages\ProductImage;

class InputAndOutputProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//Monitors
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "27\" Dell U2715H QHD 2560x1440/IPS/2xHDMI/2xDP/1xMDP/6xUSB",
    		'specifications' => "Diagonally Viewable Size: 68.58 cm 27\" (27-inch wide viewable image size)
				Aspect Ratio: Widescreen (16:9)
				Panel Type, Surface: In-plane switching, anti glare with hard coat 3H
				Optimal resolution: 2560 x 1440 at 60Hz
				
				Contrast Ratio: 1000: 1 (typical)
				Dynamic Contrast Ratio : 2 Million:1 (Max)
				
				Brightness:
				350 cd/m2 (typical)
				50 cd/m2 (min)
				
				Response Time:
				8 ms (gray to gray) Normal Mode
				6 ms ( gray to gray) FAST Mode
				
				Viewing Angle:
				(178° vertical / 178° horizontal)
				Color Support:
				Color Gamut (typical): 91 % (CIE1976), sRGB 99% ( average Delta E of<3)n16.78 Million colors
				
				Pixel Pitch:m0.2331 mm
				Backlight Technology: LED
				Display Type: Widescreen Flat Panel Display
				Display Screen Coating: Antiglare with hard-coating 3H
				
				Connectivity
				2 HDMI(MHL) connector
				1 Mini DisplayPort
				1 DisplayPort (version 1.2)
				1 DisplayPort out (MST)
				1 Audio Line out (connect your speakers)
				5 USB 3.0 ports - Downstream (4 at the back, 1 with battery charging)
				1 USB 3.0 port - Upstream",
    		'price' => "27980",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 16,
    		'id_category' => 38,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 4,
	    	'sold_items' => 4,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\9.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\10.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "24\" BL2420U Benq LED Designer 3840x2160 UHD, DVI, HDMix2, Display port,USBx3,Speakers, Black",
    		'specifications' => "24 inch 3840x2160 UHD resolution
				100% sRGB and REC 709 color space with IPS technology
				CAD/CAM mode and Animation mode enable the detailed and precise design
				Display Pilot software for customized monitor setting
				Flicker-free and Low Blue Light to protect your eyes
				
				Panel‎ 
				LCD Size (inch)‎ - 23.6‎
				Aspect Ratio‎ - 16:9‎
				Resolution (max.)‎ - 3840x2160at 60Hz(DP, HDMI-2)
				3840x2160at 30Hz(DVI, HDMI-1)‎
				Display Area(mm)‎ - 521.28x293.22‎
				Pixel Pitch (mm)‎ - 0.136 ‎
				Brightness (typ)‎ - 300‎
				Native Contrast ( typ. )‎ - 1000:1‎
				DCR (Dynamic Contrast Ratio) (typ.)‎ - 20M:1‎
				Panel Type‎ - IPS tecnology‎
				Viewing Angle (L/R;U/D) (CR>=10)‎ - 178/178‎
				Response time‎ - 7ms (GTG)‎
				Display Colors‎ - 16.7 Million‎
				Color Gamut‎ - 100% sRGB‎
				Color Bit‎ - 8bits‎
				Backlight‎ - LED‎
				Software‎ 
				Display Pilot‎ - Yes‎
				Regulations‎ 
				Energy Star‎ - 6.0‎
				Audio/Video Inputs/Outputs‎ 
				Input Connector‎ - DVI-DL / HDMI1.4 / HDMI2.0 / DP1.2a / headphone jack / line in‎
				Dimensions & Weight‎ 
				Dimensions with Wall Mount (HxWxD mm) (w/o Base)‎ - 338.7x563.2x71.5‎
				Net Weight (kg)‎ - 7.1‎
				Height Adjustment (mm)‎ - 140‎
				Special Features‎ 
				AMA‎ - Yes‎
				Display Screen Coating‎ - Anti-Glair‎
				Flicker-free Technology‎ - Yes‎
				Animation mode‎ - Yes‎
				CAD/CAM mode‎ - Yes‎
				Low Blue Light‎ - Yes‎
				Eco Sensor‎ - Yes‎
				Eye Protect Sensor‎ - Yes‎
				Auto Pivot‎ - Yes‎
				Tilt (down/up)‎ - -5~20‎
				Pivot‎ - 90‎
				Speaker‎ - 1W‎
				USB Hub‎ - USB3.0 x 2‎
				Power‎ 
				Power Consumption (Power saving mode)‎ - 0.5‎
				Power Consumption (Off mode)‎ - 0W by AC switch‎
				Power Consumption (Base on Energy star )‎ - 26‎
				Certification‎ 
				Mac Compatible‎ - Yes‎
				Windows® 7 Compatible‎ - Yes‎
				Windows® 8 Compatible‎ - Yes‎
				Windows® 8.1 Compatible‎ - Yes‎",
    		'price' => "22980",
    		'num_of_visits' => 10,
    		'recomended' => 1,
    		'id_brand' => 17,
    		'id_category' => 38,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 7,
	    	'sold_items' => 1,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\bl2420u.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\bl2420u-1.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "27\" LG 27MP38VQ-B LED IPS Full HD 1920 x 1080, HDMI, DVI, VGA 5ms",
    		'description' => "Additional Picture Mode, Reader Mode, Original Ratio, DDC/CI, HDCP,
				Key Lock, Intelligent Auto (Auto Adjustment), Plug & Play,
				Response Time Control, Colour Calibrated, Automatic Standby,
				Six Axis Control, Super Resolution+, 4 screen split
				Stand Tilt -3º (Front) ~ 20º (Rear)",
    		'specifications' => "PANEL
				Screen Size 27“ (69cm)
				Panel Technology IPS
				Aspect Ratio 16 : 9
				Native Resolution 1920 x 1080 (FHD)
				Brightness 250cd/m2
				Contrast Ratio 1,000:1
				Dinamic CR 5M:1
				Response Time 5ms (G to G)
				Viiewing Angle (H x V) 178° x 178°
				Supported colours 16.7M
				Surface Treatment Anti glare ,3H
				CONNECTIVITY
				Digital DVI-D (1), HDMI (1), D-Sub (1)
				Audio Headphone out (1)
				
				PHYSICAL SPECIFICATIONS
				Set (with stand) Dimension (W x H x D ) 614mm x 470mm x 205mm
				Set (without stand) Dimension (W x H x D) 614mm x 369mm x 52mm
				Set (with stand) Weight 4.6kg
				Set (without stand) Weight 4.3kg
				Carton Dimensions (W x H x D) 689mm x 442mm x 154mm
				Packed Weight 6.3kg
				VESA™ Standard Mount Interface 100x100 mm",
    		'price' => "13480",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 18,
    		'id_category' => 38,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 4,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\frtgorfdenfx.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "24\" Dell UltraSharp U2414H LED IPS, 16:9, FULL HD, 2xHDMI/DP/5xUSB",
    		'specifications' => "Diagonally Viewable Size: 23.8\" 
				Aspect Ratio: Widescreen (16:9)
				Panel Type, Surface: In-plane switching, anti glare with hard coat 3H
				Optimal resolution: 1920 x 1080 at 60Hz
				Contrast Ratio: 1000: 1 (typical) m2 Million:1 (Max) (Dynamic Contrast Ratio)
				Brightness: 250 cd/m2 (typical)
				Response Time: 8 ms (gray to gray)
				Viewing Angle: (178° vertical / 178° horizontal)
				Color Support: Color Gamut7 (typical): 85% (CIE1976), sRGB 96% ( average Delta E of>4) 16.77 Million colors
				Pixel Pitch: 0.274 mm
				Backlight Technology: LED
				Display Type: Widescreen Flat Panel Display
				Display Screen Coating: Antiglare with hard-coating 3H
				
				Connectivity
				2 HDMI(MHL) connectors
				1 Mini DisplayPort
				1 DisplayPort (version 1.2a)
				1 DisplayPort out (MST)
				1 Audio Line out (connect your speakers)
				4 USB 3.0 ports - Downstream (4 at the back, 1 with battery charging)
				1 USB 3.0 port - Upstream",
    		'price' => "13280",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 16,
    		'id_category' => 38,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 12,
	    	'sold_items' => 5,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\180320140929171.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\180320140929382.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "24\" Dell P2414H LED IPS, Full HD DVI/VGA/Display Port/4 x USB",
    		'specifications' => "Diagonally Viewable Size: 60.47 cm
				23.8\" (23.8-inch wide viewable image size)
				Aspect Ratio: Widescreen (16:9)
				Panel Type, Surface: In-plane switching, anti glare with hard coat 3H
				Optimal resolution: 1920 x 1080 at 60 Hz
				Contrast Ratio: 1000: 1 (typical)
				Dynamic Contrast Ratio : 2 Million:1 (Max)
				
				Brightness: 250 cd/m2 (typical)
				Response Time: 8 ms (gray to gray)
				Max Viewing Angle: (178° vertical / 178° horizontal)
				Color Support: 16.7 million colors
				Pixel Pitch: 0.2745 mm
				Panel Backlight: LED
				Display Type: Widescreen Flat Panel Display
				
				CONNECTIVITY
				1 Digital Visual Interface connectors (DVI-D) with HDCP
				1 Video Graphics Array (VGA)
				1 DisplayPort (version 1.2a)
				4 USB 2.0 ports ( 3 downstream ports, 1 upstream port)
				
					
				BUILT-IN DEVICES
				USB 2.0 Hi-Speed Hub (with 1 USB upstream port and 3 USB downstream ports)
				
				STAND
				Height-adjustable stand, pivot, tilt , swivel and built in cable-management
				Flat Panel Mount Interface:
				VESA (100 mm)",
    		'price' => "10480",
    		'num_of_visits' => 25,
    		'recomended' => 0,
    		'id_brand' => 16,
    		'id_category' => 38,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 9,
	    	'sold_items' => 3,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\295.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\1607201414140222.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "23\" Dell S2316H LED IPS Full HD 16:9 VGA/HDMI/Speakers",
    		'specifications' => "Diagonally Viewable Size:
				58.42 cm
				23.0 Inches (23.0-inch wide viewable image size)
				Aspect Ratio:
				Widescreen (16:9)
				Panel Type, Surface:
				In-plane switching, Glossy with Low-Haze 3H
				Optimal resolution:
				1920 x 1080 at 60Hz
				Active Display Area (H x V):
				509.18mm x 286.41mm
				20.05\" x 11.28\"
				Contrast Ratio:
				1000: 1 (typical), Dynamic Contrast Ratio : 8 Million: 1
				Brightness:
				250 cd/m2 (typical)
				Response Time:
				6 ms gray to gray (typical)
				Viewing Angle:
				178° vertical / 178° horizontal
				Adjustability:
				Tilt
				Color Support:
				Color Gamut (typical): 82% (CIE1976), 72% (CIE 1931)
				16.78 Million colors
				Pixel Pitch:
				0.265mm
				Backlight Technology:
				LED
				Display Type:
				Widescreen Flat Panel Display
				Display Screen Coating:
				Antiglare with hard-coating 3H
				
				Security:
				Security lock slot(cable lock sold separately)
				Built-in Devices
				2 x 3W speakers
				
				Connectivity:
				1 HDMI connector
				1 VGA connector
				1 Audio In
				1 Audio Off
				
				Power Consumption (Operational):
				20.5W (Energy Star)
				23W (typical) - 34W (maximum)
				Power Consumption Stand by / Sleep:
				Less than 0.3W",
    		'price' => "9480",
    		'num_of_visits' => 0,
    		'recomended' => 1,
    		'id_brand' => 16,
    		'id_category' => 38,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 15,
	    	'sold_items' => 1,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\dell-s2216h.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "24\" S24D300HS Samsung LED, Full HD 1920x1080, VGA, HDMI, 2ms",
    		'specifications' => "Display:
				Screen Size - 24\"
				Panel Type - TN  
				Brightness - 250cd/m²   
				Contrast Ratio - 1,000:1 (Typ) 
				Dynamic Contrast Ratio - Mega ∞
				Resolution - 1920 x 1080 
				Response Time - 2ms (GTG) 
				Color Supported - 16.7M
				
				General Feature:
				Embedded Function - Game Mode, Magic Upscale, Eco Saving, Off Timer, Image Size
				OS Compatibility - Windows, Mac
				Windows Certification - Windows 8.1 
				
				Interface:
				D-Sub - 1 
				DVI - No  
				Dual Link DVI - No
				Display Port - No 
				HDMI - 1
				USB - No  
				Headphone - No  
				USB Hub - No  
				
				Sound:
				Speaker - No  
				
				Design:
				Color - Black High Glossy
				Stand Type - Circle 
				Wall Mount - No  
				
				Power:
				Power Supply - AC100 - 240V (50 / 60Hz)
				Power Consumption - 16W (Typ) (Energy Star Current Test Condition
				Power Consumption (Stand-by) - 0.3W (Typ)
				Type - External Adaptor
				
				Dimension:
				Set Dimension with Stand (WxHxD) - 569.0 x 417.2 x 197.0mm  
				Set Dimension without Stand (WxHxD) - 569.0 x 342.1 x 53.9mm  
				Package Dimension (WxHxD) - 635.0 x 408.0 x 111.0mm  
				
				Weight:
				Set Weight with Stand - 3.15Kg  
				Set weight without stand - 2.80Kg  
				Package Weight - 4.40Kg  
				
				Accessory:
				Cables - D-sub Cable
				Others - Quick Setup Guide",
    		'price' => "8280",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 19,
    		'id_category' => 38,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 24,
	    	'sold_items' => 9,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\ls24d300hs.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\ls24d300hs-2.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "24\" LG 24M38D-B LED 1920 x 1080, 5ms, VGA, DVI",
    		'specifications' => "screen Size - 23.5 '
				panel Type - TN
				Aspect ratio - 16: 9
				Resolution - 1920x1080
				Brightness (cd / m2) - 200
				contrast ratio - 1000: 1
				Contrast Ratio (DFC) - mega
				Response time (on / off) - 5ms
				Viewing angle (CR≥10) - 170/160
				Connectors
				D-Sub - Yes
				DVI-D - Yes
				Composite video input / outlet - No
				S-Video - No
				component video - No
				SCART - No
				HDMI - No
				DisplayPort - No
				Thunderbolt - No
				RCA - No
				PC Audio Input - No
				Jack position - behind
				microphone input - No
				Headphone Output - No
				power
				Power Type (Adapter or LIPS) - Adapter / 100 ~ 240V
				Consumption - Operating - 25W
				Power consumption on standby - 0.3W
				Screen is off - 0.3W
				Frequency
				Analog Horizontal Frequency - 30 ~ 83kHz
				Vertical Frequency Analogue - 56 ~ 75Hz
				Digital Horizontal Frequency - 30 ~ 83kHz
				Digital Vertical Frequency - 56 ~ 75Hz
				product Features
				POP - No
				PBP - No
				Plug and Play - Yes
				DDC / CI - Yes
				HDCP - Yes
				Intelligent Auto (Auto-adjusting the image) - Yes
				photo effects - No
				special features
				default color calibration - No
				Color Cloning - No
				Color Weakness - Yes
				Reader Mode - Yes
				Flicker Safe - Yes
				SIX Axis Control - No
				4 split screen - Yes
				Freesync - No
				Game Mode - Yes
				On Screen Control - Yes
				Black Stabilizer - No
				PIP - Screen Split software
				number of languages - 17
				Language - English, German, French, Spanish, Italian, Swedish, Finnish, Portuguese, Polish, Russian, Greek, Chinese, Japanese, Korean, Ukrainian, Portuguese (Brazil), Hindi
				Smart Energy Saving - Yes
				Sole
				Removable Foot - Yes
				Tilt - Yes (-5º (forward) ~ 20 ° (reverse))
				pivotability - No
				Adjustable height - No
				pivot - No
				double Tilt - No
				Size / Weight
				Size: soles - 556.2 x186,7 x415,1
				Size: Without Foot - 556.2 x 56.8 x 344
				Size Box - 624 x 409 x 124
				Size: Wall Fixing - Yes (100 mm x 100 mm)
				Weight: with stand - 2.8
				Weight: Without Foot - 2.6
				Weight Box - 4.1
				accessories
				Power Cable - Optional
				D-Sub - Optional
				DVI-D - Optional
				DisplayPort - No
				HDMI - No",
    		'price' => "7580",
    		'num_of_visits' => 38,
    		'recomended' => 1,
    		'id_brand' => 18,
    		'id_category' => 38,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 19,
	    	'sold_items' => 18,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\22m38ab.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "22\" S22E390HS Samsung LED PLS, Full HD, 1920 x 1080, 4ms HDMI, D-sub",
    		'specifications' => "Display 
				Screen Size - 21.5\"
				Active Display Size (HxV) - 476.64mm(H)x268.11mm(V)
				Aspect Ratio - 16:9
				Panel Type - PLS
				Dynamic Contrast Ratio - Mega
				Resolution - 1920x1080
				Pixel Pitch (HxV) - 0.24825mm(H)x0.24825mm(V)
				Response Time - 4 ms
				Viewing Angle (H/V) - 178°/178°
				Color Support - 16.7M
				Color Gamut (NTSC 1976) - 72%
				
				Interface 
				D-Sub x1
				HDMI x1
				Headphone x1
				
				Operation Conditions:Temperature (℃) - 10℃~40 ℃
				Humidity (%) - 10~80 (non-condensing)
				Design 
				Color+ - Black High Glossy
				Stand Type - Simple
				Tilt - -2° (±2°) to 15° (±2°)
				
				Eco 
				Energy/Environment Mark - Energystar6.0
				Power 
				Power Supply - AC 100~240V
				Power Consumption (DPMS)  - 0.3(Typ.)
				Power Consumption (Stand-by) - 0.3（max）
				Energy Star Power Consumption - 17.9(Typ.)
				Type - External Adaptor
				
				Dimension 
				Set Dimension with Stand (WxHxD) (mm) - 519.06 x 391.68 x 167.46 mm
				Set Dimension without Stand (WxHxD) (mm) - 519.06 x 327.7 x 70.08 mm
				Package Dimension (WxHxD) (mm) - 583 x 140 x 391 mm
				Weight 
				Set Weight with Stand (kg) - 3.55 kg
				Set Weight without Stand (kg) - 3.05 kg
				Package Weight (kg) - 4.75 kg
				Accessory 
				Cables - D-sub cable
				Others
				Quick Setup Guide",
    		'price' => "7180",
    		'num_of_visits' => 16,
    		'recomended' => 0,
    		'id_brand' => 19,
    		'id_category' => 38,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 24,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\ls22e390hs-xk_001_front_black.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "22\" GW2270HM BenQ VA LED Eye-care 5ms 3000:1 VGA, DVI, HDMI, Speakers",
    		'specifications' => "Panel‎ 
				LCD Size (inch)‎ - 21.5‎
				Aspect Ratio‎ - 16:9‎
				Resolution (max.)‎ - 1920X1080‎
				Display Area(mm)‎ - 476.64X268.11‎
				Pixel Pitch (mm)‎ - 0.248‎
				Brightness (Typical)‎ - 250 cd/㎡‎
				Native Contrast ( typ. )‎ - 3000:1‎
				Panel Type‎ - AMVA+(SNB)‎
				Viewing Angle (L/R;U/D) (CR>=10)‎ - 178/178‎
				Response time‎ - 18ms, 5ms (GtG) ‎
				Display Colors‎ - 16.7 M‎
				Color Gamut‎ - 72% NTSC‎
				Backlight‎ - LED‎
				
				Regulations‎ 
				EPEAT‎ - 7.0‎
				TCO‎ - NA‎
				Energy Star‎ - 7.0‎
				
				Audio/Video Inputs/Outputs‎ 
				Hor. Frequency (KHz)‎ - 30~83‎
				Ver. Frequency (Hz)‎ - 50~76‎
				Video Bandwidth (MHZ)‎ - 205‎
				Audio Line In/Out‎ - line in‎
				
				Dimensions & Weight‎ 
				Dimensions with Wall Mount (HxWxD mm) (w/o Base)‎ - 304.2x 505.60x 50.5‎
				Dimensions(HxWxD mm)‎ - 400.07x 505.60x 178.17‎
				Net Weight (kg)‎ - 3.6‎
				Gross Weight (kg)‎ - 4.7‎
				
				Special Features‎ 
				AMA‎ - Yes‎
				HDCP‎ - Yes‎
				Dynamic Power Saving (DPS)‎ - Yes‎
				Flicker-free Technology‎ - Yes‎
				Color Temperature‎ - Reddish / Normal/ Bluish /user mode‎
				Low Blue Light‎ - Yes‎
				Speaker‎ - 1Wx2‎
				K Locker‎ - Yes‎
				OSD Language‎ - 18 languages‎
				
				Included Accessories‎ 
				Signal Cable‎ - VGA Cable ‎
				
				I/O‎ 
				Speakers‎ - Yes‎
				DVI‎ - Yes‎
				HDMI‎ - Yes‎
				VGA input‎ - Yes‎
				Audio Line In/Out‎ - Yes‎
				
				Power‎ 
				Power Consumption (Eco mode)‎ - 18W‎
				Power Supply (90~264 AC)‎ - Yes‎
				Power Consumption (On mode)‎ - 26W‎
				Power Consumption (Power saving mode)‎ - < 0.5W‎
				Power Consumption (Off mode)‎ - < 0.5W‎
				
				Reliability‎ 
				MTBF(hr, exclude lamp)‎ - 60000‎
				Lamp Life (hr) min‎ - 30000‎
				Lamp Life (hr) Typical‎ - 30000‎
				
				Certification‎ 
				Windows® 7 Compatible‎ - Yes‎
				Windows® 8 Compatible‎ - Yes‎
				Windows® 8.1 Compatible‎ - Yes‎
				Windows® 10 Compatible‎ - Yes‎
				TUV low blue light‎ - Yes‎
				TUV Flicker Free‎ - Yes‎",
    		'price' => "6180",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 17,
    		'id_category' => 38,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 10,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\gw2270h.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "22\" Philips 223V5LSB Slim V-Line, Full HD, 5ms, VGA/DVI Black",
    		'specifications' => "Picture/Display
				• LCD panel type: TFT-LCD
				• Backlight type: W-LED system
				• Panel Size: 21.5 inch / 54.6 cm
				• Effective viewing area: 476.64 (H) x 268.11 (V)
				• Aspect ratio: 16:9
				• Optimum resolution: 1920 x 1080 @ 60 Hz
				• Response time (typical): 5 ms
				• Brightness: 250 cd/m²
				• SmartContrast: 10,000,000:1
				• Contrast ratio (typical): 1000:1
				• Pixel pitch: 0.248x 0.248 mm
				• Viewing angle: 170º (H) / 160º (V), @ C/R > 10
				• Display colors: 16.7 M
				• Scanning Frequency: 30 -83 kHz (H) / 56 -76 Hz (V)
				• sRGB
				Connectivity
				• Signal Input: VGA (Analog ), DVI-D (digital, HDCP)
				• Sync Input: Separate Sync, Sync on Green
				Convenience
				• User convenience: Auto/Down, 4:3 Wide/Up,
				Brightness/Back, Menu/OK, Power On/Off
				• OSD Languages: Brazil Portuguese, Czech, Dutch,
				English, Finnish, French, German, Greek,
				Hungarian, Italian, Japanese, Korean, Polish,
				Portuguese, Russian, Simplified Chinese, Spanish,
				Swedish, Traditional Chinese, Turkish, Ukranian
				• Other convenience: Kensington lock, VESA mount
				(100x100mm)
				• Plug & Play Compatibility: DDC/CI, Mac OS X,
				sRGB, Windows 7, Windows 8
				Stand
				• Tilt: -5/20 degree
				Power
				• On mode: 15.29 W (typ.) (EnergyStar 6.0 test
				method)
				• Standby mode: 0.5 W
				• Off mode: 0.5 W
				• Power LED indicator: Operation - White, Standby
				mode- White (blinking)
				• Power supply: Built-in, 100-240VAC, 50-60Hz
				Dimensions
				• Product with stand (mm): 503 x 376 x 213 mm
				• Product without stand (mm): 503 x 316 x 50 mm
				• Packaging in mm (WxHxD): 555 x 414 x 107 mm
				Weight
				• Product with stand (kg): 2.61 kg
				• Product without stand (kg): 2.35 kg
				• Product with packaging (kg): 3.87 kg
				Operating conditions
				• Temperature range (operation): 0°C to 40°C °C
				• Temperature range (storage): -20°C to 60°C °C
				• Relative humidity: 20%-80 %
				• Altitude: Operation: +12,000ft (3,658m), Nonoperation:
				+40,000ft (12,192m)
				• MTBF: 30,000 hour(s)
				Sustainability
				• Environmental and energy: EnergyStar 6.0, EPEAT
				Silver, RoHS, Lead-free, Mercury",
    		'price' => "5580",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 12,
    		'id_category' => 38,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 35,
	    	'sold_items' => 7,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\223v5lsb2.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "22\" LG 22M38AB LED 1920 x 1080, 5ms, VGA",
    		'specifications' => "Size 21.5\"  Full HD LED  1920 x 1080 resolution
				Brightness: 200 (cd/m2)
				Contrast Ratio: 600:1
				Response Time:  5 (cd/m2)
				Pixel Pitch: 0.248 x 0.248  (mm)
				Surface treatment Hard coating (3H), Anti-glare
				Viewing angle (CR≥10) - 90/65
				
				CONNECTIONS 
				VGA
				
				SPECIAL FEATURES
				Dual Smart Solution
				Anti flicker
				Intelligent Auto
				Super Energy Saving",
    		'price' => "5780",
    		'num_of_visits' => 0,
    		'recomended' => 1,
    		'id_brand' => 18,
    		'id_category' => 38,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 9,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\22m38ab.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "22\" Philips 223V5LSB2/10 Slim V-Line, Full HD, 5ms, Black",
    		'specifications' => "Picture/Display
				• LCD panel type: TFT-LCD
				• Backlight type: W-LED system
				• Panel Size: 21.5 inch/54.6 cm
				• Effective viewing area: 476.64 (H) x 268.11 (V)
				• Aspect ratio: 16:9
				• Optimum resolution: 1920 x 1080 @ 60 Hz
				• Response time (typical): 5 ms
				• Brightness: 200 cd/m²
				• SmartContrast: 10,000,000:1
				• Contrast ratio (typical): 600:1
				• Pixel pitch: 0.248 x 0.248 mm
				• Viewing angle: 90º (H)/65º (V), @ C/R > 10
				• Display colours: 16.7 M
				• Scanning Frequency: 30-83 kHz (H) / 56-76 Hz (V)
				• sRGB
				Connectivity
				• Signal Input: VGA (Analogue)
				• Sync Input: Separate Sync, Sync on Green
				Convenience
				• User convenience: Auto/Down, 4:3 Wide/Up,
				Brightness/Back, Menu/OK, Power On/Off
				• OSD Languages: Brazil Portuguese, Czech, Dutch,
				English, Finnish, French, German, Greek,
				Hungarian, Italian, Japanese, Korean, Polish,
				Portuguese, Russian, Simplified Chinese, Spanish,
				Swedish, Traditional Chinese, Turkish, Ukrainian
				• Other convenience: Kensington lock, VESA mount
				(100 x 100 mm)
				• Plug and Play Compatibility: DDC/CI, Mac OS X,
				sRGB, Windows 7, Windows 8
				Stand
				• Tilt: -5/20 degree
				Power
				• On mode: 15.61 W (typ.) (EnergyStar 6.0 test
				method)
				• Standby mode: 0.5 W
				• Off mode: 0.5 W
				• Power LED indicator: Operation - White, Standby
				mode - White (flashing)
				• Power supply: Built-in, 100-240 VAC, 50-60 Hz
				Dimensions
				• Product with stand (mm): 503 x 376 x 213 mm
				• Product without stand (mm): 503 x 316 x 50 mm
				• Packaging in mm (W x H x D): 555 x 414 x 107 mm
				Weight
				• Product with stand (kg): 2.61 kg
				• Product without stand (kg): 2.35 kg
				• Product with packaging (kg): 3.87 kg
				Operating conditions
				• Temperature range (operation): 0°C to 40°C °C
				• Temperature range (storage): -20°C to 60°C °C
				• Relative humidity: 20–80% %
				• Altitude: Operation: +12,000 ft (3658 m), Nonoperation:
				+40,000 ft (12,192 m)
				• MTBF: 30,000 hour(s)
				Sustainability
				• Environmental and energy: EnergyStar 6.0, EPEAT
				Silver, RoHS, Lead-free, Mercury Free
				• Recyc",
    		'price' => "5380",
    		'num_of_visits' => 19,
    		'recomended' => 0,
    		'id_brand' => 12,
    		'id_category' => 38,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 12,
	    	'sold_items' => 6,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\223v5lsb2.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "19\" GL955A BenQ LED 5ms Wide Black",
    		'specifications' => "Product Color - Glossy Black
				Display
				Screen Size - 18.5”W
				Aspect Ratio - 16:9
				Resolution (max.) - 1366 x 768
				Pixel Pitch (mm) - 0.3
				Brightness ( typ.) - 200 cd/m2
				Native Contrast ( typ. ) - 600:1
				DCR (Dynamic Contrast Ratio) (typ.) - 12M:1
				Panel Type - TN
				Viewing Angle (L/R;U/D) (CR>=10) - 90 / 50
				Response Time(Tr+Tf) typ. - 5ms
				Display Colors - 16.7million
				Color Gamut - 72%
				Audio/Video Inputs/Outputs
				Input Connector - D-sub
				Power
				Power Supply (90~264 AC) - Built-in
				Power Consumption (On mode) - 11W (Base on Energy Star)
				(Power saving mode) - <0.3W
				Dimensions & Weight
				CTN Dimensions (HxWxD mm) - 365 x 518 x 116
				Dimensions(HxWxD mm) - 362 x 459 x 164
				Dimensions with Wall Mount (HxWxD mm) - 282 x 459 x 57
				Net Weight (kg) - 2.8
				Gross Weight (kg) - 3.9
				Senseye - Senseye 3
				Vista Compatibility - Basic
				Color Temperature - Reddish, Normal, Bluish, User mode
				OSD Language - 17 languages
				VESA Wall Mounting - 100x100mm
				Tilt (down/up) - -5/15
				K Locker - Yes",
    		'price' => "4380",
    		'num_of_visits' => 9,
    		'recomended' => 0,
    		'id_brand' => 17,
    		'id_category' => 38,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 35,
	    	'sold_items' => 20,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\9hl5platb.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "19\" 193V5LSB2 Philips LED Slim V-Line, 1366x768, 5ms, Black",
    		'specifications' => "Picture/Display
				• LCD panel type: TFT-LCD
				• Backlight type: W-LED system
				• Panel Size: 18.5 inch/47 cm
				• Effective viewing area: 409.8 (H) x 230.4 (V)
				• Aspect ratio: 16:9
				• Optimum resolution: 1366 x 768 @ 60 Hz
				• Response time (typical): 5 ms
				• Brightness: 200 cd/m?
				• SmartContrast: 10,000,000:1
				• Contrast ratio (typical): 700:1
				• Pixel pitch: 0.30 x 0.30 mm
				• Viewing angle: 90? (H)/65? (V), @ C/R > 10
				• Display colours: 16.7 M
				• Scanning Frequency: 30–83 kHz (H)/56–75 Hz (V)
				• sRGB
				Connectivity
				• Signal Input: VGA (Analogue)
				• Sync Input: Separate Sync, Sync on Green
				Convenience
				• User convenience: Power On/Off
				• Other convenience: Kensington lock, VESA mount 
				(100 x 100 mm)
				• Plug and Play Compatibility: DDC/CI, Mac OS X, 
				sRGB, Windows 7, Windows 8
				Stand
				• Tilt: -3/10 degree
				Power
				• On mode: 8.76 W (typ.) (EnergyStar 6.0 test 
				method)
				• Standby mode: 0.5 W (typ.)
				• Off mode: 0.5 W (typ.)
				• Power LED indicator: Operation - White, Standby 
				mode - White (flashing)
				• Power supply: Built-in, 100-240 VAC, 50-60 Hz
				Dimensions
				• Product with stand (mm): 437 x 338 x 170 mm
				• Product without stand (mm): 437 x 273 x 48 mm
				• Packaging in mm (W x H x D): 481 x 350 x 103 mm
				Weight
				• Product with stand (kg): 2.15 kg
				• Product without stand (kg): 1.94 kg
				• Product with packaging (kg): 3.00 kg
				
				Cabinet
				• Colour: Black
				• Finish: Hairline (front bezel)/Texture (rear cover)",
    		'price' => "4180",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 12,
    		'id_category' => 38,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 26,
	    	'sold_items' => 8,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\223v5lsb2.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	//Tatatiri
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "SteelSeries Apex M500 Mechanical Gaming Blue LED / Cherry MX Red Switches",
    		'description' => "
				The Apex M500 is a tournament-grade backlit mechanical gaming keyboard built entirely around the needs of professional gamers.
				
				Features:
				ESPORTS LEGACY
				Outlast The Competition
				Full Anti-Ghosting
				More Than A Feeling
				Cherry MX Reds
				
				THE DESIGN
				Built Like a Tank
				Blue LED Illumination
				Standard Key Layout
				
				Quick Access Media Control
				Unique Cable Management
				Adjustable height
				Engineering 101
				
				Limitless Customization
				Macros Galore
				Unlimited Profiles",
    		'specifications' => "SWITCHES
				Switch Type: Mechanical
				Switch Name: Cherry MX Red Gaming Switches
				Throw Depth: 4 mm
				Actuation and Reset Depth: 2 mm
				Actuation Force Needed: 45cN
				50 Million Click Lifetime Guarantee
				
				DESIGN
				Layout: Traditional
				Full Anti-Ghosting Support
				N-Key Roll Over: 104 Key
				Illumination: Per-Key Blue LEDs
				Quick Access Media Keys
				Fully Programmable Keys
				Cable Management System
				Weight: 1241 g, 2.742 lbs
				Height: 136.43 mm, 5.37 in
				Width: 440.56 mm, 17.34 in
				Depth: 39.52 mm, 1.56 in
				Large Adjustable Rubber Feet
				Cable Length: 2 m, 6.5 ft
				
				CUSTOMIZATION OPTIONS
				Engine Support: SSE3
				Remappable Keys
				Custom Key Illumination
				Unlimited Profiles",
    		'price' => "6490",
    		'num_of_visits' => 22,
    		'recomended' => 0,
    		'id_brand' => 3,
    		'id_category' => 39,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 4,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\64490_1.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\64490_2.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "A4 Bloody Gaming B120 Illuminated Water Resistant USB 3.0",
    		'description' => "-All types of Gaming Genres requires to continuously click a key or hold-on to a key on a keyboard.  With our exclusive \"Auto/Turbo Shot\" Function, it allow users to set any key into such function.  
				-Extreme 1MS Key Response is the key to our success compared to normal gaming keyboards in the market.  Bloody sends signal to computer less than 1ms while others requires 18ms.  Be ahead of your enemy is most important in a game.
				- No More Accidental-Click with Slant angle keycap design lowers the probability of accidental-click among the keys
				-Electronic Sealed + Water-Drain Hole Greatly prolonged the lifetime of keyboard 
				- Fully supports FPS games with 3 ANTI-GHOST KEYS:  No conflic between 3 sets of weapons
				- Keycaps are made with highly scratchable black finish, letters illuminated through 5 Levels of LED Brightness.",
    		'specifications' => "Transmission：Wired
				Letter LED：5 Levels of Brightness
				Bloody Logo: Illumiated in Red
				Connector ：USB (2.0/3.0)
				System supports：Windows XP/Vista/7/8
				Cable length：1.8 m
				B120 Dimension/Weight (g)：458 x 180 x 37（mm）/ 1694（g）",
    		'price' => "1690",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 20,
    		'id_category' => 39,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 17,
	    	'sold_items' => 3,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\b120.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "A4 Bloody Gaming B328 Illuminated w/Infrared Mechanical Switch",
    		'description' => "A4tech Bloody B328
				Gaming Keyboard Bloody panders only a small bit of music for money, but an entire orchestra that surpasses much more expensive competition. The most common gaming keys used innovatively conceived switches with optical scanning. It can withstand the same sensitivity lifetime, but mainly because of the keyboard makes fastest gaming system in the world. Only 0.2 ms from pressing for transfer to a computer means responsive interface with your fingers. Program the keys own macros and start the battle.
				
				Key Features
				Optical scanning pressing for absolute accuracy (Q, W, E, R, A, S, D, F)
				The reaction of only 0.2 ms after pressing
				Resistant to dust and water splashes
				Neon illumination with adjustable brightness
				Reinforced Space
				Software for programming commands and game profiles
				
				Switches New Age
				Instead of conventional mechanical switch conductive contacts used revolutionary optical sensing alone is pressed. That, to get the signal to noise ratio (causing double-pressing) and press the record exactly. The switches also subject to far less time because they have nothing to oxidize and do not mind settling of dust. Above all, however, it achieves the above mentioned reaction is 0.2 ms, which may be decisive in tense matches.",
    		'price' => "1990",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 20,
    		'id_category' => 39,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 8,
	    	'sold_items' => 3,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\b328.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Natec Genesis Gaming RX39 Backlight",
    		'description' => "It’s obvious – RX39 is a product designed for gamers. Gaming design and additional features cause that for a little money every player can enjoy high quality and reliable equipment.

				ANTI-GHOSTING
				The keyboard is equipped with keys with the anti-ghosting function, which prevents locking them by pressing multiple keys simultaneously. This allows for undisturbed entertainment and significantly increases comfort.
				 
				BACKLIGHTED KEYS
				Highlighting in 3 modes ensure that playing will be as comfortable and precise regardless of the time of the day. It’s also a chance to adapt the equipment to your own style.
				
				SUPPORT FOR WRISTS
				Natec Genesis pays particular attention to the comfort of players. RX39 has not only ergonomic design, but also a special place for wrists. This will ensure complete comfort, even during long hours of playing.
				
				WINDOWS KEY LOCK
				RX39 has the function of blocking the Windows key, which is especially favorite among the players. It prevents accidental game stops and exit to the desktop.
				
				ADDITIONAL MEDIA BUTTONS
				Quick access to the media during the game? Intuitively placed multimedia buttons make using the keyboard even more comfortable and media control much easier.
				
				RELIABLE CABLE
				Do not worry about entanglemented or damaged cable. It’s textile coated, so it becomes extremely strong and safe. RX39 Keyboard is an investment for many exciting games.",
    		'price' => "1280",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 21,
    		'id_category' => 39,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 4,
	    	'sold_items' => 1,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\nkg0710-2.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Logitech Business K120 USB Black Slim EN",
    		'description' => "Keyboard Connectivity Technology: Wired
				Keyboard Cable Length: 1.50 m
				
				Keyboard Features: 
				-Low-profile Keys
				-Quiet Keys
				-Spill Resistant
				-Slim",
    		'specifications' => "Interfaces/Ports
				-Keyboard Host Interface: USB
				
				Physical Characteristics
				-Dimensions: 23.50 mm Height x 450 mm Width x 155 mm Depth
				-Weight (Approximate): 550 g
				
				Miscellaneous
				-Green Compliance: Yes
				-Green Compliance Certificate/Authority: 
				-RoHS
				-WEEE",
    		'price' => "750",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 1,
    		'id_category' => 39,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 32,
	    	'sold_items' => 15,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\920002479.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Natec Genesis Gaming R33",
    		'description' => "HIGH QUALITY IN REASONABLE PRICE
				Natec Genesis R33 is a gaming keyboard is made for players, who are looking for a device with good quality and reasonable price. Despite the small price, it still has all the basic features that are essential for gamers. This is an extremely durable device dedicated for those who pay attention into strength and easiness of use.
				
				DURABLE, GAMING KEY MEMBRANES
				Genesis R33 has very good quality of membrane keys, therefore is great for gaming. Most of typical, office keyboards have poor quality keys, and that makes then not suitable for gaming. Thats why R33 uses high quality membranes, with long life its perfect for intense gaming!
				
				CLASSIC SETUP
				Classic keys shape make the keyboard easy and quick to use, and classical keyboard setup should be appreciated by all fans of PC gaming.
				
				EASY ACCESS TO MULTIMEDIA
				Natec Genesis R33 has 9 multimedia keys, which gives you easy access to multimedia functions on your PC. Without any problem, you can turn your computer to the center of home entertainment. Opening your favorite music player or controlling volume level, are just some of the features. With R33, using your computer as media center is easy like never before!
				
				SPILL RESISTANT
				Special, draining holes at the bottom of the keyboard protect it from any water splash damage.
				
				ADDITIONAL GAMING KEY CAPS
				Its obvious for every active gamer, that WASD and arrows keys are used frequently in gaming hence the included 8, additional keycaps for these keys. Rubberized surface and embossed inscriptions give longer life to the keys and substantially increases comfort of playing.
				
				GOOD QUALITY AND DURABLE CONSTRUCTION
				Stable construction and good quality materials, and gold-plated USB connector are making the device durable and solid.",
    		'price' => "690",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 21,
    		'id_category' => 39,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 18,
	    	'sold_items' => 6,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\nkg0281.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\nkg0281-2.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Microsoft Comfort Curve 3000 1482",
    		'description' => "Key Features
				-Contour: It’s familiar, yet modern: All the keys are the same size and in a familiar place, even with the Comfort Curve. The slim, glossy design saves space and makes a statement on your desktop.
				-Ergonomist-approved Comfort Curve design: The Microsoft Comfort Curve encourages natural wrist posture, plus it is easy to use.
				-Easy-Access Media Keys: Control your music and videos, and open the Calculator with the touch of a key.",
    		'price' => "690",
    		'num_of_visits' => 14,
    		'recomended' => 0,
    		'id_brand' => 2,
    		'id_category' => 39,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 4,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\3tj00015.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Delux KA150 Multimedia USB Black EN/MK layout",
    		'description' => "Flat Key Design
				Color: Black
				USB Type
				12 Multimedia Keys
				Laser Print Characters
				Unique Appearance
				
				EN/MK Layout",
    		'price' => "330",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 22,
    		'id_category' => 39,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 54,
	    	'sold_items' => 22,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\210420161355002121.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	//Glufcinja
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "A4 V7M Bloody Gaming USB Black Metal Xglide",
    		'specifications' => "Main core: Intelligent 3 cores
				Number of Buttons: 7 buttons + 1 wheel 
				Encoder: High precision optical engine
				Transmission: Wired
				Connector: USB (2.0 / 3.0)
				System supports: Windows XP / Vista / 7 / 8
				Cable length: 1.8m
				
				GAMING PERFORMANCE
				Maximum Resolution: 3,200 dpi adjustable
				Frame Capacity: 3.68M pixels / sec
				Accelerating Speed: 30g
				Tracking speed: 75 inches / sec (ips)
				Report Rate(USB): 125～1,000Hz/sec (3-level user selectable)
				Key Response time: less than 1ms
				Memory: 160K bits",
    		'price' => "1090",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 20,
    		'id_category' => 40,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 26,
	    	'sold_items' => 7,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\180920141247451.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Natec Genesis Gaming GX75 7200DPI USB",
    		'description' => "Natec Genesis GX75 is a professional mouse designed for gamers. What makes it special is the precise optical sensor and attractive gaming design. 

				IDEAL FOR SHOOTING GAMES
				GX75 has been created for FPS gamers. Proof of this is optical sensor with an ultra-low LOD (Lift of Distance) parameter, which also works great while playing on low DPI level. This sensor is also highly resistant to signal loss which ensures smooth and precise movements, which are particularly important in shooting games. 
				
				EFFECTIVE KILLER
				The mouse is equipped with a high-precision optical sensor PMW3310 with maximum resolution up to 7200 DPI and ability to change DPI sensitivity ON-THE-FLY in four levels. To each DPI level you can set one of 16 mil. backlight colors which will inform about current DPI level. In addition, the sensor offers the report rate from 125 to 1000 Hz, which ensures the fast response time and uninterrupted gameplay. Advanced software also allows to set sensitivity of sensor from 400 to 7200 DPI.
				
				UNIQUE DESIGN 
				Unique shape of the mouse Genesis GX75 is designed for players who use the handle of mouse PALM GRIP. It is distinguished by ergonomic shape and large side buttons, which ensures that even several hours of using the mouse will be still comfortable and pleasured. Proof of unique design is care taken for every detail, backlight of some elements and metal finish in back part are provide an original and attractive appearance, which will help you to stand out against other players.
				
				DURABILITY
				Mouse Natec Genesis is characterized by using of high quality materials and proof of this is to use of durable main button switches of brand OMRON which provide reliable operation with up to 10 million clicks.",
    		'price' => "1990",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 21,
    		'id_category' => 40,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 7,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\nmg0706.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\nmg0706-2.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "A4 V8M Gaming Bloody Multi Core",
    		'description' => "Multi-Core Gaming Mouse GUN3, metal feet, non-activated

				Features
				Bloody - the first “Multi-Core Gun3” gaming mice.
				BLOODY Gun3 - The Multi-Core design has 6 breakthrough innovations with 3 shooting modes to ensure the Gamers to win their games easily.
				Bloody gaming mice are the world's most accurate shooting mice with auto recoil suppression and concentrated trajectory, which offers  unprecedented high headshot rates. 
				Supports both software and hardware dual trajectory adjustments.
				Edit your own macros with Oscar Macro Editing software",
    		'specifications' => "Basic Parameters:
				Multi-core system
				Button No.: 7 keys + Wheel
				Direction of wheel: one way direction
				Tracking: HoleLESS HD engine
				Type: Wired
				Mouse connection: USB (2.0/3.0)
				Hardware system requirements: Windows
				Software system requirements: Windows XP / Vista / Windows 7 / Windows 8
				Mouse size: 125 x 64 x 39 (mm)
				Cable length: 1.8 m
				Mouse weight: 155 g
				Technical Parameters:
				Resolution: 200 Dpi to 3,200Dpi (5 ranges adjustable)
				Image processing: 368 mega pixels / sec
				Acceleration: 30g
				Tracking speed: 75 inches / sec
				Report rate : 1,000 Hz(4 ranges adjustable)
				Key response time: 1ms
				160K onboard memory",
    		'price' => "1190",
    		'num_of_visits' => 18,
    		'recomended' => 0,
    		'id_brand' => 20,
    		'id_category' => 40,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 4,
	    	'sold_items' => 3,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\v8a.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Omega VARR Gaming 800-2400DPI USB w/Mouse Pad",
    		'description' => "High-performance Optical Gaming Mouse + gaming mouse pad for free! 
				The latest from Omega - Gaming Mouse with professional solutions. 
				The special coating provides a secure grip mouse and prevents hands sweating even during the longest gaming session. 
				Switchable resolution 800-1200-1600-2400dpi - sniper precision of optical sensor and super-fast response time! The mouse sides are highlighed/illuminated in several colors. 
				High quality workmanship, braided cable and gold-plated plug ensure long-term use. ",
    		'specifications' => "Model: OM0266 
				Buttons: 6 
				Resolution: 800-1200-1600-2400dpi
				Sensor: high precision gaming class optical sensor
				Additional features: LED illumination, special anti-sweating coating",
    		'price' => "490",
    		'num_of_visits' => 0,
    		'recomended' => 1,
    		'id_brand' => 5,
    		'id_category' => 40,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 0,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\om0266.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Verbatim Go Ergo Desktop Black",
    		'description' => "Modern, easy-grip design with convenient thumb support
				1000dpi high definition optical lens
				USB Plug ‘n’ Play",
    		'specifications' => "Dimensions: 116mm x 76mm x 41mm (L x W x H)
				Weight: 95 grams
				Interface: USB
				dpi (dots per inch): 1000
				Pack Contents:
				Optical Desktop Mouse
    				
    			System Requirements
				Windows 7, Vista, XP, 2000
				Mac OS X 10.4 or higher",
    		'price' => "480",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 10,
    		'id_category' => 40,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 15,
	    	'sold_items' => 10,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\49017globalnopackaginggrip.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "MUSG-001-G Gaming Optical Green 2400DPI USB",
    		'description' => "Gaming mouse, green

				DESIGNED FOR TRUE GAMERS !
				Optimal comfort for intense and extended gameplay
				2400 DPI - 3 x more precision than standard mice
				Non-slip rubberized ergonomic design
				
				Features
				 6-button 2400 DPI gaming mouse
				 Quick DPI button (600-2400 dpi) 
				 Illuminated scroll wheel, logo and side accents 
				 Comfortable ergonomic design 
				 Rubber coated top for more grip and control
				 Practical tangle free nylon mesh cable",
    		'specifications' => "Interface: USB 2.0
				Optical resolutions: 400, 800, 1200, 1600, 2400 DPI
				Ambidextrous: yes
				Button life time: 5 million clicks
				Cable: nylon tangle free
				Cable length: 1.3 m
				Dimensions: 130 x 72 x 41 mm
				Net weight: 121 g
				System requirements
				Windows XP/Vista/7/8
				Free USB port",
    		'price' => "360",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 6,
    		'id_category' => 40,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 9,
	    	'sold_items' => 9,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\musg001g.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "MUS-102 Optical Black 1600DPI USB",
    		'description' => "Optical mouse, USB, black

				COMFORT AND PRECISION
				Compact & lightweight design
				Plug & Play - no drivers required
				No mouse pad required
				
				Features
				3-button optical mouse
				Adjustable mouse resolution: 800 ... 1600 DPI
				Works on most surfaces",
    		'specifications' => "Interface: USB
				Optical resolution: 800-1200-1600 DPI
				Power consumption: 5 V DC up to 5 mA
				Dimensions: 106 x 57 x 35 mm
				Net weight: 72 g
				
				System requirements
				Windows XP/VISTA/7/8
				Free USB port",
    		'price' => "240",
    		'num_of_visits' => 4,
    		'recomended' => 0,
    		'id_brand' => 6,
    		'id_category' => 40,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 21,
	    	'sold_items' => 19,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\mus-102.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Omega OM-412 1000DPI Black",
    		'description' => "Ergonomic optical computer mouse with 1000dpi high precision sensor. Symetric design good for both right and left handed. Shinning surface 

				Main features:
				- resolution - true 1000dpi: provides high tracking precision. No mouse pad is required. 
				- cable's length: 140cm 
				- highly durable micro switches of buttons - with millions of clicks lifetime 
				Type: computer optical mouse 1000DPI 
				- 3 buttons + scrolling wheel
				Cable's length: 1,40 m 
				Resolution: true 1000 dpi 
				Connection: USB plug 
				Packaging: blister 
				additional features:: symetric design - perfect for both right and left-handed",
    		'price' => "170",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 5,
    		'id_category' => 40,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 65,
	    	'sold_items' => 43,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\om0412cb.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	//Pecatari i skeneri
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "HP LaserJet Color Pro Printer M252n",
    		'specifications' => "Print technology Laser

				Print speed black:Normal: Up to 18 ppm 5 
				Print speed color: Normal:Up to 18 ppm 5 
				
				First page out (ready)
				Black: As fast as 11.5 sec
				color: As fast as 13 sec 6 
				
				Print quality black (best) Up to 600 x 600 dpi
				Print quality color (best) Up to 600 x 600 dpi
				
				Duty cycle (monthly, A4) Up to 30,000 pages 7 
				Recommended monthly page volume 250 to 2500
				
				Display 2-line LCD (text)
				
				Connectivity, standard
				Hi-Speed USB 2.0 port
				built-in Fast Ethernet 10/100Base-TX network port",
    		'price' => "9590",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 23,
    		'id_category' => 41,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 18,
	    	'sold_items' => 6,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\020620151542181111.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "HP LaserJet Color Pro MFP M177fw",
    		'specifications' => "Print technology: Laser
				Functions: Print, copy, scan, fax
				Multitasking supported
				
				Print speed black: Normal: Up to 17 ppm
				Print speed color: Normal:Up to 4 ppm
				
				First page out (ready)
				Black: As fast as 16 sec
				color: As fast as 27.5 sec
				
				Duty cycle (monthly, A4) Up to 20,000 pages
				
				Recommended monthly page volume 250 to 950
				Print technology Laser
				
				Print quality black (best)
				Up to 600 x 600 dpi quality (2400 dpi effective output with ImageREt 2400)
				Print quality color (best)
				Up to 600 x 600 dpi quality (2400 dpi effective output with ImageREt 2400)
				
				Print languages PCLm/PCLmS
				Display 3.0-in (7.62-cm) touchscreen CGD (Color Graphics Display)
				Processor speed 600 MHz
				
				Wireless capability
				Yes, built-in WiFi 802.11b/g/n
				
				Connectivity, standard
				Hi-Speed USB 2.0 port
				built-in Fast Ethernet 10/100Base-TX network port
				Wireless 802.11b/g/n
				
				Scanner type Flatbed, ADF
				Fax resolution Up to 300 x 300 dpi",
    		'price' => "15490",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 23,
    		'id_category' => 41,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 4,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\cz165a.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "HP LaserJet Pro 200 M225dnf MFP",
    		'specifications' => "Print, copy, scan, fax
				Print speed black: Normal: Up to 25 ppm 
				First page out (ready)
				    Black: As fast as 8 sec
				Duty cycle (monthly, A4)
				    Up to 8,000 pages 7  
				Recommended monthly page volume
				    500 to 2,000 8  
				Print technology
				    Laser 
				Print quality black (best)
				    Up to 600 x 600 dpi 
				Display
				  2-line LCD (text)
				Processor speed
				    600 MHz 
				Automatic paper sensor
				    No 
				HP ePrint capability
				    Yes 
				Mobile printing capability
				    HP ePrint, Apple AirPrint™, Mopria-certified 
				Wireless capability
				        No
				Connectivity, standard
				        Hi-Speed USB 2.0 port (host/device)
				        Built-in Fast Ethernet 10/100Base-TX network port
				        Phone line port (in/out)
				Network ready
				    Standard (built-in Ethernet)
				Memory, standard
				    256 MB ",
    		'price' => "12790",
    		'num_of_visits' => 10,
    		'recomended' => 0,
    		'id_brand' => 23,
    		'id_category' => 41,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 4,
	    	'sold_items' => 4,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\030920150932281.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "HP Laser Jet P1102",
    		'specifications' => "Print technology Laser
				Print speed black (normal, A4) Up to 18 ppm
				First page out black (A4, ready) As fast as 8.5 sec
				Print quality black (best) Up to 600 x 600 x 2 dpi (1200 dpi effective output)
				Duty cycle (monthly, A4) Up to 5000 pages
				Recommended monthly page volume 250 to 1500
				Memory 2 MB
				Processor speed 266 MHz
				Standard paper trays 1
				Paper handling standard/input 150-sheet input tray
				Duplex print options Manual (driver support provided)
				Media sizes supported
				A4; A5; A6; B5; postcards; envelopes (C5, DL, B5)
				
				Connectivity
				Hi-Speed USB 2.0 port
				
				Compatible operating systems
				Microsoft® Windows® 7 (32-bit/64-bit), Windows Vista® (32-bit/64-bit), Windows® XP (32-bit/64-bit), Windows® Server 2008 (32-bit/64-bit), Windows® Server 2003 (32-bit/64-bit), Mac OS X v 10.4, v 10.5, v 10.6, Linux",
    		'price' => "4590",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 23,
    		'id_category' => 41,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 4,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\printer2.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Canon Laser Jet LBP6030B black",
    		'specifications' => "Printing Method 	Monochrome Laser Beam Printing
				Print Speed	 A4:	Up to 18ppm
				Letter: 	Up to 19ppm
				Print Resolution 	600 x 600dpi
				Print Quality with Image Refinement Quality	2400 (equivalent) × 600dpi
				Warm-Up Time 
				(From Power On) 	10secs. or less
				First Printout Time (FPOT) 	Approx. 7.8secs.
				Recovery Time 
				(From Sleep Mode)	Approx. 1secs.
				Print Language 	UFR II LT
				Paper Handling	 
				Paper Input (Standard)	150 sheets (based on 80g/m2)
				Paper Output	100 sheets (face down)
				(based on 80g/m2)
				Paper Size	A4, B5, A5, Legal*1, Letter, Executive, 16K, Envelope COM10, Envelope Monarch, Envelope C5, Envelope DL
				Custom: Width 76.2 to 215.9mm x Length 188 to 355.6mm
				Paper Weight	60 to 163g/m2
				Paper Type	Plain paper, Heavy Paper, Transparency, Label, Envelope.
				Connectivity and Software	 
				USB Interface	USB 2.0 High Speed
				Compatible Operating Systems*2	Win 8.1 (32 / 64bit), Win 8 (32 / 64bit), Windows 7 (32 / 64-bit), Windows Vista (32 / 64-bit), Windows XP (32 / 64-bit), Windows Server 2012 (32 / 64bit), Windows Server 2012 R2 (64 bit), Windows Server 2008 (32 / 64-bit), Windows Server 2008 R2 (64-bit), Windows Server 2003 (32 / 64bit), 
				Mac OS 10.6.x~10.9*3, Linux*3, Citrix
				General Specification	 
				Operation Panel	2 LED Indicators, 2 Operation Key
				Device Memory	32MB
				Dimensions (W x D x H)	364 x 249 x 199mm
				Weight	Approx. 5.0kg (Without Cartridge)
				Noise Level*4	During Operation:	Sound Power Level:	6.53B or less
				Sound Pressure Level:	49.3dB
				During Standby:	Sound Power Level:	Inaudible*5
				Sound Pressure Level:	Inaudible*5
				Maximum Power Consumption	840W or less
				Average Power Consumption	During Operation:	Approx. 320W
				During Standby:	Approx. 1.8W (USB connection)
				During Sleep:	Approx. 0.8W (USB connection)
				Energy Star TEC 
				(Typical Electricity Consumption)	0.48kWh/week
				Operating Environment	Temperature:	10 to 30°C
				Humidity:	20% to 80% RH (no condensation)
				Power Requirement	220 - 240V (±10%), 50 / 60Hz (±2Hz)
				Toner Cartridge*6	Cartridge 925:	1,600 pag",
    		'price' => "4390",
    		'num_of_visits' => 0,
    		'recomended' => 1,
    		'id_brand' => 24,
    		'id_category' => 41,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 9,
	    	'sold_items' => 1,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\050220151132201.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "EPSON L365 Inkjet w/ Ink Tank System (CISS)",
    		'specifications' => "All-in-One Functions 	Print, Scan, Copy

				Pinting Method 	On-demand inkjet (Piezo electric)
				Nozzle Configuration 	180 Nozzles Black, 59 Nozzles per Color
				Minimum Droplet Size 	3 pl, With Variable-Sized Droplet Technology
				Ink Technology 	Dye Ink
				Printing Resolution 	5,760 x 1,440 DPI
				
				All-in-One Functions 	Print, Scan, Copy
				
				Printing Speed ISO/IEC 24734 	9.2 Pages/min Monochrome, 4.5 Pages/min Color
				Printing Speed 	15 Pages/min Color (plain paper 75 g/m²), 33 Pages/min Monochrome (plain paper 75 g/m²), 69 Seconds per 10 x 15 cm photo (Epson Premium Glossy Photo Paper)
				Colours 	Magenta, Yellow, Cyan, Black
				
				Single-sided scan speed (A4 black) 	300 DPI 2.4 msec/line; 600 DPI 7.2 msec/line
				Single-sided scan speed (A4 colour) 	300 DPI 9.5 msec/line; 600 DPI 14.3 msec/line
				Scanning Resolution 	1,200 DPI x 2,400 DPI (Horizontal x Vertical)
				Output formats 	JPEG, PDF
				Scanner type 	Contact image sensor (CIS)
				
				Number of paper trays 	1
				Paper Formats	A4, A5, A6, B5, C6 (Envelope), DL (Envelope), No. 10 (Envelope), Letter, 10 x 15 cm, 13 x 18 cm, 16:9, User defined, Legal
				Duplex	Manual
				Output Tray Capacity 	30 Sheets
				Paper Tray Capacity 	100 Sheets Standard, 100 Sheets Maximum, 20 Photo Sheets
				
				Interfaces 	WiFi, USB",
    		'price' => "10290",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 25,
    		'id_category' => 41,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 5,
	    	'sold_items' => 2,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\030620151630412222.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "EPSON L220 InkJet All-In-One w/ Ink Tank System (CISS)",
    		'specifications' => "Printing Method	On-demand inkjet (Piezo electric)
				Nozzle Configuration	180 Nozzles Black, 59 Nozzles per Color
				Minimum Droplet Size	3 pl, With Variable-Sized Droplet Technology
				Ink Technology	Dye Ink
				Printing Resolution	5,760 x 1,440 DPI
				Category	Consumer
				All-in-One Functions	Print, Scan, Copy
				
				PRINT
				Printing Speed ISO/IEC 24734	7 Pages/min Monochrome, 3.5 Pages/min Color
				Printing Speed	69 Seconds per 10 x 15 cm photo (Epson Premium Glossy Photo Paper), 15 Pages/min Color (plain paper 75 g/m²), 27 Pages/min Monochrome (plain paper 75 g/m²)
				Colours	Black, Cyan, Yellow, Magenta
				
				SCAN
				Single-sided scan speed (A4 black)	300 DPI 2.4 msec/line; 600 DPI 7.2 msec/line
				Single-sided scan speed (A4 colour)	300 DPI 9.5 msec/line; 600 DPI 14.3 msec/line
				Scanning Resolution	600 DPI x 1,200 DPI (Horizontal x Vertical)
				Output formats	BMP, JPEG, TIFF, PDF
				Scanner type	Contact image sensor (CIS)
				
				PAPER / MEDIA HANDLING
				Number of paper trays	1
				Paper Formats	A4, A5, A6, B5, C6 (Envelope), DL (Envelope), No. 10 (Envelope), Letter, 10 x 15 cm, 13 x 18 cm, 16:9, User defined, Legal
				Duplex	Manual
				Output Tray Capacity	30 Sheets
				Paper Tray Capacity	50 Sheets Standard, 50 Sheets Maximum, 10 Photo Sheets
				
				GENERAL
				Energy Use	12 Watt (standalone copying, ISO/IEC 24712 pattern), 0.8 Watt (sleep mode), 3.8 Watt (ready), 0.3 Watt (Power off),  ENERGY STAR® qualified
				Supply Voltage	AC 100 V - 240 V,50 Hz - 60 Hz
				Product dimensions	482‎ x 300 x 145 mm (Width x Depth x Height)
				Product weight	4.3 kg
				Noise Level	5.2 B (A) with Epson Premium Glossy Photo Paper / Photo RPM mode - 38 dB (A) with Epson Premium Glossy Photo Paper / Photo RPM mode
				Compatible Operating Systems	Mac OS 10.7.x, Mac OS 10.8.x, Mac OS 10.9.x, Mac OS X 10.6.8 or later, Windows 7, Windows 7 x64, Windows 8, Windows 8 (32/64 bit), Windows 8.1, Windows 8.1 x64 Edition, Windows Vista, Windows Vista x64, Windows XP, Windows XP x64, XP Professional x64 Edition
				Included Software	Epson Scan
				Interfaces	USB",
    		'price' => "9290",
    		'num_of_visits' => 11,
    		'recomended' => 0,
    		'id_brand' => 25,
    		'id_category' => 41,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 4,
	    	'sold_items' => 4,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\15032016140642999.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "EPSON L130 Inkjet w/ Ink Tank System (CISS)",
    		'specifications' => "Printing Method 	On-demand inkjet (Piezo electric)
				Nozzle Configuration 	180 Nozzles Black, 59 Nozzles per Color
				Minimum Droplet Size	3 pl, With Variable-Sized Droplet Technology
				Ink Technology 	Dye Ink
				Printing Resolution 	5,760 x 1,440 DPI
				
				All-in-One Functions 	Print
				
				Printing Speed ISO/IEC 24734 	7 Pages/min Monochrome, 3.5 Pages/min Color
				Printing Speed 	27 Pages/min Monochrome (plain paper 75 g/m²), 15 Pages/min Color (plain paper 75 g/m²), 69 Seconds per 10 x 15 cm photo (Epson Premium Glossy Photo Paper)
				Colours	Magenta, Yellow, Cyan, Black
				
				Number of paper trays 	1
				Paper Formats 	A4, A5, A6, B5, C6 (Envelope), DL (Envelope), No. 10 (Envelope), Letter, 9 x 13 cm, 10 x 15 cm, 13 x 18 cm, 16:9, User defined, Legal
				Duplex	Manual
				Output Tray Capacity 	30 Sheets
				Paper Tray Capacity 	50 Sheets Standard, 50 Sheets Maximum, 10 Photo Sheets
				
				Interfaces 	USB",
    		'price' => "7590",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 25,
    		'id_category' => 41,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 10,
	    	'sold_items' => 3,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\03062015162629111.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Canon InkJet PIXMA MG5550 All-in-one",
    		'specifications' => "Functions Wireless Print, Copy, Scan, Cloud Link
				Print Resolution Up to 4800¹ x 1200 dpi
				Print Technology 5 Individual Ink Tanks (PGBK, BK, C, M, Y)
				Inkjet FINE print head with 2pl (min.) ink droplet size
				ChromaLife100+ inks
				Mono Print Speed Approx. 12.2 ipm¹
				Colour Print Speed Approx. 8.7 ipm¹
				Photo Print Speed Borderless 10x15cm : Approx. 44 seconds¹
				Borderless Printing Yes (A4, Letter, 20x25cm, 13x18cm, 10x15cm)
				Two Sided Printing Auto Duplex Print (A4, A5, B5, Letter)
				
				Paper Weight Plain paper: 64 - 105 g/m²
				Canon photo paper: up to 300 g/m²
				
				Scanner Type CIS flatbed photo and document scanner
				Scanner Resolution (Optical) 1200 x 2400 dpi¹
				A4 Scan Speed Approx. 14 seconds¹
				Scanning Depth (Input / Output) Colour: 48 bit / 24 bit
				Greyscale: 16 bit / 8 bit
				Maximum Document Size 216 x 297 mm
				
				Copy Speed sFCOT: Approx. 20 seconds¹
				sESAT: Approx. 7.2 ipm¹
				Multiple Copy 1-99 copies
				Copy Functions Document Copying, Photo Copying, Borderless copy, 2-sided copy, 2-on-1 and 4-on-1 copy, Frame erase copy
				Copy Zoom 25 - 400% (in 1% increments)
				Fit to page
				A5 to A4, B5 to A4, A4 to A5 and A4 to B5
				
				Display Type & Size 6.2cm LCD (TFT)
				Interface Type - PC / Mac Hi-Speed USB (B Port) 
				Wi-Fi: IEEE802.11 b/g/n
				Wi-Fi Security: WPA-PSK, WPA2-PSK, WEP, Administration password",
    		'price' => "5490",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 24,
    		'id_category' => 41,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 4,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\200320140940581.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "Xerox InkJet Phaser 6000",
    		'specifications' => "Speed Up to 10 ppm colour / 12 ppm black and white
				Duty Cycle Up to 30,000 pages / month
				
				Paper Handling
				Paper input
				Main Tray:
				150 sheets, Custom sizes: 76.2 × 127 mm to 215.9 × 355.6 mm
				
				Paper output 100 sheets
				Two-sided printing Manual
				
				Print 
				First-page-out-time
				As fast as 17 seconds colour / 14 seconds black and white
				Resolution (max) 600 × 600 × 4 dpi
				Processor 192 MHz
				Memory 64 MB
				
				Connectivity USB 2.0
				
				Page description languages Host-Based",
    		'price' => "5480",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 4,
    		'id_category' => 41,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 2,
	    	'sold_items' => 1,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\phaser-6000.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "HP DeskJet 2135 All-In-One Ink Advantage",
    		'specifications' => "Functions Print, copy, scan

				Print speed black:
				ISO: Up to 7.5 ppm
				Draft: Up to 20 ppm
				
				Print speed color:
				ISO:Up to 5.5 ppm
				Draft:Up to 16 ppm
				
				First page out (ready)
				Black: As fast as 15 sec
				color: As fast as 18 sec
				
				Duty cycle (monthly, A4) Up to 1,000 pages
				Recommended monthly page volume 50 to 200
				
				Print technology HP Thermal Inkjet
				
				Print quality (best)
				color: Up to 4800 x 1200 optimised dpi colour (when printing from a computer on selected HP photo papers and 1200 input dpi)
				Black: Up to 1200 x 1200 rendered dpi
				
				Number of print cartridges 2 (1 each black, tri-colour)
				
				Connectivity, standard
				1 Hi-Speed USB 2.0
				
				Scanner type Flatbed
				Scan file format JPEG, TIFF, PDF, BMP, PNG
				Scan resolution, optical Up to 1200 dpi
				Bit depth 24-bit
				
				Copy resolution (black text) Up to 600 x 300 dpi
				Copy resolution (color text and graphics) Up to 600 x 300 dpi",
    		'price' => "2550",
    		'num_of_visits' => 5,
    		'recomended' => 0,
    		'id_brand' => 23,
    		'id_category' => 41,
    	]);
    	 
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 4,
	    	'sold_items' => 0,
	    	'adding_date' => '2016-09-24',
    	]);
    	 
    	ProductImage::create([
	    	'url' => 'uploads\images\2910201512432211.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "HP Office Jet 7510 Wide Format A3 All-in-One WiFi",
    		'specifications' => "Print technology HP Thermal Inkjet
				Functions Print, copy, scan, fax
				
				Print speed black:
				ISO: Up to 15 ppm
				Draft: Up to 33 ppm
				
				Print speed color:
				ISO:Up to 8 ppm
				Draft:Up to 29 ppm
				
				Duty cycle (monthly, A4) Up to 12,000 pages
				Recommended monthly page volume 200 to 800 
				
				Print quality (best)
				color: Up to 4800 x 1200 optimised dpi colour (when printing from a computer on selected HP photo papers and 1200 input dpi)
				Black: Up to 600 x 1200 dpi
				
				Display 6.73 cm (2.65\") LCD (colour) with IR touchscreen function
				Number of print cartridges 4 (1 each black, cyan, magenta, yellow)
				
				Connectivity 
				1 Hi-Speed USB 2.0
				1 Ethernet
				1 Wireless 802.11b/g/n
				1 USB Host
				1 RJ-11 fax
				
				Scanner type Flatbed, ADF
				Scan resolution, optical Up to 1200 dpi
				Bit depth 24-bit
				
				Copy speed (draft)
				Black: Up to 33 cpm
				color: Up to 29 cpm 1 
				Copy resolution (black text) Up to 600 x 1200 dpi
				Copy resolution (color text and graphics) Up to 4800 x 1200 optimised dpi colour (when printing from a computer on selected HP photo papers and 1200 input dpi)
				
				Faxing Yes, colour
				Fax transmission speed 4 sec per page 3 
				Fax memory Up to 100 pages 2 ",
    		'price' => "8790",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 23,
    		'id_category' => 41,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 4,
	    	'sold_items' => 2,
	    	'adding_date' => '2016-09-24',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\181220151521311.jpg',
	    	'id_product' => $product->id,
    	]);
    }
}
