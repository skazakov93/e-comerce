<?php

use Illuminate\Database\Seeder;
use App\Modules\Products\Product;
use App\Modules\Stocks\Stock;
use App\Modules\ProductImages\ProductImage;

class TVProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
    	
    	////////////////////
    	$product = Product::create([
    		'name' => "LED PHILIPS 49PUS6401/12",
    		'specifications' => "4K Ultra Slim LED TV powered by Android TV with Ambilight 2-sided
4K Resolution, 3840x2160
Picture Performance Index 1000
Micro Dimming Pro, PixelPlus ULTRA HD
Ambilight 2 Sided
QUAD Core Processoe
Google Play Apps, Clound Gaming
HDMI x4, 3x USB 2.0, LAN, Wi-Fi Built In 2x2 Integrated
DVB-T/T2/C/S/S2, MPEG4
OS: Android 5.1 LoliPop",
    		'price' => "44990",
    		'num_of_visits' => 0,
    		'recomended' => 0,
    		'id_brand' => 12,
    		'id_category' => 13,
    	]);
    	
    	Stock::create([
	    	'id_product' => $product->id,
	    	'id_warehouse' => 1,
	    	'num_of_items' => 9,
	    	'sold_items' => 1,
	    	'adding_date' => '2016-09-27',
    	]);
    	
    	ProductImage::create([
	    	'url' => 'uploads\images\img.jpg',
	    	'id_product' => $product->id,
    	]);
    	
    	////////////////////
    	$product = Product::create([
			'name' => "LED PHILIPS 32PFH5501/88",
			'specifications' => "Full HD LED TV powered by Android TV
Resolution, 1920x1080
Picture Performance Index 500
Micro Dimming, PixelPlus HD
Dual Core Processor, 8 GB memory and expandable
Google Play Apps, Clound Gaming, Google Cast
HDMI x4, 3x USB 2.0, LAN, Wi-Fi Built In 2x2 Integrated
DVB-T/C, MPEG4
OS: Android",
			'price' => "21990",
			'num_of_visits' => 0,
			'recomended' => 0,
			'id_brand' => 12,
			'id_category' => 13,
		]);
    	 
    	Stock::create([
        	'id_product' => $product->id,
        	'id_warehouse' => 1,
        	'num_of_items' => 9,
        	'sold_items' => 8,
        	'adding_date' => '2016-09-27',
    	]);
    	 
    	ProductImage::create([
        	'url' => 'uploads\images\img (1).jpg',
        	'id_product' => $product->id,
    	]);

        ////////////////////
        $product = Product::create([
            'name' => "LCD SONY KDL50W805BBAE2",
            'specifications' => "50\" 3D Smart LED Full HD TV, 1920x1080p
X-Reality Pro
Motion Flow XR 400 Hz
SOny Entertainment Network + Apps, Web Browser
Sence of Quartz Design
Wi-Fi built in, DLNA, Skype Ready
HDMI x4, PC Input
USB 2.0 Multimedia (Movies, Photos, Music)
DVB-T,DVB-T2,  DVB-C, DVB-S2, MPEG4",
            'price' => "41990",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 8,
            'id_category' => 13,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 14,
            'sold_items' => 14,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\img (2).jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "4K UltraHD PHILIPS 49PUH4900/88",
            'specifications' => "49\"(123cm) 4K Ultra HD Slim LED TV, 3840x2160
Perfect Motion Rate 400 Hz 
Pixel Plus Ultra HD
Micro Dimming, Brightness 300 cd/m²
HDMI x3, 2x USB 2.0 Multimedia
Tuner:DVB-T/C",
            'price' => "26990",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 1,
            'id_category' => 13,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 8,
            'sold_items' => 0,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\img (3).jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "TV Hyundai 40\" LE-F D LED FullHD 1920x1080 16:9 HDMI x3/USB/Scart/DVB-C-T",
            'specifications' => "ЛЕД Телевизор, дијагонала 102 см (40“)
- Direct LED - Позадинско осветлување
- Full HD 
- Резолуција 1920x1080
- Aspect ratio 16:9 (wide screen)
- Digital Video Broadcasting -Terrestrial / Cable
DVB-T/C MPEG4 H.264
3x HDMI (high definiton multimedia interface)
SCART
USB (picture, music, video) H.264
VGA
PC Audio
AV IN
YPbPr
- Contrast Ratio 3000:1
- Brightness 250 cd/m2
- 3D Combo filter
- Response time 6ms
- Телетекст
- PVR функција - овозможува снимање на USB
- Мени на македонски, српски, албански јазик
- Ултра тенка рамка
- Metal brush look 
- Стаклен држач",
            'price' => "13990",
            'num_of_visits' => 0,
            'recomended' => 1,
            'id_brand' => 26,
            'id_category' => 13,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 3,
            'sold_items' => 3,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\170920151706172.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "TV Samsung UE32J4000 32\" LED HD Ready 1366 x 768 16:9 100Hz HDMI x2/USB/Scart/Optical/DVB-C-T/DTS",
            'specifications' => "4 Series
32 inches  (80cm) Inch 1366 x 768
DVB-T / C DTV Tuner

Video
100 Clear Motion Rate
Image Editor: Hyper Real Engine
Dynamic Contrast Ratio: Mega Contrast
Wide Colour Enhancer (Plus) 
Film Mode available

Audio
10W x 2 Sound Output (RMS)
Dolby Digital Plus
DTS Sound Studio
DTS Premium Audio
Down Firing  + Full Range

Input and output
Digital Audio Out (Optical) x 1 , HDMI x 2, RF input x 1, USB x 1, Headphone x 1, CI Slot x 1

Accessories
Remote control TM1240A",
            'price' => "13990",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 19,
            'id_category' => 13,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 16,
            'sold_items' => 4,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\ue32j4000aw-1.jpg',
            'id_product' => $product->id,
        ]);

        ProductImage::create([
            'url' => 'uploads\images\ue32j4000aw.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "TV Hyundai 40\" LE-FDL LED FullHD 1920x1080 16:9 HDMI x3/USB/Scart/DVB-C-T",
            'specifications' => "- ЛЕД Телевизор, дијагонала 102 см (40“)                   
 - Direct LED - Позадинско осветлување                  
 - Full HD                  
 - Резолуција 1920x1080                 
 - Aspect ratio 16:9 (wide screen)                  
 - Digital Video Broadcasting -Terrestrial / Cable                  
   DVB-T/C MPEG4 H.264                  
 - Response time 6ms                    
 - Refresh rate 60Hz                    
 - Contrast Ratio 3000:1                    
 - Brightness 250 cd/m2                 
 - 3D Combo filter                  
 - Телетекст                    
 - PVR функција - овозможува снимање на USB                 
 - Ултра тенка рамка                    
 - Metal brush look                     
 - Мени на македонски јазик                 Технички спецификации               
                
 - Аналоген прием PAL/SECAM             
 - Дигитален прием DVB-T DVB-C MPEG4 H.264              
 - Потрошувачка max 58W             
 - Потрошувачка во stand by max 0,5W                
 - Растојание на дупки за држач на ѕид 200х200 mm               
 - Димензии на пакување 102 х 18.5 х 63.5 cm                
 - Тежина 11.8 кг               
 - Сертификати, CE/LVD/EMC              
 - EAN 5310147654017                
 - Пак.1/1              
Влезови и излези                
                
 - 3x HDMI (high definiton multimedia interface)                
 - SCART                
 - USB 2.0 (слики, музика, видео) H.264             
 - VGA (компјутерски влез)              
 - PC Audio             
 - AV IN                
 - YPbPr                
 - Earphone (излез за слушалки)             
 - Coaxial Out.             
 - Common Interface",
            'price' => "13490",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 26,
            'id_category' => 13,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 25,
            'sold_items' => 8,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\400(2).jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "TV Toshiba 32E2533DG 32\" LED HD 16:9 HDMI x2 /USB/Scart/DVB-C-T/DTS/Black",
            'specifications' => "Diagonal Screen Size (cm) : 80 
Diagonal Screen Size (inch) : 32 
Visible Area (H x V) : 878 x 485 
Screen Format : 16:9 
Panel Resolution : 1366 x 768 
Brightness (cd/m²) : 300 
Native Contrast
Native Contrast (number) : 5000 
Dynamic Contrast
Dynamic Contrast (number) : 12500 
Contrast (Ultra, Mega, High) : High 
Response Time (G to G) (ms) : 9,5 
Viewing Angle (º) : 178 
LCD Panel
LED TV
LED Edge
HD TV
50Hz
AMR+ : 100 
Digital Noise Reduction
Active Backlight Control
Colour Temperature selectable
Manual picture size select
Exact Scan Mode

Audio
    NICAM Stereo
Dolby® Digital Plus
Stable Sound
Bass Boost
Sound Output (RMS) W : 2 x 7W 

Tuning
    TV Standards : PAL BG/I SECAM DK/L 
Number of Channels : ATV(100), DTV(1000) 
Analogue
DVB-T
DVB-C / DVB-C (HD)
H.264
DVB Common Interface+ (CI+) v1.3
NTSC Video-Playback
Auto Set-up

Interactive Features
    Digital Text
Text Page Memory : 100 
Electronic Programme Guide 8 Day
Favourite Channel Memory : 1000 
USB Features    Picture (USB)
Audio (USB)
Video (USB)
Smart TV    PVR Record (to external USB storage device)
Other Features  Audio Description (UK only)
Freeze Screen
Timer
Panel Lock
Auto Format
4:3 stretch
No Signal Off

Connections
    Number of HDMI® (back) : 1 
Number of HDMI® (side) : 1 
2160p, 1080p, 1080i, 720p, 720i, 576p, 576i, 480p, 480i : 1080p, 1080i, 720p, 720i, 576p, 576i, 480p, 480i 
24Hz (24p) : Yes (HDMI® 1080p) 
HDMI® CEC
Component Video
Composite Video
PC Input
Number of USB : 1 
Digital Audio Out
Headphone

Store Mode
    Digital POP Demo
Power Supply    Power Consumption EN62087 - Home Mode (W) : 32.0 
Power Consumption EN60065 - Maximum power (W) : 51 
Power Consumption - Stand-by (W) : 0.28 
Yearly Energy Consumption (average kWh) : 47 
Eco Energy Label : A+ 
Peak Luminance Ratio (%) : 76 
Presence of Lead (Below RoHS Directive limit including exemptions)
Mercury Content (mg/kg) : <0.1 
Dimensions  Width (mm) : 734 
Height without s",
            'price' => "12990",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 27,
            'id_category' => 13,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 7,
            'sold_items' => 4,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\150920151003011.jpg',
            'id_product' => $product->id,
        ]);

        ProductImage::create([
            'url' => 'uploads\images\150920151003092.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "TV Samsung UE46F5000 46\" LED FullHD 1920x1080p 16:9 100Hz HDMI x2/USB/Scart/Optical/DVB-C-T/DTS",
            'specifications' => "Series:5
46 inches (116cm) FULL HD 1920 x 1080 LED
DVB-T / C DTV Tuner

Video
100 Clear Motion Rate
Image enhancement Electronics: Hyper Real Engine
Dynamic Contrast Ratio: Mega Contrast
Wide Colour Enhancer (Plus) available
Movie mode is available
Natural picture mode available

Audio:
10W x 2 Sound Output (RMS)
Dolby Digital Plus / Dolby Pulse
DTS Sound Studio
DTS Premium Audio
Down Firing + Full Range Speakers

Input and output
Component Input  x 1, Input Composite (AV) x 1, Digital Audio Out (Optical) x 1, HDMI x 2, RF input x 1 , USB x 1, Headphone x 1 

Consumption
0.1 W power consumption (standby)
59 W power consumption (on mode)

Size
Dimensions without stand (W x H x D): 1059.6 x 626.2 x 49.4 mm
Dimensions with stand (W x H x D): 1059.6 x 686.1 x 235 mm

Accessories
TM1240 remote control",
            'price' => "27990",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 19,
            'id_category' => 13,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 21,
            'sold_items' => 6,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\ue32f5000-1.jpg',
            'id_product' => $product->id,
        ]);

        ProductImage::create([
            'url' => 'uploads\images\ue32f5000.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "TV Hyundai 50\" FL-50S327 LED SMART FullHD 1920 x 1080p 300cd/HDMI x2/Scart/USB x2/LAN/Dsub/DVB-T/C/S",
            'specifications' => "Smart TV 
Screen size 50 \"
Resolution 1920 x 1080 (Full HD)
Technology Refresh 400 Hz Clear Motion Picture
Display Technology LED
Dynamic Contrast 1 100 000
Brightness 300 cd / m2
Response Time 5 ms

Built-in tuner DVB-T (Digital terrestrial), 
DVB-C (digital cable), 
DVB-S (Digital Satellite)

Smart Features 
YouTube 
Browser 
Facebook

Tuner functions PVR - video recording function digital 
EPG - Electronic Program Guide on television channels 
TimeShift - stop function and reversing watched programs 
Hbb TV - Smart TV
Wireless Wi-Fi ready (after connecting the adapter - not included), 
DLNA

Number of HDMI connectors 2 pcs.
The number of USB connectors 2 pcs.
CI slot 
Other connectors Euroconnector (SCART) 
Headphone 
Component 
PC input (D-sub 15-pin)
Digital audio output (optical) 
Ethernet (LAN)
Speaker Power 2 x 8

Housing color black
Dimensions (W x H x D) without Stand    66.6 x 113.4 x 8 cm
Dimensions (W x H x D) with Stand   71 x 113.5 x 23 cm
Weight (kg)     14.00 kg

Energy class A
Average power consumption 62
Power consumption (standby) 0.3 W
Additional information Teletext

Included Accessories TV stand 
Remote 
User Manual",
            'price' => "28490",
            'num_of_visits' => 0,
            'recomended' => 1,
            'id_brand' => 26,
            'id_category' => 13,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 12,
            'sold_items' => 0,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\1311201513414211.jpg',
            'id_product' => $product->id,
        ]);

        ProductImage::create([
            'url' => 'uploads\images\131120151343070088dbe4ee72e0df3e_700x700_c1ng.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "TV Samsung UE40J5000 40\" LED Full HD 16:9 HDMI x2/USBx1/Optical/DVB-C-T/DTS",
            'specifications' => "Series 5
Size of the screen 40 \"
Screen Size (cm) 101 cm
Resolution 1920 x 1080
PQI (Picture Quality Index) 200
Dolby Digital Plus Yes
Sound Power (RMS) 20 W (10 W + 10 W)
Tuner DVB-T / C
HDMI 2
USB 1
Component input (Y / Pb / Pr) 1
Composite Input (AV) 1 (Common Use for Component Y)
Audio output (mini jack) No
Optical audio output 1
Antenna: Terrestrial / Cable / Satellite 1/1 (Common Use for Terrestrial) / 0
CI Location 1
MHL No",
            'price' => "21990",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 19,
            'id_category' => 13,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 3,
            'sold_items' => 1,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\fr_ue40j5000awxzf_002_r-perspective_black.jpg',
            'id_product' => $product->id,
        ]);

        ProductImage::create([
            'url' => 'uploads\images\fr_ue40j5000awxzf_001_front_black.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "TV Hyundai 48\" LE-F D LED FullHD 1920x1080 16:9 HDMI x3/USB/Scart/DVB-C-T",
            'specifications' => "- ЛЕД Телевизор, дијагонала 122 см (48“)
- Direct LED - Позадинско осветлување
- Full HD 
- Резолуција 1920x1080
- Aspect ratio 16:9 (wide screen)
- Digital Video Broadcasting -Terrestrial / Cable
DVB-T/C MPEG4 H.264
- Contrast Ratio 3000:1
- Brightness 250 cd/m2
- Response time 6ms
- 3D Combo filter
- Телетекст
- PVR функција - овозможува снимање на USB
- Мени на македонски, српски, албански јазик
- Ултра тенка рамка
- Metal brush look",
            'price' => "21490",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 26,
            'id_category' => 13,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 38,
            'sold_items' => 15,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\170920151706552.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "TV Philips 40PFH4101 40\" FullHD LED HDMI x2/USB/Scart/Optical/DVB-C-T/DTS",
            'specifications' => "Panel:
-LED Panel
-Size: 102cm (40\")
-FullHD resolution
Image:
-Perfect Motion Rate 200Hz
-Pixel Plus HD
-Micro Dimming
-Digital CrystalClear
Audio
-Sound output: 16W Audio Power Output
-Auto Volume Leveller
-Dynamic Bass Enhancement
System
-DTV Tuner: DVB-TC
-CI +
Connectivity
-2 HDMI
-1 DVI
-2 USB
-1 Input Component (Y / Pb / Pr)
-1 Input Composite (AV) (Common Use for Component Y)
-1 Ethernet (LAN)
-1 Headphones
-1 Output digital audio (Optical)
-RF In (Terrestrial / entr cable / satellite entr..):
Additional features
-USB Recording
Design
-Design: Slim
-Stand Design: black
-Eco Energy label A +
Consumption
-Consumption Operational 50W
-Annual consumption 73kWh
-Consumption (standby) 0.3W
Dimensions
-With stand (WxHxD cm) 91,8 x 58,4 x 19,5
-Without pedestal (WxHxD cm) 91,8 x 53,1 x 7,9
Weight
-With stand (kg) 7
-No base (kg) 6,9",
            'price' => "17990",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 12,
            'id_category' => 13,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 4,
            'sold_items' => 0,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\40pfh4101_1.jpg',
            'id_product' => $product->id,
        ]);

        ProductImage::create([
            'url' => 'uploads\images\40pfh4101_2.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "TV Sony KDL-32R400C 32\" LED HD Ready 1366 x 768 100Hz HDMI x2/USB/Scart/Optical/DVB-C-T/DTS",
            'specifications' => "Panel:
-LED Panel
-Size: 80 cm (32\")
-HD Ready resolution
Image:
-Motionflow XR 100Hz
-Advanced Contrast Enhancer (ACE)
-24p True Cinema
-CineMotion / Film Mode / Cinema Drive
Audio
-Dolby® Digital / Dolby® Digital Plus / Dolby® Pulse
-Sound output: 5W x2 Audio Power Output
-S-Master
System
-DTV Tuner: DVB-TC
-CI +
Connectivity
-2 HDMI
-1 USB
-1 Input Component (Y / Pb / Pr)
-1 Input Composite (AV) (Common Use for Component Y)
-1 Ethernet (LAN)
-1 Headphones
-1 Output digital audio (Optical)
-RF In (Terrestrial / entr cable / satellite entr..):
Additional features
-BRAVIA Sync
-Electronic Program Guide (EPG)
-Picture and Picture (PAP)
-USB Play
-USB HDD Recording
-PVR (Recorder for external device)
Design
-Design: Slim
-Stand Design: MLD BLK Matt
-Eco Energy label A +
Consumption
-Consumption Operational 31W
-Annual consumption 45kWh
-Consumption (standby) 0.5W
Dimensions
-With stand (WxHxD cm) 73.8 x 47 x 15.1
-Without pedestal (WxHxD cm) 73.8 x 44.1 x 6.5
Weight
-With stand (kg) 6.3
-No base (kg) 5.8",
            'price' => "15990",
            'num_of_visits' => 0,
            'recomended' => 0,
            'id_brand' => 8,
            'id_category' => 13,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 11,
            'sold_items' => 8,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\kdl-32r400c.jpg',
            'id_product' => $product->id,
        ]);

        ProductImage::create([
            'url' => 'uploads\images\kdl-32r400c-2.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "TV LG 32LX320C 32\" LED HD Ready 1366 x 768 16:9 HDMI/USB/Scart/Optical/DVB-C-T/",
            'specifications' => "General
Screen size (inches) - 32
Resolution - 1366 x 768
Brightness (cd/m2) - 300
Contrast Ratio - 1,200:1, 1,000,000:1(DCR)
Response Time (ms) - 9
Viewing Angle (°) - 178 x 178
Video
XD Engine - Yes
Aspect Ratio Mode - 8 modes ( 16:9, Just scan,Original, full wide, 4:3,16:9, Zoom,Cinema zoom1)
Real Cinema 24p (3:3 pull down) - Yes

Input / Output
Full Scart - 1
Component in (Y,Pb,Pr-Video) - 1(Phone Jack)
Component in (Audio) - 1(Phone jack): Component/DVI/PC Audio
Audio Output - 1 (Optical)
Audio Inputs - 1 PC Audio Input
HDMI - 1
HDMI/HDCP Input - Yes
RS-232C Input/Output - Yes
USB - 1
External_Speaker Out - Yes
RGB In (D-sub 15pin) - PC - 1
CI Slot - 1
RF In - 1
Audio
Audio Power - 10W + 10W
Sound mode - Standard, News, Music, Cinema, Sport, Game
Feature
Pro:Centric - Remote Diagnosis - Yes
Smart Share - MHL
HTNG - Yes
HDMI-CEC - Yes
Hotel Mode / PDM / Installer Menu - Yes
Lock mode - Yes
Special Features
Motion Eye Care - Yes
Intelligent sensor - Yes
Smart Energy Saving - Yes
Anti-theft System - Kensington Lock
Standard (Certification) & VESA Mounting
Safety - CB
EMC - CE
VESA Mounting - 200 x 200
Power
Power Supply - 100-240V, 50/60Hz
Power Consumption - Typ.:56.8W, Max: 84.4W, Energy Saving:45.44W(Min), 31.24W(Med), 17.04W(Max), Stand-by: 0.3W
Dimensions - WxHxD (mm)
Monitor - 732mm X 437mm X 55.5mm
Monitor with Optional Stand - 732mm X 481mm X 207mm
Weight (kg)
Monitor - 6.2kg",
            'price' => "14490",
            'num_of_visits' => 0,
            'recomended' => 1,
            'id_brand' => 18,
            'id_category' => 13,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 15,
            'sold_items' => 3,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\32lx320c.jpg',
            'id_product' => $product->id,
        ]);

        ////////////////////
        $product = Product::create([
            'name' => "TV Sony KDL-50W805 50\" 3D Smart LED FullHD XR800Hz/HDMIx4/USBx3/WiFi/DVB-T-C/DTS",
            'specifications' => "Panel:
-LED Panel
-Size: 126cm (50 \")
-Full HD resolution
Image:
-X-Reality PRO™
-Motionflow XR 800Hz
-Advanced Contrast Enhancer (ACE)
-24p True Cinema
-CineMotion / Film Mode / Cinema Drive
Audio
-Dolby® Digital / Dolby® Digital Plus / Dolby® Pulse
-Sound output: 10W x2 Audio Power Output
-S-Master
Smart TV
-Android 5,0
Convergence
-Wi-Fi® Direct
-Screen Mirroring
System
-DTV Tuner: DVB-TC (TDT2 Ready)
-CI / CI + / + 2CI: CI + (1.3)
Connectivity
-4 HDMI
-3 USB
-1 Input Component (Y / Pb / Pr)
-1 Input Composite (AV) (Common Use for Component Y)
-1 Ethernet (LAN)
-1 Headphones
-1 Output digital audio (Optical)
-RF In (Terrestrial / entr cable / satellite entr..):
-Integrated Wi-Fi
Additional features
-BRAVIA Sync
-Electronic Program Guide (EPG)
-Picture and Picture (PAP)
-USB Play
-USB HDD Recording
-PVR (Recorder for external device)
-iManual
Design
-Design: Slim
-Stand Design: Silver Mirror Shaft
-Eco Energy label A +
Consumption
-Consumption Operational 72W
-Annual consumption 100kWh
-Consumption (standby) 0.5W
Dimensions
-With stand (WxHxD cm) 111.6 x 69.1 x 19.1
-Without pedestal (WxHxD cm) 111.6 x 65.4 x 5.9
Weight
-With stand (kg) 14.6
-No base (kg) 13.7",
            'price' => "45990",
            'num_of_visits' => 0,
            'recomended' => 1,
            'id_brand' => 8,
            'id_category' => 13,
        ]);
         
        Stock::create([
            'id_product' => $product->id,
            'id_warehouse' => 1,
            'num_of_items' => 2,
            'sold_items' => 0,
            'adding_date' => '2016-09-27',
        ]);
         
        ProductImage::create([
            'url' => 'uploads\images\kdl-50w805c-2.jpg',
            'id_product' => $product->id,
        ]);

        ProductImage::create([
            'url' => 'uploads\images\kdl-55w805c.jpg',
            'id_product' => $product->id,
        ]);
    }
}
