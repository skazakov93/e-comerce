<?php

use Illuminate\Database\Seeder;
use App\Modules\Buys\Buy;

class BuysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	Buy::create([
       		'id_product' => 1,
			'id_user' => 1,
       	]);

       	Buy::create([
       		'id_product' => 5,
			'id_user' => 1,
       	]);

       	Buy::create([
       		'id_product' => 89,
			'id_user' => 5,
       	]);

       	Buy::create([
       		'id_product' => 23,
			'id_user' => 4,
       	]);

       	Buy::create([
       		'id_product' => 33,
			'id_user' => 1,
       	]);

       	Buy::create([
       		'id_product' => 12,
			'id_user' => 2,
       	]);

       	Buy::create([
       		'id_product' => 43,
			'id_user' => 6,
       	]);

       	Buy::create([
       		'id_product' => 44,
			'id_user' => 2,
       	]);

       	Buy::create([
       		'id_product' => 56,
			'id_user' => 10,
       	]);

       	Buy::create([
       		'id_product' => 1,
			'id_user' => 9,
       	]);

       	Buy::create([
       		'id_product' => 56,
			'id_user' => 5,
       	]);

       	Buy::create([
       		'id_product' => 3,
			'id_user' => 1,
       	]);

       	Buy::create([
       		'id_product' => 65,
			'id_user' => 7,
       	]);

       	Buy::create([
       		'id_product' => 56,
			'id_user' => 8,
       	]);

       	Buy::create([
       		'id_product' => 21,
			'id_user' => 1,
       	]);

       	Buy::create([
       		'id_product' => 21,
			'id_user' => 7,
       	]);

       	Buy::create([
       		'id_product' => 100,
			'id_user' => 7,
       	]);

       	Buy::create([
       		'id_product' => 101,
			'id_user' => 5,
       	]);

       	Buy::create([
       		'id_product' => 100,
			'id_user' => 1,
       	]);

       	Buy::create([
       		'id_product' => 1,
			'id_user' => 10,
       	]);

       	Buy::create([
       		'id_product' => 1,
			'id_user' => 5,
       	]);

       	Buy::create([
       		'id_product' => 122,
			'id_user' => 6,
       	]);

       	Buy::create([
       		'id_product' => 100,
			'id_user' => 6,
       	]);

       	Buy::create([
       		'id_product' => 13,
			'id_user' => 5,
       	]);

       	Buy::create([
       		'id_product' => 113,
			'id_user' => 8,
       	]);

       	Buy::create([
       		'id_product' => 33,
			'id_user' => 5,
       	]);

       	Buy::create([
       		'id_product' => 46,
			'id_user' => 7,
       	]);

       	Buy::create([
       		'id_product' => 113,
			'id_user' => 2,
       	]);

       	Buy::create([
       		'id_product' => 97,
			'id_user' => 5,
       	]);

       	Buy::create([
       		'id_product' => 24,
			'id_user' => 4,
       	]);

       	Buy::create([
       		'id_product' => 24,
			'id_user' => 6,
       	]);

       	Buy::create([
       		'id_product' => 24,
			'id_user' => 10,
       	]);

       	Buy::create([
       		'id_product' => 24,
			'id_user' => 10,
       	]);

       	Buy::create([
       		'id_product' => 24,
			'id_user' => 1,
       	]);

       	Buy::create([
       		'id_product' => 24,
			'id_user' => 6,
       	]);

       	Buy::create([
       		'id_product' => 24,
			'id_user' => 1,
       	]);

       	Buy::create([
       		'id_product' => 18,
			'id_user' => 7,
       	]);

       	Buy::create([
       		'id_product' => 118,
			'id_user' => 8,
       	]);

       	Buy::create([
       		'id_product' => 90,
			'id_user' => 9,
       	]);

       	Buy::create([
       		'id_product' => 1,
			'id_user' => 7,
       	]);

       	Buy::create([
       		'id_product' => 1,
			'id_user' => 1,
       	]);

       	Buy::create([
       		'id_product' => 41,
			'id_user' => 7,
       	]);

       	Buy::create([
       		'id_product' => 44,
			'id_user' => 7,
       	]);

       	Buy::create([
       		'id_product' => 17,
			'id_user' => 7,
       	]);

       	Buy::create([
       		'id_product' => 13,
			'id_user' => 7,
       	]);

       	Buy::create([
       		'id_product' => 30,
			'id_user' => 6,
       	]);

       	Buy::create([
       		'id_product' => 30,
			'id_user' => 9,
       	]);

       	Buy::create([
       		'id_product' => 80,
			'id_user' => 8,
       	]);

       	Buy::create([
       		'id_product' => 87,
			'id_user' => 8,
       	]);

       	Buy::create([
       		'id_product' => 14,
			'id_user' => 8,
       	]);

       	Buy::create([
       		'id_product' => 12,
			'id_user' => 9,
       	]);

       	Buy::create([
       		'id_product' => 107,
			'id_user' => 9,
       	]);

       	Buy::create([
       		'id_product' => 119,
			'id_user' => 9,
       	]);

       	Buy::create([
       		'id_product' => 124,
			'id_user' => 10,
       	]);

       	Buy::create([
       		'id_product' => 125,
			'id_user' => 10,
       	]);

       	Buy::create([
       		'id_product' => 115,
			'id_user' => 5,
       	]);

       	Buy::create([
       		'id_product' => 85,
			'id_user' => 5,
       	]);

       	Buy::create([
       		'id_product' => 88,
			'id_user' => 8,
       	]);

       	Buy::create([
       		'id_product' => 24,
			'id_user' => 9,
       	]);

       	Buy::create([
       		'id_product' => 17,
			'id_user' => 7,
       	]);

       	Buy::create([
       		'id_product' => 21,
			'id_user' => 1,
       	]);

       	Buy::create([
       		'id_product' => 37,
			'id_user' => 7,
       	]);

       	Buy::create([
       		'id_product' => 90,
			'id_user' => 10,
       	]);
    }
}
