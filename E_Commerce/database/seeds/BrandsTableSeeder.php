﻿<?php

use Illuminate\Database\Seeder;
use App\Modules\Brands\Brand;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//1
        Brand::create([
	        'name' => "Logitech",
	        'photo_url' => "uploads\images\Logitech.jpg",
        ]);
        
        //2
        Brand::create([
	        'name' => "Microsoft",
	        'photo_url' => "uploads\images\microsoft.jpg",
        ]);
        
        //3
        Brand::create([
	        'name' => "SteelSeries",
	        'photo_url' => "uploads\images\SteelSeries.png",
        ]);
        
        //4
        Brand::create([
        	'name' => "Other",
        ]);
        
        //5
        Brand::create([
        	'name' => "Omega",
        ]);
        
        //6
        Brand::create([
	        'name' => "Gembird",
	        'photo_url' => "uploads\images\gembird.png",
        ]);
        
        //7
        Brand::create([
        	'name' => "Natec",
        ]);
        
        //8
        Brand::create([
	        'name' => "Sony",
	        'photo_url' => "uploads\images\sony.png",
        ]);
        
        //9
        Brand::create([
        	'name' => "AKG",
        ]);
        
        //10
        Brand::create([
	        'name' => "Verbatim",
	        'photo_url' => "uploads\images\Verbatim.jpg",
        ]);
        
        //11
        Brand::create([
        	'name' => "Trevi",
        ]);
        
        //12
        Brand::create([
        	'name' => "Philips",
        ]);
        
        //13
        Brand::create([
	        'name' => "Hantol",
	        'photo_url' => "uploads\images\hantol.png",
        ]);
        
        //14
        Brand::create([
        	'name' => "Speedo",
        ]);
        
        //15
        Brand::create([
        	'name' => "Mediacom",
        ]);
        
        //16
        Brand::create([
	        'name' => "Dell",
	        'photo_url' => "uploads\images\Dell-logo.jpg",
        ]);
        
        //17
        Brand::create([
	        'name' => "BenQ",
	        'photo_url' => "uploads\images\benq.jpg",
        ]);
        
        //18
        Brand::create([
	        'name' => "LG",
	        'photo_url' => "uploads\images\LG_Logo.svg.png",
        ]);
        
        //19
        Brand::create([
	        'name' => "Samsung",
	        'photo_url' => "uploads\images\Samsung.png",
        ]);
        
        //20
        Brand::create([
	        'name' => "A4 Tech",
	        'photo_url' => "uploads\images\a4tech.png",
        ]);
        
        //21
        Brand::create([
        	'name' => "Natec",
        ]);
        
        //22
        Brand::create([
        	'name' => "Delux",
        ]);
        
        //23
        Brand::create([
	        'name' => "HP",
	        'photo_url' => "uploads\images\hp.png",
        ]);
        
        //24
        Brand::create([
        	'name' => "Canon",
        ]);
        
        //25
        Brand::create([
	        'name' => "Epson",
	        'photo_url' => "uploads\images\epson.png",
        ]);

        //26
        Brand::create([
            'name' => "Hyundai",
            'photo_url' => "uploads\images\hyundai.png",
        ]);
        
        //27
        Brand::create([
            'name' => "Toshiba",
            'photo_url' => "uploads\images\toshiba.png",
        ]);

        //28
        Brand::create([
            'name' => "Seagate",
        ]);

        //29
        Brand::create([
            'name' => "Western Digital",
            'photo_url' => "uploads\images\western.png",
        ]);

        //30
        Brand::create([
            'name' => "Hitachi",
            'photo_url' => "uploads\images\hitachi.png",
        ]);
    }
}
