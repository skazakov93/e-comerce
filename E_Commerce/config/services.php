<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'github' => [
        'client_id' => '5453ef42bef75838d7ad',
        'client_secret' => '30f2ff7b238ded89ba92d1f860e81a0bb524505a',
        'redirect' => 'http://localhost:8000/api/github/login',
    ],

    'facebook' => [
        'client_id' => '240648546352988',
        'client_secret' => '129f7fac568280bcaf30703a3b489c7b',
        'redirect' => 'http://localhost:8000/api/facebook/login',
    ],

    'twitter' => [
        'client_id' => '5Xo19OfybltovVCKgQlNqc9jy',
        'client_secret' => '8FoWUKuSyeW3GWnsF9wKaEoCEk4Vukukc2snyR4U4ZbvKMdLiJ',
        'redirect' => 'http://localhost:8000/api/twitter/login',
    ],

    'google' => [
        'client_id' => '415816018507-rgl2o94rhh0bv9bmlqfke7ut86u5uc2n.apps.googleusercontent.com',
        'client_secret' => 'tJfhsL1lBP6eZ46t3FpC5cun',
        'redirect' => 'http://localhost:8000/api/google/login',
    ],

];
