<?php

namespace App\Modules\Products;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Categories\Category;
use App\Modules\Products\Product;
use App\Modules\Reviews\Review;
use App\Modules\Products\ProductsRepository;

class ProductsController extends Controller
{
    private $productsRepository;

    public function __construct(ProductsRepository $productsRepository)
    {
        $this->productsRepository = $productsRepository;
    }

    public function proudctsInCategory($slug, Request $request){
        $orderType = $request->input('sortBy');

        $itemsPerPage = $request->input('itemsPerPage');

        return $this->productsRepository->productsSortedBy($slug, $orderType, $itemsPerPage);
    }

    public function productDetails($slug){
    	$product = Product::with(['images', 'manifacturer', 'availableAtWarehouses', 'onSaleProducts'])->where('slug', '=', $slug)->first();

    	return $product;
    }

    public function recentlyViewedProducts(){
    	$products = Product::with(['images', 'manifacturer', 'onSaleProducts', 'availableAtWarehouses'])
    			->orderBy('num_of_visits', 'desc')
    			->limit(13)
    			->get();

    	return $products;
    }

    public function getLatestProducts(){
    	$products = Product::with(['manifacturer', 'images', 'onSaleProducts', 'availableAtWarehouses'])
    				->orderBy('created_at')
    				->limit(4)
    				->get();

    	return $products;
    }

    public function getRecomendedProducts(){
        $products = Product::with(['manifacturer', 'images', 'onSaleProducts', 'availableAtWarehouses'])
                    ->where('recomended', '=', '1')
                    ->get();   

        //vrakanje na 4 produkti, odbrani po slucaen izbor!!!
        return $products->random(4);
    }

    public function getProductsByCategoryBrands(Request $request){
        $slugArray = explode("#", $request->input('slugList'));

        $orderType = $request->input('sortBy');

        $itemsPerPage = $request->input('itemsPerPage');

        return $this->productsRepository->productsByBrandsSortedBy($request->input('categorySlug'), $orderType, $slugArray, $itemsPerPage);
    }

    public function getProductReviews($slug){
        $product = Product::where('slug', '=', $slug)->first();

        return $product->reviewedByUsers;
    }

    public function getProductRating($slug){
        $product = Product::where('slug', '=', $slug)->first();

        $rating = Review::where('id_product', '=', $product->id)
                        ->where('rating', '!=', 'null')
                        ->avg('rating');

        $count = Review::where('id_product', '=', $product->id)
                        ->where('rating', '!=', 'null')
                        ->count();

        return response()->json([
            'product_rating' => $rating,
            'count' => $count,
            'sum' => $rating * $count
        ]);
    }
}
