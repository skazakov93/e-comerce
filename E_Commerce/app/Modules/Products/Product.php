<?php

namespace App\Modules\Products;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Brands\Brand;
use App\Modules\Categories\Category;
use App\Modules\Warehouses\Warehouse;
use App\Modules\ProductImages\ProductImage;
use App\Modules\Customers\Customer;
use App\Modules\Stocks\Stock;
use Cviebrock\EloquentSluggable\Sluggable;
use Carbon\Carbon;

class Product extends Model
{
	use Sluggable;
	
	protected $table = "products";
	
	protected $fillable = [
		'name',
		'description',
		'specifications',
		'price',
		'num_of_visits',
		'recomended',
		'id_brand',
		'id_category',
	];
	
	public function reviewedByUsers(){
		return $this->belongsToMany(Customer::class, 'reviews', 'id_product', 'id_user')
						->withPivot('description', 'rating', 'created_at')
						->orderBy('pivot_created_at');
	}
	
	public function boughtByUsers(){
		return $this->belongsToMany(Customer::class, 'buys', 'id_product', 'id_user');
	}
	
	//da se proveri!
	public function manifacturer(){
		return $this->belongsTo(Brand::class, 'id_brand');
	}
	
	public function category(){
		return $this->belongsTo(Category::class, 'id_category');
	}
	
	//gi vraka site magacini kade sto se naoga Proizvodot
	//ToDo: da gi vraka samo magacinite kade sto Produktot na zaliha!!!
	public function availableAtWarehouses(){
		return $this->belongsToMany(Warehouse::class, 'stocks', 'id_product', 'id_warehouse')
						->withPivot('num_of_items', 'sold_items');
	}

	// public function availableAtWarehouses(){
	// 	return $this->belongsToMany(Stock::class, 'stocks', 'id_product');		
	// }
	
	//testirano i RABOTI!!!
	public function onSaleProducts(){
		return $this->belongsToMany(Product::class, 'on_sales', 'id_product', 'id_sale')
						->withPivot('price')
						->whereColumn('ending_date', '>=', 'starting_date')
						->where('ending_date', '>=', Carbon::today())
						->select('on_sales.price');
	}
	
	public function images(){
		return $this->hasMany(ProductImage::class, 'id_product', 'id');
	}

	//testirano i RABOTI!!!
	public function reviewedByUserWithRating($customerId){
		return $this->belongsToMany(Customer::class, 'reviews', 'id_product', 'id_user')
						->withPivot('description', 'rating')
						->where('id_user', '=', $customerId)
						->where('rating', '!=', 'null');
	}
	
	/**
	 * Return the sluggable configuration array for this model.
	 *
	 * @return array
	 */
	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'name'
			]
		];
	}
}