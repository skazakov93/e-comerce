<?php

namespace App\Modules\Products;

use App\Modules\Categories\Category;

class ProductsRepository
{
   	public function productsSortedBy($slug, $orderType, $itemsPerPage){
   		$order = $this->getOrderType($orderType);

        $productsPerPage = $this->getItemPerPage($itemsPerPage);

    	$products = Category:://with(['products.images', 'products.manifacturer'])
                    where('slug', '=', $slug)
                    ->first()
                    ->products()
                    ->with(['images', 'manifacturer', 'onSaleProducts', 'availableAtWarehouses'])
                    ->orderBy($order['orderKey'], $order['orderValue'])
                    ->paginate($productsPerPage);

       	return $products;
   	}

   	public function productsByBrandsSortedBy($slug, $orderType, $slugArray, $itemsPerPage){
   		$order = $this->getOrderType($orderType);

        $productsPerPage = $this->getItemPerPage($itemsPerPage);

        $category = Category::where('slug', '=', $slug)->first();

        $slugArray = array_slice($slugArray, 0, sizeof($slugArray) - 1);

        $products = null;
        if(sizeof($slugArray) > 0) {
            $products = Product::with(['manifacturer', 'images', 'onSaleProducts', 'availableAtWarehouses'])
                        ->where('id_category', '=', $category->id)
                        ->whereIn('id_brand', $slugArray)
                        ->orderBy($order['orderKey'], $order['orderValue'])
                        ->paginate($productsPerPage);
        }
        else {
            $products = Product::with(['manifacturer', 'images', 'onSaleProducts', 'availableAtWarehouses'])
                        ->where('id_category', '=', $category->id)
                        ->orderBy($order['orderKey'], $order['orderValue'])
                        ->paginate($productsPerPage);
        }

        return $products;
   	}

   	private function getItemPerPage($itemsPerPage){
   		$productsPerPage = 12;

        if($itemsPerPage == 2){
        	$productsPerPage = 24;
        }
        else if($itemsPerPage == 3){
        	$productsPerPage = 36;
        }
        else if($itemsPerPage == 4){
        	$productsPerPage = 48;
        }
        else if($itemsPerPage == 5){
        	$productsPerPage = 60;
        }

        return $productsPerPage;
   	}

   	private function getOrderType($orderType){
   		$orderKey = 'name';
        $orderValue = 'asc';
        
        if($orderType == 'newest'){
            $orderKey = 'created_at';
            $orderValue = 'desc';
        }
        else if($orderType == 'priceDesc'){
            $orderKey = 'price';
            $orderValue = 'desc';
        }
        else if($orderType == 'priceAsc'){
            $orderKey = 'price';
            $orderValue = 'asc';
        }
        else if($orderType == 'nameAsc'){
            $orderKey = 'name';
            $orderValue = 'asc';
        }
        else if($orderType == 'nameDesc'){
            $orderKey = 'name';
            $orderValue = 'desc';
        }

        $myArray = [];
        $myArray['orderKey'] = $orderKey;
        $myArray['orderValue'] = $orderValue;

        return $myArray;
   	}
}
