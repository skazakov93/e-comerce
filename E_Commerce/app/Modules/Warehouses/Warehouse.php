<?php

namespace App\Modules\Warehouses;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Products\Product;
use Cviebrock\EloquentSluggable\Sluggable;

class Warehouse extends Model
{
	use Sluggable;
	
	protected $table = "warehouses";
	
	public $timestamps = false;
	
	protected $fillable = [
		'name',
		'address',
		'phone',
	];
	
	public function products(){
		return $this->belongsToMany(Product::class, 'stocks', 'id_warehouse', 'id_product');
	}
	
	/**
	 * Return the sluggable configuration array for this model.
	 *
	 * @return array
	 */
	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'name'
			]
		];
	}
}
