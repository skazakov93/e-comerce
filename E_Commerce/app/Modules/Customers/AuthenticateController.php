<?php

namespace App\Modules\Customers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Modules\Users\User;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterUserRequest;
use App\Modules\Customers\Customer;
use Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class AuthenticateController extends Controller
{
	public function __construct()
	{
		$this->middleware('jwt.auth', ['except' => ['authenticate', 'registerUser']]);
	}
	
	public function authenticate(LoginRequest $request){
		$credentials = Input::only('email', 'password');
		
		try {
			// verify the credentials and create a token for the user
			if (! $token = JWTAuth::attempt($credentials)) {
				return response()->json(['error' => 'The Credentials do not match our records!!'], 401);
			}
		} catch (JWTException $e) {
			// something went wrong
			return response()->json(['error' => 'could_not_create_token'], 500);
		}
	
		// if no errors are encountered we can return a JWT
		return response()->json(compact('token'));
	}
	
	public function getAuthenticatedUser(){
		try {
	
			if (! $user = JWTAuth::parseToken()->authenticate()) {
				return response()->json(['user_not_found'], 404);
			}
	
		}
		catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
	
			return response()->json(['token_expired'], $e->getStatusCode());
	
		}
		catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
	
			return response()->json(['token_invalid'], $e->getStatusCode());
	
		}
		catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
	
			return response()->json(['token_absent'], $e->getStatusCode());
	
		}
	
		// the token is valid and we have found the user via the sub claim
		$jsonUser = compact('user');

		$user = $jsonUser['user'];

		$myUser = [];
		$myUser['firstName'] = $user->firstName;
		$myUser['lastName'] = $user['lastName'];
		$myUser['email'] = $user['email'];
		$myUser['role'] = $user['role'];
		$myUser['photo_url'] = $user['photo_url'];

		$returnUser = [];
		$returnUser['user'] = $myUser;

		return response()->json($returnUser);
	}

	public function logOutUser(){
        $token = JWTAuth::getToken();

        $token = JWTAuth::refresh($token); 

        return "Token refreshed!!!";  
    }

    public function registerUser(RegisterUserRequest $request){
    	$customer = new Customer();
    	
    	$customer->setRole();
    	$customer->firstName = $request->input('firstName');
    	$customer->lastName = $request->input('lastName');
    	$customer->email = $request->input('email');
    	$customer->password = Hash::make($request->input('password'));
    	$customer->provider = 'local';
    	$customer->phone_number = $request->input('phone_number');
    	$customer->save();

    	if($request->file('photo')){
    		// zacuvuvanje na slikata vo fajl sistemot
	    	$fileName = $customer->slug . '.png';
			Storage::disk('public')->put($fileName, File::get($request->file('photo')));

			$customer->photo_url = $fileName;
			$customer->save();
    	}


    	// najavuvanje na stotuku registriraniot korisnik!!!
    	$credentials = [];
    	$credentials['email'] = $customer->email;
    	$credentials['password'] = $request->input('password');

    	try {
			// verify the credentials and create a token for the user
			if (! $token = JWTAuth::attempt($credentials)) {
				return response()->json(['error' => 'The Credentials do not match our records!!'], 401);
			}
		} catch (JWTException $e) {
			// something went wrong
			return response()->json(['error' => 'could_not_create_token'], 500);
		}
	
		// if no errors are encountered we can return a JWT
		return response()->json(compact('token'));
    }
}
