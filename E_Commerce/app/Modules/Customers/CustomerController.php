<?php

namespace App\Modules\Customers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\PostReviewForProduct;
use App\Modules\Customers\CustomersRepositoryInterface;
use App\Modules\Buys\Buy;
use App\Modules\Customers\Customer;
use App\Modules\Customers\CustomersInterface;
use App\Modules\Products\Product;
use App\Modules\Reviews\Review;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
	private $customerInterface;

    public function __construct(CustomersRepositoryInterface $customerInterface)
	{
		//dokolku sakame nekoj metod da go isklucime od validacijata
		// ['except' => ['authenticate']]
		$this->middleware('stripe-preprocesor', ['only' => ['customerBuysProducts']]);

		$this->middleware('jwt.auth', ['except' => ['authenticate']]);

		$this->customerInterface = $customerInterface;
	}

	public function postReviewForProduct(PostReviewForProduct $request){
		$messageFromRepo = $this->customerInterface->postReviewForProduct($request);

		return $messageFromRepo;
	}

	public function customerBuysProducts(Request $request){
		return $this->customerInterface->customerBuysProducts($request);	
	}
}