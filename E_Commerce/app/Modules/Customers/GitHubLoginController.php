<?php

namespace App\Modules\Customers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Customers\GitHubAuthenticateUser;

class GitHubLoginController extends Controller
{
    public function login(GitHubAuthenticateUser $authenticateUser, Request $request) {
    	return $authenticateUser->execute($request->has('code'));
    }
}
