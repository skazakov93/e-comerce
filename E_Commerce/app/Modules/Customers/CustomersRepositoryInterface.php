<?php

namespace App\Modules\Customers;

use App\Http\Requests\PostReviewForProduct;
use Illuminate\Http\Request;

interface CustomersRepositoryInterface
{
    public function postReviewForProduct(PostReviewForProduct $request);

    public function customerBuysProducts(Request $request);
}