<?php

namespace App\Modules\Customers;

use App\Modules\Customers\Customer;
use App\Modules\Users\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Hash;
use GuzzleHttp;

class GoogleAuthenticateUser
{
    public function execute($request) {
    	$client = new GuzzleHttp\Client();

    	$params = [
            'code' => $request->input('code'),
            'client_id' => $request->input('clientId'),
            'client_secret' => 'UU0C9KOaFR6nHyloMStSC-xt',
            'redirect_uri' => $request->input('redirectUri'),
            'grant_type' => 'authorization_code',
        ];

        // Step 1. Exchange authorization code for access token.
        $accessTokenResponse = $client->request('POST', 'https://accounts.google.com/o/oauth2/token', [
            'form_params' => $params
        ]);
        $accessToken = json_decode($accessTokenResponse->getBody(), true);

        // Step 2. Retrieve profile information about the current user.
        $profileResponse = $client->request('GET', 'https://www.googleapis.com/plus/v1/people/me/openIdConnect', [
            'headers' => array('Authorization' => 'Bearer ' . $accessToken['access_token'])
        ]);

        $googleUser = json_decode($profileResponse->getBody(), true);
       
    	$userInDB = User::where('email', '=', $googleUser['email'])->first();

    	if(! $userInDB) {
    		// zacuvuvanje na user-ot
	    	$user = new Customer();
	    	$user->firstName = $googleUser['given_name'];
	    	$user->lastName = $googleUser['family_name'];
	    	$user->email = $googleUser['email'];
	    	$user->provider = 'google';
	    	$user->photo_url = $googleUser['picture'];
	    	$user->password = Hash::make('googleProvider');
	    	$user->setRole();

	    	$user->save();

	    	return $this->authenticate($user);
    	}

    	return $this->authenticate($userInDB);
    }

    private function authenticate($user){
    	$credentials = [];
    	$credentials['email'] = $user->email;
    	$credentials['provider'] = $user->provider;
    	$credentials['password'] = 'googleProvider';

		try {
			// verify the credentials and create a token for the user
			if (! $token = JWTAuth::attempt($credentials)) {
				return response()->json(['error' => 'The Credentials do not match our records!!'], 401);
			}
		} catch (JWTException $e) {
			// something went wrong
			return response()->json(['error' => 'could_not_create_token'], 500);
		}

		// if no errors are encountered we can return a JWT
		$myToken = compact('token');

		return $myToken;
    }
}
