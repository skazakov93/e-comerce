<?php

namespace App\Modules\Customers;

use App\Modules\Customers\CustomersRepositoryInterface;
use App\Http\Requests\PostReviewForProduct;
use App\Modules\Products\Product;
use App\Modules\Users\User;
use App\Modules\Reviews\Review;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Stripe\Charge;
use Stripe\Customer as StripeCustomer;
use Stripe\Stripe;
use App\Modules\Stocks\Stock;
use App\Modules\Buys\Buy;

class CustomersRepository implements CustomersRepositoryInterface
{
    public function postReviewForProduct(PostReviewForProduct $request){
    	//za zemanje na User-ot od prateniot token!!!!
        $token = JWTAuth::getToken();
        $customer = JWTAuth::toUser($token); 

        $product = Product::where('id', '=', $request->input('id_product'))->first();

    	$productReviews= $product->reviewedByUserWithRating($customer->id)->get();

    	if(sizeof($productReviews) <= 0){
    		// User-ot seuste go nema oceneto proizvodot
    		// pa dokolkuodbral ocena, treba da ja postavime
    		$review = $this->saveReview($product->id, $customer->id, $request->input('description'), $request->input('rating'));

    		return $this->returnData("OK", $customer, $review);
    	}
    	else if(sizeof($productReviews) > 0 && $request->input('rating')){
    		// User-ot veke go ocenil produktot, i povtorno praka nova ocenka
    		// ke go zapisam noviot review, no ke mu kazam deka ocenkata ne moze da ja menuva!!!
    		$review = $this->saveReview($product->id, $customer->id, $request->input('description'), null);

    		return $this->returnData("NO", $customer, $review);
    	}
    	else{
    		// User-ot veke go ocenil produktot, i praka samo novo riview
    		$review = $this->saveReview($product->id, $customer->id, $request->input('description'), null);

            return $this->returnData("OK1", $customer, $review);
    	}
    }

    public function customerBuysProducts(Request $request) {
        //za zemanje na User-ot od prateniot token!!!!
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        // Perform the charge!!!
        Stripe::setApiKey(config('services.stripe.secret'));

        $customer = $this->createStripeCustomer($request->input('stripeEmail'), $request->input('stripeToken'));

        $producsListSlug = $request->input('products-list');;
        foreach ($producsListSlug as $producstSlug) {
            $productFromDB = Product::where('slug', '=', $producstSlug)->first();
            
            // dokolku postoi produktot izvrsi naplata!!!
            if($productFromDB) {
                // get product price
                $productPrice = $this->getProductPrice($productFromDB);

                $priceUSD = $this->mkdToDollars($productPrice);

                $productOnStock = Stock::where('id_product', '=', $productFromDB->id)
                                ->whereColumn('num_of_items', '>', 'sold_items')
                                ->first();

                // Dali produktot go ima na zaliha vo nekoj magacin
                if($productOnStock){
                    // ova ke treba da bide vo transakcija!!
                    $this->stripeCharge($customer->id, $priceUSD);

                    $this->updateProductSoldItems($productOnStock);

                    $this->saveRecordToBuys($user->id, $productFromDB->id);
                }
            }
        }

        $myUrl = 'http://localhost:9000/#/success-payment';
        return redirect($myUrl);
    }

    private function saveReview($product_id, $id_user, $description, $rating){
        $review = new Review();
        $review->id_product = $product_id;
        $review->id_user = $id_user;
        $review->description = $description;

        if($rating){
            $review->rating = $rating;
        }

        $review->save();

        return $review;
    }

    private function returnData($status, User $customer, Review $review){
        $returnData = [];
        $returnData['firstName'] = $customer->firstName;
        $returnData['lastName'] = $customer->lastName;
        $returnData['review_date'] = "Just Now";
        $returnData['pivot'] = $review->toArray();

        $returnArray = [];
        $returnArray[] = $status;
        $returnArray[] = $returnData;

        return $returnArray;
    }

    private function saveRecordToBuys($user_id, $product_id) {
        $customersBuysProduct = new Buy();
        $customersBuysProduct->id_user = $user_id;
        $customersBuysProduct->id_product = $product_id;
        $customersBuysProduct->save();
    }

    private function updateProductSoldItems($product) {
        $product->sold_items += 1;
        $product->save();
    }

    private function mkdToDollars($price){
        $priceUSD = $price / 57.19;
        $pricePenny = $priceUSD * 100;

        return ceil($pricePenny);
    }

    private function stripeCharge($customer_id, $price){
        Charge::create([
            'customer' => $customer_id,
            'amount' => $price,
            'currency' => 'usd'
        ]);
    }

    private function createStripeCustomer($stripe_email, $stripe_token){
        $customer = StripeCustomer::create([
            'email' => $stripe_email,
            'source' => $stripe_token,
        ]);

        return $customer;
    }

    private function getProductPrice($product){
        $productPrice = $product->price;
        if(sizeof($product->onSaleProducts) > 0) {
            $productPrice = $product->onSaleProducts[0]->price;
        }

        return $productPrice;
    }
}