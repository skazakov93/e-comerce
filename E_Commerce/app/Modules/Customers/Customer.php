<?php

namespace App\Modules\Customers;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Users\User;
use App\Modules\Products\Product;
use Carbon\Carbon;

class Customer extends User
{
	protected $appends = ['review_date'];

    public function getReviewDateAttribute() {
        if($this->pivot && $this->pivot->created_at){
        	return Carbon::parse($this->pivot->created_at)->diffForHumans();	
        }
        return null;
    }

	public function setRole() {
        $this->attributes['role'] = 'customer';
    }

	public function boughtProducts(){
		return $this->belongsToMany(Product::class, 'buys', 'id_user', 'id_product');
	}
	
	public function userReviews(){
		return $this->belongsToMany(Product::class, 'reviews', 'id_user', 'id_product');
	}
}
