<?php

namespace App\Modules\Customers;

use Laravel\Socialite\Contracts\Factory as Socialite;
use App\Modules\Customers\Customer;
use App\Modules\Users\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Hash;
use GuzzleHttp;

class FacebookAuthenticateUser
{
    private $socialite;

	public function __construct(Socialite $socialite) {
		$this->socialite = $socialite;
	}

    public function execute($request) {
    	$client = new GuzzleHttp\Client();

    	$params = [
            'code' => $request['code'],
            'client_id' => $request['clientId'],
            'redirect_uri' => $request['redirectUri'],
            'client_secret' => '129f7fac568280bcaf30703a3b489c7b'
        ];

        // Step 1. Exchange authorization code for access token.
        $accessTokenResponse = $client->request('GET', 'https://graph.facebook.com/v2.5/oauth/access_token', [
            'query' => $params
        ]);
        $accessToken = json_decode($accessTokenResponse->getBody(), true);

        // Step 2. Retrieve profile information about the current user.
        $fields = 'id,email,first_name,last_name,link,name,picture';
        $profileResponse = $client->request('GET', 'https://graph.facebook.com/v2.5/me', [
            'query' => [
                'access_token' => $accessToken['access_token'],
                'fields' => $fields
            ]
        ]);
        $facebookUser = json_decode($profileResponse->getBody(), true);

    	$userInDB = User::where('email', '=', $facebookUser['email'])->first();

    	if(! $userInDB) {
    		// zacuvuvanje na user-ot
	    	$user = new Customer();
	    	$user->firstName = $facebookUser['name'];
	    	$user->email = $facebookUser['email'];
	    	$user->provider = 'facebook';
	    	$user->photo_url = $facebookUser['picture']['data']['url'];
	    	$user->password = Hash::make('facebookProvider');
	    	$user->setRole();

	    	$user->save();

	    	return $this->authenticate($user);
    	}

    	return $this->authenticate($userInDB);
    }

    private function authenticate($user){
    	$credentials = [];
    	$credentials['email'] = $user->email;
    	$credentials['provider'] = $user->provider;
    	$credentials['password'] = 'facebookProvider';

		try {
			// verify the credentials and create a token for the user
			if (! $token = JWTAuth::attempt($credentials)) {
				return response()->json(['error' => 'The Credentials do not match our records!!'], 401);
			}
		} catch (JWTException $e) {
			// something went wrong
			return response()->json(['error' => 'could_not_create_token'], 500);
		}

		// if no errors are encountered we can return a JWT
		$myToken = compact('token');

		return $myToken;
    }
}
