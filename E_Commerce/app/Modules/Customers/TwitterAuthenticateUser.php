<?php

namespace App\Modules\Customers;

use Laravel\Socialite\Contracts\Factory as Socialite;
use App\Modules\Customers\Customer;
use App\Modules\Users\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Hash;
use GuzzleHttp;
use GuzzleHttp\Subscriber\Oauth\Oauth1;

class TwitterAuthenticateUser
{
    public function execute($request) {
    	$stack = GuzzleHttp\HandlerStack::create();

    	// Part 1 of 2: Initial request from Satellizer.
        if (!$request->input('oauth_token') || !$request->input('oauth_verifier'))
        {
            $stack = GuzzleHttp\HandlerStack::create();
            $requestTokenOauth = new Oauth1([
              'consumer_key' => '5Xo19OfybltovVCKgQlNqc9jy',
              'consumer_secret' => '8FoWUKuSyeW3GWnsF9wKaEoCEk4Vukukc2snyR4U4ZbvKMdLiJ',
              'callback' => $request->input('redirectUri'),
              'token' => '',
              'token_secret' => ''
            ]);
            $stack->push($requestTokenOauth);
            $client = new GuzzleHttp\Client([
                'handler' => $stack
            ]);
            // Step 1. Obtain request token for the authorization popup.
            $requestTokenResponse = $client->request('POST', 'https://api.twitter.com/oauth/request_token', [
                'auth' => 'oauth'
            ]);
            $oauthToken = array();
            parse_str($requestTokenResponse->getBody(), $oauthToken);
            // Step 2. Send OAuth token back to open the authorization screen.
            return response()->json($oauthToken);
        }
        // Part 2 of 2: Second request after Authorize app is clicked.
        else
        {
            $accessTokenOauth = new Oauth1([
                'consumer_key' => '5Xo19OfybltovVCKgQlNqc9jy',
              	'consumer_secret' => '8FoWUKuSyeW3GWnsF9wKaEoCEk4Vukukc2snyR4U4ZbvKMdLiJ',
                'token' => $request->input('oauth_token'),
                'verifier' => $request->input('oauth_verifier'),
                'token_secret' => ''
            ]);
            $stack->push($accessTokenOauth);
            $client = new GuzzleHttp\Client([
                'handler' => $stack
            ]);
            // Step 3. Exchange oauth token and oauth verifier for access token.
            $accessTokenResponse = $client->request('POST', 'https://api.twitter.com/oauth/access_token', [
                'auth' => 'oauth'
            ]);
            $accessToken = array();
            parse_str($accessTokenResponse->getBody(), $accessToken);
            $profileOauth = new Oauth1([
                'consumer_key' => '5Xo19OfybltovVCKgQlNqc9jy',
              	'consumer_secret' => '8FoWUKuSyeW3GWnsF9wKaEoCEk4Vukukc2snyR4U4ZbvKMdLiJ',
                'oauth_token' => $accessToken['oauth_token'],
                'token_secret' => ''
            ]);
            $stack->push($profileOauth);
            $client = new GuzzleHttp\Client([
                'handler' => $stack
            ]);
            // Step 4. Retrieve profile information about the current user.
            $profileResponse = $client->request('GET', 'https://api.twitter.com/1.1/users/show.json?screen_name=' . $accessToken['screen_name'], [
                'auth' => 'oauth'
            ]);

            $twitterUser = json_decode($profileResponse->getBody(), true);

	    	$twitterEmail = $twitterUser['screen_name'] . '23598@twi67ter.com';

	    	$userInDB = User::where('email', '=', $twitterEmail)->first();

	    	if(! $userInDB) {
	    		// zacuvuvanje na user-ot
		    	$user = new Customer();
		    	$user->firstName = $twitterUser['name'];
		    	$user->email = $twitterUser['screen_name'] . '23598@twi67ter.com';
		    	$user->provider = 'twitter';
		    	$user->photo_url = $twitterUser['profile_image_url'];
		    	$user->password = Hash::make('twitterProvider');
		    	$user->setRole();

		    	$user->save();

		    	return $this->authenticate($user);
	    	}

	    	return $this->authenticate($userInDB);
        }
    }

    private function authenticate($user){
    	$credentials = [];
    	$credentials['email'] = $user->email;
    	$credentials['provider'] = $user->provider;
    	$credentials['password'] = 'twitterProvider';

		try {
			// verify the credentials and create a token for the user
			if (! $token = JWTAuth::attempt($credentials)) {
				return response()->json(['error' => 'The Credentials do not match our records!!'], 401);
			}
		} catch (JWTException $e) {
			// something went wrong
			return response()->json(['error' => 'could_not_create_token'], 500);
		}

		// if no errors are encountered we can return a JWT
		$myToken = compact('token');

		return $myToken;
    }
}
