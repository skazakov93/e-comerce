<?php

namespace App\Modules\Customers;

use Laravel\Socialite\Contracts\Factory as Socialite;
use App\Modules\Customers\Customer;
use App\Modules\Users\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Hash;

class GitHubAuthenticateUser
{
	private $socialite;

	public function __construct(Socialite $socialite) {
		$this->socialite = $socialite;
	}

    public function execute($hasCode) {
    	if(! $hasCode) {
    		return $this->getAuthoizationFirst();
    	}

    	$gitHubUser = $this->socialite->with('github')->user();

    	$userInDB = User::where('email', '=', $gitHubUser->email)->first();

    	if(! $userInDB) {
    		// zacuvuvanje na user-ot
	    	$user = new Customer();
	    	$user->firstName = $gitHubUser->nickname;
	    	$user->email = $gitHubUser->email;
	    	$user->provider = 'github';
	    	$user->photo_url = $gitHubUser->avatar;
	    	$user->password = Hash::make('gitHubProvider');
	    	$user->setRole();

	    	$user->save();

	    	return $this->authenticate($user);
    	}

    	return $this->authenticate($userInDB);
    }

    private function getAuthoizationFirst() {
    	return $this->socialite->with('github')->redirect();
    }

    private function authenticate($user){
    	$credentials = [];
    	$credentials['email'] = $user->email;
    	$credentials['provider'] = $user->provider;
    	$credentials['password'] = 'gitHubProvider';

		try {
			// verify the credentials and create a token for the user
			if (! $token = JWTAuth::attempt($credentials)) {
				return response()->json(['error' => 'The Credentials do not match our records!!'], 401);
			}
		} catch (JWTException $e) {
			// something went wrong
			return response()->json(['error' => 'could_not_create_token'], 500);
		}

		// if no errors are encountered we can return a JWT
		$myToken = compact('token');

		$myUrl = 'http://localhost:9000/#/redirecting/' . $myToken['token'];
		return redirect($myUrl);
    }
}
