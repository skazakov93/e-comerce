<?php

namespace App\Modules\Customers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Customers\GoogleAuthenticateUser;

class GoogleLoginController extends Controller
{
    public function login(GoogleAuthenticateUser $authenticateUser, Request $request) {
    	return $authenticateUser->execute($request);
    }
}
