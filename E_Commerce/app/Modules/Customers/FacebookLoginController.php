<?php

namespace App\Modules\Customers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Customers\FacebookAuthenticateUser;

class FacebookLoginController extends Controller
{
    public function login(FacebookAuthenticateUser $authenticateUser, Request $request) {
    	return $authenticateUser->execute($request->all());
    }
}
