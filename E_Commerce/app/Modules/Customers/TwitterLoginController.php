<?php

namespace App\Modules\Customers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Customers\TwitterAuthenticateUser;

class TwitterLoginController extends Controller
{
    public function login(TwitterAuthenticateUser $authenticateUser, Request $request) {
    	return $authenticateUser->execute($request);
    }
}
