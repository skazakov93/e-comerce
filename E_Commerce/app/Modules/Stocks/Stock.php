<?php

namespace App\Modules\Stocks;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Products\Product;
use App\Modules\Warehouses\Warehouse;

class Stock extends Model
{
	protected $table = "stocks";
	
	public $timestamps = false;
	
	protected $fillable = [
		'id_product',
		'id_warehouse',
		'num_of_items',
		'sold_items',
		'adding_date',
	];
	
	public function product(){
		return $this->belongsTo(Product::class, 'id_product');
	}
	
	public function warehouse(){
		return $this->belongsTo(Warehouse::class, 'id_warehouse');
	}
}
