<?php

namespace App\Modules\Categories;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Products\Product;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
	use Sluggable;
	
	protected $table = "categories";
	
	public $timestamps = false;
	
	protected $fillable = [
		'name',
		'main_category',
	];
	
	public function products(){
		return $this->hasMany(Product::class, 'id_category', 'id');
	}

	// public function categoryBrands(){
	// 	return $this->hasMany(Product::class, 'id_category', 'id')
	// 				->with('manifacturer');
	// }
	
	//treba da ja testiram!!!
	public function subCategories(){
		return $this->belongsToMany(Category::class, 'sub_categories', 'id_category', 'id_subcategory');
	}
	
	/**
	 * Return the sluggable configuration array for this model.
	 *
	 * @return array
	 */
	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'name'
			]
		];
	}
}
