<?php

namespace App\Modules\Categories;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function getCategories(){
    	$categories = Category::with('subCategories')->where('main_category', '=', '1')->get();
    	
    	return $categories;
    }

    public function getCategoryName($slug){
    	$category = Category::where('slug', '=', $slug)->first();

    	return $category;
    }
}
