<?php

namespace App\Modules\Brands;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Brands\Brand;
use App\Modules\Categories\Category;
use Illuminate\Support\Facades\DB;

class BrandsController extends Controller
{
    public function getLatestBrands(){
    	$brands = Brand::where('photo_url', '!=', null)
    				->orderBy('created_at', 'desc')
    				->limit(13)
    				->get();

    	return $brands;
    }

    public function getCategoryBrands($slug){
    	
    	//ova raboti
    	//ToDo: da se refaktorira so koristenje samo na Eloquent
    	//mozno resenie whereHas() !!!
    	$brands = DB::select(
    		'select b.*, IF(b.name != 1, \'false\', \'false\') AS selected
			from brands b, (select distinct p.id_brand
			from products p, (SELECT c.id
			FROM categories c
			where c.slug = :category_slug) cat_id
			where p.id_category = cat_id.id) brand_id
			where b.id = brand_id.id_brand', 
			['category_slug' => $slug]
		);

    	return $brands;
    }
}
