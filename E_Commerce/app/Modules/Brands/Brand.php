<?php

namespace App\Modules\Brands;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Products\Product;
use Cviebrock\EloquentSluggable\Sluggable;

class Brand extends Model
{
	use Sluggable;
	
	protected $table = "brands";
	
	protected $fillable = [
		'name', 
		'photo_url',
	];
	
	public function products(){
		return $this->hasMany(Product::class, 'id_brand', 'id');
	}
	
	/**
	 * Return the sluggable configuration array for this model.
	 *
	 * @return array
	 */
	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'name'
			]
		];
	}
}
