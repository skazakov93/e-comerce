<?php

namespace App\Modules\Users;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Modules\Products\Product;
use Cviebrock\EloquentSluggable\Sluggable;

class User extends Authenticatable
{
	use Sluggable;
	
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName', 
    	'lastName', 
    	'email', 
    	'password',
    	'provider',
    	'role',
    	'photo_url',
    	'phone_number',
    ];
    
    protected $table = "users";

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
    	return [
    		'slug' => [
    			'source' => 'firstName'
    		]
    	];
    }
}
