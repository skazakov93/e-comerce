<?php

namespace App\Modules\OnSales;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\OnSales\OnSale;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class OnSalesController extends Controller
{
    public function getProductsOnSale(){
    	$products = OnSale::with(['product.images', 'product.manifacturer', 'product.availableAtWarehouses'])
    				->where('ending_date', '>=', 'starting_date')
    				->where('ending_date', '>=', Carbon::today())
    				->get();

    	if(count($products) > 7){
    		//vrakanje na 7 produkti, odbrani po slucaen izbor!!!
    		return $products->random(7);	
    	}
    	return $products;
    }
}
