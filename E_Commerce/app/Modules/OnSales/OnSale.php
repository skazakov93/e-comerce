<?php

namespace App\Modules\OnSales;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Products\Product;

class OnSale extends Model
{
	protected $table = "on_sales";
	
	public $timestamps = false;
	
	protected $fillable = [
		'id_product',
		'id_sale',
		'price',
		'starting_date',
		'ending_date',
	];
	
	//da se testira
	public function product(){
		return $this->belongsTo(Product::class, 'id_product');
	}
}
