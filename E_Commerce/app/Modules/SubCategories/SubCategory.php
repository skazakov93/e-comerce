<?php

namespace App\Modules\SubCategories;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Categories\Category;

class SubCategory extends Model
{
	protected $table = "sub_categories";
	
	public $timestamps = false;
	
	protected $fillable = [
		'id_category',
		'id_subcategory',
		'position',
	];
	
	public function category(){
		return $this->belongsTo(Category::class, 'id_category');
	}
	
	public function subCategory(){
		return $this->belongsTo(Category::class, 'id_subcategory');
	}
}
