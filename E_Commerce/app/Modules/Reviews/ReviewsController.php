<?php

namespace App\Modules\Reviews;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Reviews\Review;
use Illuminate\Support\Facades\DB;

class ReviewsController extends Controller
{
    public function getTopRatedProducts(){
    	$products = Review::with('product.images', 'product.onSaleProducts')->select('id_product', DB::raw('avg(rating) as average_rating'))
                 ->groupBy('id_product')
                 ->orderBy('average_rating', 'desc')
                 ->limit(3)
                 ->get();

    	return $products;
    }
}
