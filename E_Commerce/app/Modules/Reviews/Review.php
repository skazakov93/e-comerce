<?php

namespace App\Modules\Reviews;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Products\Product;
use App\Modules\Customers\Customer;

class Review extends Model
{
	protected $table = "reviews";
	
	protected $fillable = [
		'id_product',
		'id_user',
		'rating',
		'description',
	];
	
	public function user(){
		return $this->belongsTo(Customer::class, 'id_user');
	}
	
	public function product(){
		return $this->belongsTo(Product::class, 'id_product');
	}
}
