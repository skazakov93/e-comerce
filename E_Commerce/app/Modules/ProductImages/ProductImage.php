<?php

namespace App\Modules\ProductImages;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Products\Product;

class ProductImage extends Model
{
	protected $table = "product_images";
	
	public $timestamps = false;
	
	protected $fillable = [
		'url',
		'id_product',
	];
	
	public function product(){
		return $this->belongsTo(Product::class, 'id_product');
	}
}
