<?php

namespace App\Modules\Buys;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Buys\Buy;
use Illuminate\Support\Facades\DB;

class BuysController extends Controller
{
    public function getMostBoughtProducts(){
    	$products = Buy::with(['product.images', 'product.manifacturer', 'product.onSaleProducts', 'product.availableAtWarehouses'])
    				->select('id_product', DB::raw('count(id_product) as product_count'))
    				->groupBy('id_product')
    				->orderBy('product_count', 'desc')
    				->limit(4)
    				->get();

    	return $products;
    }
}
