<?php

namespace App\Modules\Buys;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Products\Product;
use App\Modules\Customers\Customer;

class Buy extends Model
{
	protected $table = "buys";
	
	protected $fillable = [
		'id_product',
		'id_user',
	];
	
	public function user(){
		return $this->belongsTo(Customer::class, 'id_user');
	}
	
	public function product(){
		return $this->belongsTo(Product::class, 'id_product');
	}
}
