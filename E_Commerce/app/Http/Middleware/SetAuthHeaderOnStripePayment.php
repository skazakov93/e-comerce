<?php

namespace App\Http\Middleware;

use Closure;

class SetAuthHeaderOnStripePayment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->headers->set('authorization', 'Bearer ' . $request->input('sat-token'));

        return $next($request);
    }
}
