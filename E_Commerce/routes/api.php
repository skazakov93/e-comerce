<?php

use Illuminate\Http\Request;

header("Access-Control-Allow-Origin: *");

header("Access-Control-Allow-Methods: DELETE");

header('Access-Control-Allow-Headers: Content-Type, Authorization');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Facebook Authentication
Route::get('auth/facebook', 'Customers\FacebookLoginController@login');
Route::post('auth/facebook', 'Customers\FacebookLoginController@login');

// Twitter Authentication
Route::get('auth/twitter', 'Customers\TwitterLoginController@login');
Route::post('auth/twitter', 'Customers\TwitterLoginController@login');

// Google+ Authentication
Route::get('auth/google', 'Customers\GoogleLoginController@login');
Route::post('auth/google', 'Customers\GoogleLoginController@login');

Route::group(array('prefix' => 'api'), function() {
	Route::post('register', 'Customers\AuthenticateController@registerUser');	

	Route::post('authenticate', 'Customers\AuthenticateController@authenticate');

	Route::get('authenticate/user', 'Customers\AuthenticateController@getAuthenticatedUser');

	Route::get('authenticate/userLogOut', 'Customers\AuthenticateController@logOutUser');

	// Github Authentication
	Route::get('github/login', 'Customers\GitHubLoginController@login');
	
	///////////////////////////////
	Route::get('getCategories', 'Categories\CategoriesController@getCategories');
	
	Route::get('productsInCategory/{slug}', 'Products\ProductsController@proudctsInCategory');

	Route::get('product/{slug}', 'Products\ProductsController@productDetails');

	Route::get('topRatedProducts', 'Reviews\ReviewsController@getTopRatedProducts');

	Route::get('recentlyVisitedProducts', 'Products\ProductsController@recentlyViewedProducts');

	Route::get('latestProducts', 'Products\ProductsController@getLatestProducts');

	Route::get('productsOnSale', 'OnSales\OnSalesController@getProductsOnSale');

	Route::get('recomendedProducts', 'Products\ProductsController@getRecomendedProducts');	

	Route::get('latestBrands', 'Brands\BrandsController@getLatestBrands');	

	Route::get('mostBoughtProducts', 'Buys\BuysController@getMostBoughtProducts');	

	Route::get('categoryBrands/{slug}', 'Brands\BrandsController@getCategoryBrands');	

	Route::post('productsByCategoryBrands', 'Products\ProductsController@getProductsByCategoryBrands');

	Route::get('category/{slug}', 'Categories\CategoriesController@getCategoryName');

	Route::post('reviewForProduct', 'Customers\CustomerController@postReviewForProduct');

	Route::get('productReviews/{slug}', 'Products\ProductsController@getProductReviews');

	Route::get('productRating/{slug}', 'Products\ProductsController@getProductRating');

	Route::post('buyProducts', 'Customers\CustomerController@customerBuysProducts');
});
