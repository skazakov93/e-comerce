angularAppStat.directive('owlCarousel', function() {
	return {
        restrict: 'E',
        transclude: false,
        link: function (scope) {
            scope.initCarousel = function(element) {
                // provide any default options you want
                var defaultOptions = {
                };
                var customOptions = scope.$eval($(element).attr('data-options'));
                // combine the two options objects
                for(var key in customOptions) {
                    defaultOptions[key] = customOptions[key];
                }
                //defaultOptions['slideBy'] = 1;

                // init carousel
                $(element).owlCarousel(defaultOptions);

                $(element).data('owlCarousel').destroy();

                $(element).owlCarousel(defaultOptions);

                //za selektiranje na inicijalniot thumbnail
                $(".horizontal-thumb").first().addClass("active");
            };

            scope.prevFunction = function(element) {
                // Custom Navigation Events
                $('#prev1').click(function() {
                    $(element).trigger('owl.prev');
                })
            };

            scope.nextFunction = function(element) {
                // Custom Navigation Events
                $('#next1').click(function() {
                    $(element).trigger('owl.next');
                })
            };
        }
    };
});