angularAppStat.directive('owlCarouselItem', function() {
	return {
        restrict: 'A',
        transclude: false,
        link: function(scope, element) {
            var sync1 = $("#owl-single-product-my");
            var sync2 = $("#owl-single-product-thumbnails-my");

            scope.syncPosition = function(el){
                var current = this.currentItem;
                $("#owl-single-product-thumbnails-my")
                  .find(".owl-item")
                  .removeClass("synced")
                  .eq(current)
                  .addClass("synced");

                if($("#owl-single-product-thumbnails-my").data("owlCarousel") !== undefined){
                  center(current);
                }
            }

            // wait for the last item in the ng-repeat then call init
            if(scope.$last) {
                scope.initCarousel(element.parent());

                $("#owl-single-product-thumbnails-my").on("click", ".owl-item", function(e){
                    e.preventDefault();

                    //za obelezuvanje koj e kliknat!!!!
                    $(".horizontal-thumb").removeClass("active");
                    $(this).children().children().addClass("active");

                    var number = $(this).data("owlItem");
                    sync1.trigger("owl.goTo",number);
                });

                scope.center = function(number){
                    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
                    var num = number;
                    var found = false;

                    for(var i in sync2visible){
                      if(num === sync2visible[i]){
                        var found = true;
                      }
                    }
                 
                    if(found===false){
                      if(num>sync2visible[sync2visible.length-1]){
                        sync2.trigger("owl.goTo", num - sync2visible.length+2);
                      }else{
                        if(num - 1 === -1){
                          num = 0;
                        }
                        sync2.trigger("owl.goTo", num);
                      }
                    } else if(num === sync2visible[sync2visible.length-1]){
                      sync2.trigger("owl.goTo", sync2visible[1]);
                    } else if(num === sync2visible[0]){
                      sync2.trigger("owl.goTo", num-1);
                    }
                    
                };
            }
        }
    };
});