angularAppStat.directive('owlCarouselMostVisitedProducts', function() {
    return {
        restrict: 'A',
        transclude: false,
        link: function(scope, element) {
            // wait for the last item in the ng-repeat then call init
            if(scope.$last) {
                scope.initCarousel(element.parent());

                scope.prevFunction(element.parent());
                scope.nextFunction(element.parent());
            }
        }
    };
});