angularAppStat.controller('SaveToSatelizer', 
    ['$http', '$rootScope', '$stateParams', '$state', 
        function ($http, $rootScope, $stateParams, $state) {

    var vm = this;

    saveToken();

   	function saveToken (){
   		vm.token = ($stateParams.token) ? $stateParams.token : null;

   		// Grab the user from local storage and parse it to an object
	    var tokenS = localStorage.getItem('satellizer_token');

	    if(!tokenS){
	    	// Save the returned token in localStorage
        	localStorage.setItem('satellizer_token', vm.token);
	    }
        
        getUserData();
    };

    function getUserData() {
    	$http.get(api_url + 'authenticate/user')
    		.then(function(response) {
    			// Grab the token from local storage
	    		var tokenS = localStorage.getItem('satellizer_token');

	    		if(tokenS){
	    			// Stringify the returned data to prepare it
		            // to go into local storage
		            var user = JSON.stringify(response.data.user);

		            // Set the stringified user data into local storage
		            localStorage.setItem('user', user);

		            // The user's authenticated state gets flipped to
		            // true so we can now show parts of the UI that rely
		            // on the user being logged in
		            $rootScope.authenticated = true;

		            // Putting the user's data on $rootScope allows
		            // us to access it anywhere across the app
		            $rootScope.currentUser = response.data.user;
	    		}
	            
	            // Everything worked out so we can now redirect to
	            // the users state to view the data
	            $state.go('index');
	        });
    }

}]);