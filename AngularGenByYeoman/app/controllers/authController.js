angularAppStat.controller('AuthController', 
    ['$http', '$log', '$auth', '$state', '$rootScope', 
        function ($http, $log, $auth, $state, $rootScope) {

    var vm = this;

    vm.invalidCredidentials = false;
    vm.emailNotValid = false;
    vm.passwordRequired = false;

    vm.invalidError = "";
    vm.mailError = "";
    vm.passwordError = "";
    vm.loading = false;
    
    vm.login = function() {

        // Dokolku kliknam na Logiraj se kopceto, da se prikaze loading bar!!
        vm.loading = true;

        var credentials = {
            email: vm.email,
            password: vm.password
        }

        $auth.login(credentials).then(function() {

            // Return an $http request for the now authenticated
            // user so that we can flatten the promise chain
            return $http.get(api_url + 'authenticate/user');

        // Handle errors
        }, function(error) {
            // da se iscisti loading bar-ot
            vm.loading = false;

            vm.loginError = true;
            vm.loginErrorText = error.data.error;

            if(error.status == 401){
                vm.invalidCredidentials = true;
                vm.invalidError = error.data.error;
            }
            else if(error.state != 401){
                vm.invalidCredidentials = false;
                vm.invalidError = "";
            }

            if(error.data.email != null){
                vm.emailNotValid = true;
                vm.mailError = error.data.email[0];
            }
            else{
                vm.emailNotValid = false;
                vm.mailError = "";   
            }
            
            if(error.data.password != null){
                vm.passwordRequired = true;
                vm.passwordError = error.data.password[0];
            }
            else{
                vm.passwordRequired = false;
                vm.passwordError = "";   
            }

        // Because we returned the $http.get request in the $auth.login
        // promise, we can chain the next promise to the end here
        }).then(function(response) {

            // Stringify the returned data to prepare it
            // to go into local storage
            var user = JSON.stringify(response.data.user);

            // Set the stringified user data into local storage
            localStorage.setItem('user', user);

            // The user's authenticated state gets flipped to
            // true so we can now show parts of the UI that rely
            // on the user being logged in
            $rootScope.authenticated = true;

            // Putting the user's data on $rootScope allows
            // us to access it anywhere across the app
            $rootScope.currentUser = response.data.user;

            // da se iscisti loading bar-ot
            vm.loading = false;

            // Everything worked out so we can now redirect to
            // the users state to view the data
            $state.go('index');
        });
    }

    vm.authenticate = function(provider){
        vm.loading = true;

        $auth.authenticate(provider).then( function(response) {
            vm.loading = true;

            // Return an $http request for the now authenticated
            // user so that we can flatten the promise chain
            return $http.get(api_url + 'authenticate/user');
        }, function(error) {
            vm.loading = false;
        }).then(function(response) {

            // Stringify the returned data to prepare it
            // to go into local storage
            var user = JSON.stringify(response.data.user);

            // Set the stringified user data into local storage
            localStorage.setItem('user', user);

            // The user's authenticated state gets flipped to
            // true so we can now show parts of the UI that rely
            // on the user being logged in
            $rootScope.authenticated = true;

            // Putting the user's data on $rootScope allows
            // us to access it anywhere across the app
            $rootScope.currentUser = response.data.user;

            // Everything worked out so we can now redirect to
            // the users state to view the data

            vm.loading = false;

            $state.go('index');
        });
    }

}]);