angularAppStat.service('brandService', 
    ['$http', '$log', '$rootScope', 'toastr',
        function($http, $log, $rootScope, toastr) {

    var service = {
        getCategoryBrands: getCategoryBrands,
        getProductsByCategoryBrand: getProductsByCategoryBrand,
        getLatestBrands: getLatestBrands
    };

    return service;

    function getCategoryBrands(slug) {
        return $http.get(api_url + 'categoryBrands/' + slug)
            .then(getCategoryBrandsComplete, errorWithServer);

        function getCategoryBrandsComplete(data, status, headers, config) {
            return data.data;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting the Category Brands', 'Error!');
        }
    }

    function getProductsByCategoryBrand(data1, currentPage, sortBy, itemsPerPage) {
        return $http({
                    method: 'POST',
                    url: api_url + 'productsByCategoryBrands?page=' + currentPage + '&sortBy=' + sortBy + '&itemsPerPage=' + itemsPerPage,
                    data: data1
                })
            .then(getProductsByCategoryBrandComplete, errorWithServer);

        function getProductsByCategoryBrandComplete(data, status, headers, config) {
            return data.data;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting the Category Brands', 'Error!');
        }
    }

    function getLatestBrands() {
        return $http.get(api_url + 'latestBrands')
            .then(getLatestBrandsComplete, errorWithServer);

        function getLatestBrandsComplete(data, status, headers, config) {
            return data.data;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting the Latest Brands', 'Error!');
        }
    }

}]);