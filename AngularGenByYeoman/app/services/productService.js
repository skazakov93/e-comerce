angularAppStat.service('productService', 
    ['$http', '$log', '$rootScope', 'toastr',
        function($http, $log, $rootScope, toastr) {

    var primePromise;

    var service = {
        getProductsInCategory: getProductsInCategory,
        getProductDetails: getProductDetails,
        getProductsOnSale: getProductsOnSale,
        getRecomendedProducts: getRecomendedProducts,
        getTopRatedProducts: getTopRatedProducts,
        getProductsOnSaleFooter: getProductsOnSaleFooter,
        getRecomendedProductsFooter: getRecomendedProductsFooter,
        getMostViewedProducts: getMostViewedProducts,
        getLatestProducts: getLatestProducts,
        getProductsOnSaleIndex: getProductsOnSaleIndex,
        getMostBoughtProducts: getMostBoughtProducts,
        postProductReview: postProductReview,
        getProductReviews: getProductReviews, 
        getProductRating: getProductRating,
        ready: ready
    };

    return service;

    function getProductsInCategory(slug, currentPage, sortBy, itemsPerPage) {
        return $http.get(api_url + 'productsInCategory/' + slug + '?page=' + currentPage + '&sortBy=' + sortBy + '&itemsPerPage=' + itemsPerPage)
            .then(getProductsInCategoryComplete, errorWithServer);

        function getProductsInCategoryComplete(data, status, headers, config) {
            return data.data;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting the Products', 'Error!');
        }
    }

    function getProductDetails(slug) {
        return $http.get(api_url + 'product/' + slug)
            .then(getProductDetailsComplete, errorWithServer);

        function getProductDetailsComplete(data, status, headers, config) {
            return data.data;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting the Product Details', 'Error!');
        }
    }

    function getProductsOnSale() {
        return $http.get(api_url + 'productsOnSale')
            .then(getProductsOnSaleComplete, errorWithServer);

        function getProductsOnSaleComplete(data, status, headers, config) {
            var procesedData = [];

            var br = 0;
            angular.forEach(data.data, function(value, key) {
                if(br < 4){
                    procesedData.push(value);
                }

                br++;
            });

            return procesedData;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting the Products on Sale', 'Error!');
        }
    } 

    function getRecomendedProducts(slug) {
        return $http.get(api_url + 'recomendedProducts')
            .then(getRecomendedProductsComplete, errorWithServer);

        function getRecomendedProductsComplete(data, status, headers, config) {
            return data.data;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting the Recomended Products', 'Error!');
        }
    } 

    function getTopRatedProducts() {
        return $http.get(api_url + 'topRatedProducts')
            .then(getTopRatedProductsComplete, errorWithServer);

        function getTopRatedProductsComplete(data, status, headers, config) {
            return data.data;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting the Top Rated Products', 'Error!');
        }
    }

    function getProductsOnSaleFooter(){
        return $http.get(api_url + 'productsOnSale')
            .then(getProductsOnSaleFooterComplete, errorWithServer);

        function getProductsOnSaleFooterComplete(data, status, headers, config) {
            var procesedData = [];

            var br = 0;
            angular.forEach(data.data, function(value, key) {
                if(br < 3){
                    procesedData.push(value);
                }

                br++;
            });

            return procesedData;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting the Products on Sale', 'Error!');
        }
    } 

    function getRecomendedProductsFooter(slug) {
        return $http.get(api_url + 'recomendedProducts')
            .then(getRecomendedProductsFooterComplete, errorWithServer);

        function getRecomendedProductsFooterComplete(data, status, headers, config) {
            var procesedData = [];

            var br = 0;
            angular.forEach(data.data, function(value, key) {
                if(br < 3){
                    procesedData.push(value);
                }

                br++;
            });

            return procesedData;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting the Recomended Products', 'Error!');
        }
    }

    function getMostViewedProducts() {
        return $http.get(api_url + 'recentlyVisitedProducts')
            .then(getMostViewedProductsComplete, errorWithServer);

        function getMostViewedProductsComplete(data, status, headers, config) {
            return data.data;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting the Most Viewed Products', 'Error!');
        }
    }

    function getLatestProducts() {
        return $http.get(api_url + 'latestProducts')
            .then(getLatestProductsComplete, errorWithServer);

        function getLatestProductsComplete(data, status, headers, config) {
            return data.data;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting the Latest Products', 'Error!');
        }
    } 

    function getProductsOnSaleIndex() {
        return $http.get(api_url + 'productsOnSale')
            .then(getProductsOnSaleIndexComplete, errorWithServer);

        function getProductsOnSaleIndexComplete(data, status, headers, config) {
            var products1 = [];

            var products2 = [];

            var mainProduct = null;

            var br = 0;
            angular.forEach(data.data, function(value, key) {
                if(br < 3){
                    products1.push(value);
                }
                else if(br < 6){
                    products2.push(value);
                }
                else if(br == 6){
                    mainProduct = value;
                }

                br++;
            });

            var procesedData = {
                firstList: products1,
                secoundList: products2,
                mainProduct: mainProduct
            };

            return procesedData;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting the Products on Sale', 'Error!');
        }
    }

    function getMostBoughtProducts() {
        return $http.get(api_url + 'mostBoughtProducts')
            .then(getMostBoughtProductsComplete, errorWithServer);

        function getMostBoughtProductsComplete(data, status, headers, config) {
            var products = [];

            angular.forEach(data.data, function(value, key) {
                products.push(value.product);
            });

            return products;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting the Latest Products', 'Error!');
        }
    } 

    function postProductReview(data1) {
        return $http({  
                    method: 'POST',
                    url: api_url + 'reviewForProduct',
                    data: data1
                })
            .then(postProductReviewComplete, errorWithServer);

        function postProductReviewComplete(data, status, headers, config) {
            $rootScope.descriptionFieldError = null;

            // Proverka dali user-ot ima postaveno ocena, i povtorno praka nova ocena
            if(data.data[0] == "NO"){
                toastr.info("Веќе имате поставено оцена за продуктот. Не можете да ја промените!", 'Безбедност');  
            }

            return data.data;
        } 

        function errorWithServer(data, status) {
            $rootScope.descriptionFieldError = null;
            
            if(data.data.description){
                //toastr.error("Рецензијата е задолжително поле!!!", 'Error');
                $rootScope.descriptionFieldError = "Рецензијата е задолжително поле!!!";
            }
            else if(data.data.error == "token_not_provided" || data.data.error == "token_expired" || data.data.error == "token_absent" || data.data.error == "token_invalid"){
                toastr.error("За да постирате рецензија потребно е да сте најавени!!", 'Не сте Најавени!');
            }
            else{
                toastr.error('XHR Failed while posting the product Review', 'Error!');
            }
        }
    }

    function getProductReviews(slug) {
        return $http.get(api_url + 'productReviews/' + slug)
            .then(getProductReviewsComplete, errorWithServer);

        function getProductReviewsComplete(data, status, headers, config) {
            return data.data;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting the product reviews', 'Error!');
        }
    }

    function getProductRating(slug) {
        return $http.get(api_url + 'productRating/' + slug)
            .then(getProductRatingComplete, errorWithServer);

        function getProductRatingComplete(data, status, headers, config) {
            return data.data;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting product rating', 'Error!');
        }
    }
    
    function ready(nextPromises) {
        var readyPromise = primePromise || prime();

        return readyPromise
            .then(function() { return $q.all(nextPromises); })
            .catch(exception.catcher('"ready" function failed'));
    }

}]);