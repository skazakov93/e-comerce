angularAppStat.service('userService', 
    ['$http', '$log', '$rootScope', 'toastr',
        function($http, $log, $rootScope, toastr) {

    var service = {
        registerUser: registerUser,
        logOutUser: logOutUser
    };

    return service;

    function registerUser(data1) {
        return $http({
                    method: 'POST',
                    url: api_url + 'register',
                    data: data1,
                    headers: {'Content-Type': undefined},
                    transformRequest: angular.identity
                })
            .then(registerUserComplete, errorWithServer);

        function registerUserComplete(data, status, headers, config) {
           	eraseAllErrors();

            var token = JSON.stringify(data.data.token);

            // Set the stringified user data into local storage
            localStorage.setItem('satellizer_token', token);

            $http.get(api_url + 'authenticate/user').then(function(response) {
                // Stringify the returned data to prepare it
                // to go into local storage
                var user = JSON.stringify(response.data.user);

                // Set the stringified user data into local storage
                localStorage.setItem('user', user);

                // The user's authenticated state gets flipped to
                // true so we can now show parts of the UI that rely
                // on the user being logged in
                $rootScope.authenticated = true;

                // Putting the user's data on $rootScope allows
                // us to access it anywhere across the app
                $rootScope.currentUser = response.data.user;
            });

            return data.data;
        } 

        function errorWithServer(data, status) {
            eraseAllErrors();

            if(data.data.firstName){
                $rootScope.firstNameFieldError = "Името е задолжително поле!!!";
            }
            if(data.data.lastName){
                $rootScope.lastNameFieldError = "Презимето е задолжително поле!!!";
            }
            if(data.data.password && data.data.password == 'The password field is required.'){
                $rootScope.passwordFieldRequired = "Лозинката е задолжително поле!!!";
            }
            if(data.data.password && data.data.password[0] == 'The password must be at least 6  characters.'){
            	$rootScope.passwordFieldLength = "Лозинката е потребно е да биде барем 6 карактера!!!";
            }
            if(data.data.password && data.data.password == 'The password confirmation does not match.'){
                $rootScope.passwordFieldNotMatch = "Лозинките не се еднакви!!!";
            }

            if(data.data.password && data.data.password[0] == 'The password must be at least 6  characters.'){
            	$rootScope.passwordFieldLength = "Лозинката е потребно е да биде барем 6 карактера!!!";
            }
            if(data.data.password && data.data.password[0] == 'The password confirmation does not match.'){
                $rootScope.passwordFieldNotMatch = "Лозинките не се еднакви!!!";
            }

            if(data.data.password && data.data.password[1] == 'The password must be at least 6  characters.'){
            	$rootScope.passwordFieldLength = "Лозинката е потребно е да биде барем 6 карактера!!!";
            }
            if(data.data.password && data.data.password[1] == 'The password confirmation does not match.'){
                $rootScope.passwordFieldNotMatch = "Лозинките не се еднакви!!!";
            }

            if(data.data.email && data.data.email == 'The email field is required.'){
                $rootScope.emailFieldRequired = "E-mail-от е задолжително поле!!!";
            }
            if(data.data.email && data.data.email == 'The email must be a valid email address.'){
                $rootScope.emailFieldNotValid = "Внесете валиден е-mail!!!";
            }
            if(data.data.email && data.data.email == 'The email has already been taken.'){
                $rootScope.emailFieldused = "E-mail-от е веќе искористен. Внесете друга е-mail адреса!!!";
            }
            if(data.data.phone_number){
                $rootScope.phoneNumFieldError = "Мобилниот број е задолжително поле!!!";
            }
            if(data.data.password_confirmation && $rootScope.passwordFieldRequired && $rootScope.passwordFieldLength && $rootScopepasswordFieldNotMatch){
            	$rootScope.passwordConfirmationFieldError = "Потврдете ја внесената лозинка!!!";
            }
            if(data.data.photo){
                $rootScope.photoFieldError = "Потребно е да закачите валиден формат на слика!!!"; 
            }

            return -1;
        }
    }

    function logOutUser() {
        return $http.get(api_url + 'authenticate/userLogOut')
            .then(logOutUserComplete, logOutUserComplete);

        function logOutUserComplete(data, status, headers, config) {
            // remove the Satellizer Token from Local Storage
            localStorage.removeItem('satellizer_token');

            // remove the user from Local Storage
            localStorage.removeItem('user');

            // set the Current User to null
            $rootScope.currentUser = null;

            return data.data;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while Loggin Out the user', 'Error!');
        }
    }

    function eraseAllErrors() {
    	$rootScope.firstNameFieldError = null;
        $rootScope.lastNameFieldError = null;
        $rootScope.passwordFieldRequired = null;
        $rootScope.passwordFieldLength = null;
        $rootScope.passwordFieldNotMatch = null;
        $rootScope.emailFieldRequired = null;
        $rootScope.emailFieldNotValid = null;
        $rootScope.emailFieldused = null;
        $rootScope.phoneNumFieldError = null;
        $rootScope.passwordConfirmationFieldError = null;
        $rootScope.photoFieldError = null;
    }
}]);