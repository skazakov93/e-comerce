angularAppStat.service('categoryService', 
    ['$http', '$log', '$rootScope', 'toastr',
        function($http, $log, $rootScope, toastr) {

    var service = {
        getAllCategories: getAllCategories,
        getCategoryNameBySlug: getCategoryNameBySlug
    };

    return service;

    function getAllCategories(slug, currentPage) {
        return $http.get(api_url + 'getCategories')
            .then(getAllCategoriesComplete, errorWithServer);

        function getAllCategoriesComplete(data, status, headers, config) {
            return data.data;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting the Categories', 'Error!');
        }
    }

    function getCategoryNameBySlug(slug) {
        return $http.get(api_url + 'category/' + slug)
            .then(getCategoryNameBySlugComplete, errorWithServer);

        function getCategoryNameBySlugComplete(data, status, headers, config) {
            return data.data;
        } 

        function errorWithServer() {
            toastr.error('XHR Failed while getting the Category Name', 'Error!');
        }
    }

}]);