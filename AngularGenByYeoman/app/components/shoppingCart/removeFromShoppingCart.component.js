angularAppStat.component("removeFromShoppingCart", {
    template: `<a ng-click="vm.removeFromCart(vm.product.slug)" class="close-btn" href=""></a>`,
            
    bindings: {
        product: '='
    },
    controllerAs: "vm",
    controller: ['$rootScope', 
        function ($rootScope) {
    	
        var vm = this;

        vm.removeFromCart = function(slug){
            for(i = 0; i < $rootScope.cartItems.length; i++){
                if($rootScope.cartItems[i].slug == slug){
                    $rootScope.cartItems.splice(i, 1);
                    break;
                }
            }

            getCartPrice();

            getTotalPrice();

            //Remove the authenticated user from local storage
            localStorage.removeItem('shoppingCart');

            //Set the stringified user data into local storage
            localStorage.setItem('shoppingCart', JSON.stringify($rootScope.cartItems));
        }

        var getCartPrice = function(){
	        $rootScope.cartPrice = 0;

	        for(i = 0; i < $rootScope.cartItems.length; i++){
	            $rootScope.cartPrice += $rootScope.cartItems[i].price;
	        }  
	    }

        var getTotalPrice = function(){
            $rootScope.totalPrice = $rootScope.cartPrice;

            if($rootScope.deliveryType == 'local'){
                $rootScope.totalPrice += 200;
            }
        }
    }],
});