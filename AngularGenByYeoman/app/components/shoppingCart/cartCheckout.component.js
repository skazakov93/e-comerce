angularAppStat.component("cartCheckout", {
    templateUrl: "../views/partials/cartCheckout.component.html",
    controllerAs: "vm",
    controller: ['$rootScope',
        function ($rootScope) {
    	
        var vm = this;

        vm.token_satellizer;

        vm.products = [];

        vm.cvc = null;
        vm.expiry = null;
        vm.number = null;

    	// Grab the Shopping Cart from local storage and parse it to an object
        var localCart = JSON.parse(localStorage.getItem('shoppingCart'));
        
        if(localCart){
            $rootScope.cartItems = localCart;
        }
        else{
            $rootScope.cartItems = [];
        }

        $rootScope.cartPrice = 0;
        $rootScope.deliveryType = 0;
        $rootScope.totalPrice = 0;

        var getCartPrice = function(){
            $rootScope.cartPrice = 0;

            for(i = 0; i < $rootScope.cartItems.length; i++){
                $rootScope.cartPrice += $rootScope.cartItems[i].price;
            }  
        }

        getCartPrice();

	    vm.choosenDelivery = function(type){
	        $rootScope.deliveryType = type;

	        getTotalPrice();
	    }

	    var getTotalPrice = function(){
	        $rootScope.totalPrice = $rootScope.cartPrice;

	        if($rootScope.deliveryType == 'local'){
	            $rootScope.totalPrice += 200;
	        }
	    }

	    getTotalPrice();

      function fetchTokenFromLocalStorage() {
          vm.token_satellizer = localStorage.getItem('satellizer_token');
      }

      fetchTokenFromLocalStorage();

      vm.submit = function() {

          console.log('YUUU');
          console.log(vm.products);

          $http({
            method: 'GET',
            url: api_url + 'buyProducts',
            data: 'test',
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('satellizer_token') } 
           })
            .success(function(data) {
              console.log(data);

              if (!data.success) {
                // if not successful, bind errors to error variables
                //$scope.errorName = data.errors.name;
                //$scope.errorSuperhero = data.errors.superheroAlias;
              } else {
                // if successful, bind success message to message
                //$scope.message = data.message;
              }
            });
        }

        vm.submitToMyServer = function (code, result) {
            console.log("YYYY");
            console.log(result);

            if (result.error) {
                window.alert('it failed! error: ' + result.error.message);
            } else {
                window.alert('success! token: ' + result.id);
            }
        };

    }],
});