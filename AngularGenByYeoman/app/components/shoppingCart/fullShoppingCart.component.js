angularAppStat.component("fullShoppingCart", {
    templateUrl: "../views/partials/fullShoppingCart.component.html",
    controllerAs: "vm",
    controller: ['$rootScope', 
        function ($rootScope) {
    	
        var vm = this;

        /**************************************************************************/
        vm.choosenDelivery = choosenDelivery;
        vm.getCartPrice = getCartPrice;
        vm.getTotalPrice = getTotalPrice;
        /**************************************************************************/

        initializeCart();

        $rootScope.deliveryType = 0;
    	$rootScope.totalPrice = 0;

        function getCartPrice(){
            $rootScope.cartPrice = 0;

            for(i = 0; i < $rootScope.cartItems.length; i++){
                $rootScope.cartPrice += $rootScope.cartItems[i].price;
            }  
        }

	    function choosenDelivery(type){
	        $rootScope.deliveryType = type;

	        getTotalPrice();
	    }

	    function getTotalPrice(){
	        $rootScope.totalPrice = getCartPrice();

	        if($rootScope.deliveryType == 'local'){
	            $rootScope.totalPrice += 200;
	        }
	    }

        function initializeCart(){
            // Grab the user from local storage and parse it to an object
            var localCart = JSON.parse(localStorage.getItem('shoppingCart'));
            
            console.log(localCart);

            if(localCart){
                $rootScope.cartItems = localCart;
            }
            else{
                $rootScope.cartItems = [];
            }
        }
    }],
});