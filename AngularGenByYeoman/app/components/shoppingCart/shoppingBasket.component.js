angularAppStat.component("shoppingBasket", {
    templateUrl: "../views/partials/shoppingBasket.component.html",
    controllerAs: "vm",
    controller: ['$rootScope', 
        function ($rootScope) {
    	
        var vm = this;

    	// Grab the user from local storage and parse it to an object
        var localCart = JSON.parse(localStorage.getItem('shoppingCart'));
        
        if(localCart){
            $rootScope.cartItems = localCart;
        }
        else{
            $rootScope.cartItems = [];
        }

        $rootScope.cartPrice = 0;
        $rootScope.deliveryType = 0;
    	$rootScope.totalPrice = 0;

        var getCartPrice = function(){
            $rootScope.cartPrice = 0;

            for(i = 0; i < $rootScope.cartItems.length; i++){
                $rootScope.cartPrice += $rootScope.cartItems[i].price;
            }  
        }

        getCartPrice();

	    vm.choosenDelivery = function(type){
	        $rootScope.deliveryType = type;

	        getTotalPrice();
	    }

	    var getTotalPrice = function(){
	        $rootScope.totalPrice = $rootScope.cartPrice;

	        if($rootScope.deliveryType == 'local'){
	            $rootScope.totalPrice += 200;
	        }
	    }

	    getTotalPrice();
    }],
});