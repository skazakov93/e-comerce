angularAppStat.component("addToShoppingCart", {
    template: `<a ng-click="vm.addToCart(vm.product)" href="" class="le-button {{ vm.type }} {{ vm.available }}">Додади во кошничката</a>`,
            
    bindings: {
        product: '<',
        type: '@'
    },
    controllerAs: "vm",
    controller: ['$rootScope', 'productService',
        function ($rootScope, productService) {
    	
        var vm = this;

        vm.deliveryType = 0;
        vm.totalPrice = 0;
        vm.available = "";

        vm.$onChanges = function(){
            checkIfAvailable();
        }

        checkIfAvailable();
        initializeCart();

        vm.addToCart = function(product){
            var flag = 1;
            for(i = 0; i < $rootScope.cartItems.length; i++){
                if($rootScope.cartItems[i].slug == product.slug){
                    flag = 0;
                    break;
                }
            }

            if(flag == 1){
                getProductDetails(product.slug);
            }
        }

        var getCartPrice = function(){
            $rootScope.cartPrice = 0;

            for(i = 0; i < $rootScope.cartItems.length; i++){
                $rootScope.cartPrice += $rootScope.cartItems[i].price;
            }  
        }

        var getTotalPrice = function(){
            $rootScope.totalPrice = $rootScope.cartPrice;

            if($rootScope.deliveryType == 'local'){
                $rootScope.totalPrice += 200;
            }
        }

        var getProductDetails = function(slug){
            return productService.getProductDetails(slug).then(function(data) {
                vm.procesedProduct = data;

                if(vm.procesedProduct.on_sale_products.length > 0){
                    vm.procesedProduct.price = vm.procesedProduct.on_sale_products[0].pivot.price;
                }

                if(vm.procesedProduct.available_at_warehouses[0].pivot.num_of_items > vm.procesedProduct.available_at_warehouses[0].pivot.sold_items) {
                    $rootScope.cartItems.push(vm.procesedProduct);

                    // Set the stringified user data into local storage
                    localStorage.setItem('shoppingCart', JSON.stringify($rootScope.cartItems));

                    getCartPrice();

                    getTotalPrice();
                }
                
                return $rootScope.cartItems;
            });
        }

        function initializeCart(){
            // Grab the user from local storage and parse it to an object
            var localCart = JSON.parse(localStorage.getItem('shoppingCart'));
            
            if(localCart){
                $rootScope.cartItems = localCart;
            }
            else{
                $rootScope.cartItems = [];
            }
        }

        function checkIfAvailable() {
            if(vm.product 
                && vm.product.available_at_warehouses
                && vm.product.available_at_warehouses.length > 0 
                && (vm.product.available_at_warehouses[0].pivot.num_of_items <= vm.product.available_at_warehouses[0].pivot.sold_items)) {
                
                vm.available = "disabled";
            }
            else if(vm.product 
                && vm.product.available_at_warehouses
                && vm.product.available_at_warehouses.length <= 0) {
                // ova e proverka dokolku produktot ne e dodaden vo nieden magacin!!!

                vm.available = "disabled";
            }
        }
    }],
});