angularAppStat.component("removeProductsFromCompare", {
    template: `<a ng-click="vm.removeFromCompareList(vm.product.slug)" href="" class="remove-link"><i class="fa fa-times-circle"></i></a>`,
            
    bindings: {
        product: '<'
    },
    controllerAs: "vm",
    controller: ['$rootScope', function ($rootScope) {
    	var vm = this;

	    vm.removeFromCompareList = function(slug){
	        for(i = 0; i < $rootScope.compareList.length; i++){
	            if($rootScope.compareList[i].slug == slug){
	                $rootScope.compareList.splice(i, 1);
	                break;
	            }
	        }

	        // Remove the authenticated user from local storage
	        localStorage.removeItem('comparelist');

	        // Set the stringified user data into local storage
	        localStorage.setItem('comparelist', JSON.stringify($rootScope.compareList));
	    }
    }],
});