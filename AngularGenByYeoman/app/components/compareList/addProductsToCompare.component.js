angularAppStat.component("addProductsToCompare", {
    template: `<span>
                    <a ng-click="vm.addToCompareList(vm.product)" class="btn-add-to-compare" href="">Спореди</a>
                </span>`,
            
    bindings: {
        product: '<'
    },
    controllerAs: "vm",
    controller: ['productService', '$rootScope', function (productService, $rootScope) {
    	var vm = this;

    	// Grab the user from local storage and parse it to an object
	    var localCompareList = JSON.parse(localStorage.getItem('comparelist'));
	    
	    if(localCompareList){
	        $rootScope.compareList = localCompareList;
	    }
	    else{
	        $rootScope.compareList = [];
	    }

	    var getProductDetails = function(slug){
	        return productService.getProductDetails(slug).then(function(data) {
	            $rootScope.compareList.push(data);

	            console.log(data);
	            // Set the stringified user data into local storage
	            localStorage.setItem('comparelist', JSON.stringify($rootScope.compareList));

	            return $rootScope.compareList;
	        });
	    }

	    vm.addToCompareList = function(product){
	        getProductDetails(product.slug);

	        console.log("Produkt za vo CompareList: ");
	        console.log($rootScope.compareList);
	    }
    }],
});