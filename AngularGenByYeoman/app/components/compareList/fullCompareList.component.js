angularAppStat.component("fullCompareList", {
    templateUrl: "../views/partials/fullCompareList.component.html",
    controllerAs: "vm",
    controller: ['$rootScope',
    	function ($rootScope) {
    	
    	var vm = this;

        // Grab the user from local storage and parse it to an object
	    var localCompareList = JSON.parse(localStorage.getItem('comparelist'));
	    
	    if(localCompareList){
	        $rootScope.compareList = localCompareList;
	    }
	    else{
	        $rootScope.compareList = [];
	    }
    }],
});