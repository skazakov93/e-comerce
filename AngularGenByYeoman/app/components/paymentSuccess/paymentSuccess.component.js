angularAppStat.component("paymentSuccess", {
    templateUrl: "../views/payment-success.html",
    controllerAs: "vm",
    controller: ['toastr',
    	function (toastr) {
    	
    	var vm = this;

    	succesfullyPayed();
    	clearShoppingCart();

    	function succesfullyPayed(){
    		toastr.success('Вашата трансакција беше успешна!!!', 'Success!', {
			  timeOut: 30000,
			  closeButton: true
			});
    	}

    	function clearShoppingCart(){
    		// Clear Local Shopping Cart
    		localStorage.removeItem('shoppingCart');
    	}
    }],
});