angularAppStat.component("showRating", {
    template: `<ul class="star-rating">
				  	<li ng-repeat="star in vm.listStars" class="star {{ star.selected }} starReadOnly">
				    	<i class="fa fa-star"></i>
				  	</li>
				</ul>`,
            
    bindings: {
        rating: '<'
    },
    controllerAs: "vm",
    controller: [
        function () {
    	
        var vm = this;

        showReviewRating();

        vm.$onChanges = function(){
        	showReviewRating();
        };

    	function showReviewRating() {
	    	vm.listStars = [];

	    	vm.numOfStars = 5;

	     	for(var i = 0; i < vm.numOfStars; i++){
	     		var pom = [];
	     		
	     		if(i < vm.rating){
	     			pom['selected'] = "filledStar";
	     			pom['number'] = i;	
	     		}
	     		else{
	     			pom['selected'] = "";
	     			pom['number'] = i;	
	     		}
	     		
	     		vm.listStars.push(pom);
	     	}
        }
    }],
});