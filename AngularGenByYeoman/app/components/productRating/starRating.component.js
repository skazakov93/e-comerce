angularAppStat.component("starRating", {
    templateUrl: "../views/partials/starRating.component.html",
    bindings: {
    	numOfStars: '@'
    },
    require: {
    	parent: '^addProductReview'
    },
    controllerAs: "vm",
    controller: [function () {
    	
    	var vm = this;

    	vm.rating = 0;

        showStars();
        vm.selectStar = selectStar;

	    function showStars(){
	    	vm.listStars = [];

	     	for(var i = 0; i < vm.numOfStars; i++){
	     		var pom = [];
	     		pom['selected'] = "";
	     		pom['number'] = i;

	     		vm.listStars.push(pom);
	     	}
	    }

	    function selectStar(index) {
	    	vm.rating = index + 1;

	    	vm.listStars = [];

	     	for(var i = 0; i < vm.numOfStars; i++){
	     		var pom = [];
	     		
	     		if(i <= index){
	     			pom['selected'] = "filledStar";
	     			pom['number'] = i;	
	     		}
	     		else{
	     			pom['selected'] = "";
	     			pom['number'] = i;	
	     		}
	     		
	     		vm.listStars.push(pom);
	     	}

	    	vm.parent.rating = vm.rating;
        }
    }],
});