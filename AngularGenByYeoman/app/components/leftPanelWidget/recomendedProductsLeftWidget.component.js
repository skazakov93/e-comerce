angularAppStat.component("recomendedProductsLeftWidget", {
    template: `<left-panel-products-widget products="vm.recomendedProductsCategory" section-title="vm.sectionTitle">
    			</left-panel-products-widget>`,

    controllerAs: "vm",
    controller: ['productService', function (productService) {
    	var vm = this;

    	vm.sectionTitle = "Препорачуваме";
    	vm.recomendedProductsCategory = [];

    	vm.getRecomendedProducts = function(){
	        return productService.getRecomendedProducts().then(function(data) {
	            vm.recomendedProductsCategory = data;

	            return vm.recomendedProductsCategory;
	        });
	    } 

	    vm.getRecomendedProducts();
    }],
});