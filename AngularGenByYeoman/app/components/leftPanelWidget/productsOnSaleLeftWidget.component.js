angularAppStat.component("productsOnSaleLeftWidget", {
    template: `<left-panel-products-on-sale-widget products="vm.productsOnSaleCategory" section-title="vm.sectionTitle">
    			</left-panel-products-on-sale-widget>`,

    controllerAs: "vm",
    controller: ['productService', function (productService) {
    	var vm = this;

    	vm.sectionTitle = "Продукти на Акција";
    	vm.productsOnSaleCategory = [];

	    vm.getProductsOnSale = function(){
	        return productService.getProductsOnSale().then(function(data) {
	            vm.productsOnSaleCategory = data;

	            return vm.productsOnSaleCategory;
	        });
	    }

	    vm.getProductsOnSale();
    }],
});