angularAppStat.component("verticalMenuCategories", {
    templateUrl: "../views/partials/verticalMenuCategories.component.html",
    controllerAs: "vm",
    controller: ['categoryService', '$rootScope', '$anchorScroll',
    	function (categoryService, $rootScope, $anchorScroll) {
    	
    	var vm = this;

        vm.fetchCategories = function(){
        	// call $anchorScroll()
		    $anchorScroll(['body']);
        	
	        if($rootScope.categories == null) {
	        	return categoryService.getAllCategories().then(function(data) {
		            $rootScope.categories = data;

		            return $rootScope.categories;
		        });
	        }
	    };

	    vm.fetchCategories();
    }],
});