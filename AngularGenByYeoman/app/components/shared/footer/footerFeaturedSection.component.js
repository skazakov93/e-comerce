angularAppStat.component("footerFeaturedSection", {
    templateUrl: "../views/partials/footerSectionProducts.component.html",
    controllerAs: "vm",
    controller: ['productService', 
        function (productService) {
    	
        var vm = this;

        vm.sectionTitle = "Препорачуваме";
    	vm.footerProducts = [];

    	vm.getRecomendedProductsFooter = function(){
            return productService.getRecomendedProductsFooter().then(function(data) {
                vm.footerProducts = data;

                return vm.footerProducts;
            });
        }

        vm.getRecomendedProductsFooter();
    }],
});