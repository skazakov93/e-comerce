angularAppStat.component("footerOnSaleSection", {
    templateUrl: "../views/partials/footerOnSaleSection.component.html",
    controllerAs: "vm",
    controller: ['productService', 
        function (productService) {
    	
        var vm = this;

        vm.sectionTitle = "На акција";
    	vm.footerProducts = [];

    	vm.getProductOnSaleFooter = function(){
	        return productService.getProductsOnSaleFooter().then(function(data) {
	            vm.footerProducts = data;

	            return vm.footerProducts;
	        });
	    }

	    vm.getProductOnSaleFooter();
    }],
});