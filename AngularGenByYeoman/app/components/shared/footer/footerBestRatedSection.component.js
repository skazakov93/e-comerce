angularAppStat.component("footerBestRatedSection", {
    templateUrl: "../views/partials/footerBestRatedSection.component.html",
    controllerAs: "vm",
    controller: ['productService', 
        function (productService) {
    	
        var vm = this;

        vm.sectionTitle = "Најдобро рангирани";
    	vm.footerProducts = [];

    	vm.getTopRatedProducts = function(){
	        return productService.getTopRatedProducts().then(function(data) {
	            vm.footerProducts = data;

	            return vm.footerProducts;
	        });
	    }

	    vm.getTopRatedProducts();
    }],
});