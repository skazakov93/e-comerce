angularAppStat.component("topBrandsSection", {
    templateUrl: "../views/partials/topBrandsSection.component.html",
    controllerAs: "vm",
    controller: ['brandService', 
    	function (brandService) {
    	
    	var vm = this;

    	vm.latestBrands;

    	var getLatestBrands = function(){
	        return brandService.getLatestBrands().then(function(data) {
	            vm.latestBrands = data;

	            return vm.latestBrands;
	        });
	    }

	    getLatestBrands(); 
    }],
});