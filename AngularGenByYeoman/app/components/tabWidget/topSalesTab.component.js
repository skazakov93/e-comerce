angularAppStat.component("topSalesTab", {
    templateUrl: "../views/partials/main-tabs.component.html",
    controllerAs: "vm",
    controller: ['productService', 
        function (productService) {
    	
        var vm = this;

    	vm.tabProducts = [];

    	vm.getMostBoughtProducts = function(){
	        return productService.getMostBoughtProducts().then(function(data) {
	            vm.tabProducts = data;

	            return vm.tabProducts;
	        });
	    }

	    vm.getMostBoughtProducts();
    }],
});