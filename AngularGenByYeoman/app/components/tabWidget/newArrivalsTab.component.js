angularAppStat.component("newArrivalsTab", {
    templateUrl: "../views/partials/main-tabs.component.html",
    controllerAs: "vm",
    controller: ['productService', 
        function (productService) {
    	
        var vm = this;

    	vm.tabProducts = [];

    	vm.getLatestProducts = function(){
	        return productService.getLatestProducts().then(function(data) {
	            vm.tabProducts = data;

	            return vm.tabProducts;
	        });
	    }

	    vm.getLatestProducts(); 
    }],
});