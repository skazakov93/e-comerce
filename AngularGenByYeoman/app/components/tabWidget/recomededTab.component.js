angularAppStat.component("recomededTab", {
    templateUrl: "../views/partials/main-tabs.component.html",
    controllerAs: "vm",
    controller: ['productService', 
        function (productService) {
    	
        var vm = this;

    	vm.tabProducts = [];

    	vm.getRecomendedProducts = function(){
	        return productService.getRecomendedProducts().then(function(data) {
	            vm.tabProducts = data;

	            return vm.tabProducts;
	        });
	    }  

	    vm.getRecomendedProducts(); 
    }],
});