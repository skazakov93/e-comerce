angularAppStat.component("productDetails", {
    templateUrl: "../views/product_details.html",
    controllerAs: "vm",
    controller: ['productService', '$stateParams', '$anchorScroll',
    	function (productService, $stateParams, $anchorScroll) {
    	
    	var vm = this;

    	vm.productSlug = ($stateParams.slugProduct) ? $stateParams.slugProduct : null;
    	vm.productInfo = null;
    	vm.loading = false;

    	var getProductDetails = function(slug){
	        vm.loading = true;

	        return productService.getProductDetails(slug).then(function(data) {
	            vm.productInfo = data;
	                
	            vm.loading = false;

	            return vm.productInfo;
	        });
	    }

	    if(vm.productSlug){
	    	console.log("Na klik na link za produckt!!");
	        console.log(vm.productSlug);

	        // call $anchorScroll()
		    $anchorScroll(['body']);

	        getProductDetails(vm.productSlug);
	    }
    }],
});