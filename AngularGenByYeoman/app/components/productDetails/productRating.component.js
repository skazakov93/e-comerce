angularAppStat.component("productRating", {
    template: `<show-rating rating="$root.rating"></show-rating> {{ $root.rating }}`,
    
    bindings: {
        product: '<',
    },
    controllerAs: "vm",
    controller: ['productService', '$rootScope',
        function (productService, $rootScope) {
    	
        var vm = this;

        vm.$onChanges = function(){
        	getProductRating(vm.product.slug);
        };

        function getProductRating(slug){
            return productService.getProductRating(slug).then(function(data) {
                $rootScope.ratingObj = data;

                $rootScope.rating = data.product_rating;

                return $rootScope.rating;
            });
        }
    }],
});