angularAppStat.component("productsLink", {
    template: `<a href="/#/product/{{ vm.product.slug }}">
			    	<strong ng-show="vm.strong">{{ vm.product.name | limitTo : 40 }}</strong>
			    	<span ng-hide="vm.strong">{{ vm.product.name | limitTo : 40 }}</span>
			    </a>`,
            
    bindings: {
        product: '<',
        strong: '<'
    },
    controllerAs: "vm",
    controller: [ 
    	function () {
    	
    }],
});