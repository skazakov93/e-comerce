angularAppStat.component("productReviews", {
    templateUrl: "../views/partials/productReviews.component.html",
    bindings: {
    	product: '<'
    },
    controllerAs: "vm",
    controller: ['productService', '$rootScope',
    	function (productService, $rootScope) {
    	
    	var vm = this;

    	vm.$onChanges = function () {
	     	getProductReviews(vm.product.slug);
	    };

        function getProductReviews(slug){
        	return productService.getProductReviews(slug).then(function(data) {
                $rootScope.productReviews = data;

                $rootScope.numOfReviews = $rootScope.productReviews.length;

                return $rootScope.productReviews;
            });
        }
    }],
});