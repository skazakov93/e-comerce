angularAppStat.component("addProductReview", {
    templateUrl: "../views/partials/addProductReview.component.html",
    bindings: {
    	product: '<'
    },
    controllerAs: "vm",
    controller: ['productService', '$rootScope',
    	function (productService, $rootScope) {
    	
    	var vm = this;

    	vm.postReview = postReview;
    	vm.rating = null;

		function postReview(){
			var data1 = {
				id_product: vm.product,
				description: vm.description,
				rating: vm.rating 
			};

			return productService.postProductReview(data1).then(function(data) {
	            $rootScope.productReviews.push(data[1]);

	            $rootScope.numOfReviews = $rootScope.productReviews.length;

	            vm.description = "";

	            // dokolku e zapisana ocenkata, azuriraj ja i vkupnata ocena na produktot vo product_details.html
	            if(data[1].pivot.rating){
	                var br = ++$rootScope.ratingObj.count;
	                var sum = $rootScope.ratingObj.sum + data[1].pivot.rating;

	                $rootScope.rating = sum / br;

	                $rootScope.ratingObj.rating = sum;
	            }

	            return $rootScope.productReviews;
	        });
		}
    }],
});