angularAppStat.component("categoryProducts", {
    templateUrl: "../views/list_products.html",
    controllerAs: "vm",
    controller: ['brandService', 'productService', 'categoryService', '$rootScope', '$stateParams', '$anchorScroll',
    	function (brandService, productService, categoryService, $rootScope, $stateParams, $anchorScroll) {
    	
    	var vm = this;

        // loading variable to show the spinning loading icon
	    vm.loading = false;
		vm.categorySlug = ($stateParams.slugCategory) ? $stateParams.slugCategory : null;
		vm.categoryName = "";

		vm.filtered = false;
		vm.data = null;
		vm.orderByType = null;

		vm.$onChange = function(){
			//console.log("RERERER");
			vm.pageChanged;
		}

	    function getCategoryName (slug){
	        return categoryService.getCategoryNameBySlug(slug).then(function(data) {
	            vm.categoryName = data;

	            return vm.categoryName;
	        });
	    }

	    function getCategoryProducts(slug, currentPage, sortBy, itemsPerPage) {
	        vm.loading = true;

        	return productService.getProductsInCategory(slug, currentPage, sortBy, itemsPerPage).then(function(data) {
	            $rootScope.productsInCategory = data.data;

	            vm.loading = false;

	            vm.itemsPerPage = data.per_page;
	            vm.totalItems = data.total;
	            vm.currentPage = data.current_page;
	            
	            //zemanje na imeto na kategorijata!!!
	            getCategoryName(slug);

	            return $rootScope.productsInCategory;
	        });
	    }

	    getCategoryProductsByBrand = function(data1, currentPage, sortBy, itemsPerPage){
	    	vm.loading = true;
	    	
    		return brandService.getProductsByCategoryBrand(data1, currentPage, sortBy, itemsPerPage).then(function(data) {
	            $rootScope.productsInCategory = data.data;

	            vm.loading = false;

	            vm.itemsPerPage = data.per_page;
	            vm.totalItems = data.total;
	            vm.currentPage = data.current_page;
	            
	            return $rootScope.productsInCategory;
	        });
	    } 

	    if(vm.categorySlug){
	        console.log("Na URL change, kaj Categories klik");
	        console.log(vm.categorySlug);

	        getCategoryProducts(vm.categorySlug, vm.currentPage, vm.orderByType, vm.viewby);
	    }

	    //////////////////////////////////
	    //za pagination
	    vm.viewby = 12;
	    vm.totalItems = 1;
	    vm.currentPage = 1;
	    vm.itemsPerPage = vm.viewby;
	    vm.maxSize = 5; //Number of pager buttons to show

	    vm.setPage = function (pageNo) {
	        vm.currentPage = pageNo;
	    };

	    vm.pageChanged = function() {
	        // call $anchorScroll()
		    $anchorScroll(['body']);

	        // proverka dali sme izvrsile filtriranje spored brendovite
	        if(!vm.filtered){
	        	getCategoryProducts(vm.categorySlug, vm.currentPage, vm.orderByType, vm.viewby);	
	        }
	        else if(vm.filtered){
	        	getCategoryProductsByBrand(vm.data, vm.currentPage, vm.orderByType, vm.viewby);
	        }
	    };

	    vm.setItemsPerPage = function(num) {
	        vm.itemsPerPage = num;
	        vm.currentPage = 1; //reset to first page
	    }
	    //////////////////////////////////

	    //za testiranje
		console.log("component categoryProducts");
		console.log(vm.categorySlug);
    }],
});