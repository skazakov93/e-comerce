angularAppStat.component("mainProductOnSale", {
    templateUrl: "../views/partials/mainProductOnSale.component.html",
    controllerAs: "vm",
    controller: ['productService',
    	function (productService) {
    	
    	var vm = this;

        vm.productOnSaleMain = null;

	    vm.getProductOnSale = function(){
	        return productService.getProductsOnSaleIndex().then(function(data) {
	            vm.productOnSaleMain = data['mainProduct'];

	            return vm.productOnSaleMain;
	        });
	    }

	    vm.getProductOnSale();
    }],
});