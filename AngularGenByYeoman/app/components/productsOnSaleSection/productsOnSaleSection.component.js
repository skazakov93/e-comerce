angularAppStat.component("productsOnSaleSection", {
    templateUrl: "../views/partials/productsOnSaleSection.component.html",
    controllerAs: "vm",
    controller: ['productService', 
    	function (productService) {
    	
    	var vm = this;

    	vm.productsOnSalePart1;
    	vm.productsOnSalePart2;

    	vm.getProductOnSale = function(){
	        return productService.getProductsOnSaleIndex().then(function(data) {
	            vm.productsOnSalePart1 = data['firstList'];

	            vm.productsOnSalePart2 = data['secoundList'];

	            return data;
	        });
	    }

	    vm.getProductOnSale();
    }],
});