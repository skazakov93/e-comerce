angularAppStat.component("productsPerPage", {
    templateUrl: "../views/partials/productsPerPage.component.html",
    require: {
    	parent: '^categoryProducts'
    },
    controllerAs: "vm",
    controller: [function (rootScope) {
    	
    	var vm = this;

        vm.options = [
            { name: '12', value: '1' }, 
            { name: '24', value: '2' }, 
            { name: '36', value: '3' },
            { name: '48', value: '4' },
            { name: '60', value: '5' }
        ];

    	vm.itemsPerPage = vm.options[0].value;

	    vm.changeItemsPerPage = function(){
	    	// stom go smenime brojot za prikazuvanje na produkti
	        // na edna strana, go resetirame momentalniot broj na
	        // stranata da e 1!
	        vm.parent.currentPage = 1;

			vm.parent.viewby = vm.itemsPerPage;
	    	vm.parent.pageChanged();
	    } 
    }],
});