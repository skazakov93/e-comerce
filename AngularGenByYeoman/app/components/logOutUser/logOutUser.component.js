angularAppStat.component("logOutUser", {
    template: `<a ng-click="vm.logOut()" class="hm-right2" href="" rel="nofollow">
    				Sign Out
    			</a>`,
    controllerAs: "vm",
    controller: ['userService', '$state', '$rootScope',
    	function (userService, $state, $rootScope) {
    	
    	var vm = this;

        $rootScope.logOutLoading = false;

    	vm.logOut = function() {
            $rootScope.logOutLoading = true;

    		return userService.logOutUser().then(function(data) {
	            $rootScope.logOutLoading = false;
                
                if(data == "Token refreshed!!!"){
	            	$state.go('index');
	            }
	        });
    	}
    }],
});