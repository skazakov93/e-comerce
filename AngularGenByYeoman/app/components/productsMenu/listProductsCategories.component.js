angularAppStat.component("listProductsCategories", {
    templateUrl: "../views/partials/listProductsCategories.component.html",
    controllerAs: "vm",
    controller: ['categoryService', '$rootScope',
    	function (categoryService, $rootScope) {
    	
    	var vm = this;

		vm.fetchCategories = function(){
	        if($rootScope.categories == null) {
	        	return categoryService.getAllCategories().then(function(data) {
		            $rootScope.categories = data;

		            return $rootScope.categories;
		        });
	        }
	    };

	    vm.fetchCategories();
    }],
});