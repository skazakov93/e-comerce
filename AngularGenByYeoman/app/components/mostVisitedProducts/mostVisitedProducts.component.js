angularAppStat.component("mostVisitedProducts", {
    templateUrl: '../../views/partials/mostVisitedProducts.component.html',
    controllerAs: "vm",
    controller: ['productService', '$rootScope', 
    	function (productService, $rootScope) {
    	
    	var vm = this;

    	function getRecentlyViewedProducts (){
            if($rootScope.recentlyVisitedProducts == null){
                return productService.getMostViewedProducts().then(function(data) {
                    $rootScope.recentlyVisitedProducts = data;

                    return $rootScope.recentlyVisitedProducts;
                });
            }
	    }

        getRecentlyViewedProducts();
    }],
});