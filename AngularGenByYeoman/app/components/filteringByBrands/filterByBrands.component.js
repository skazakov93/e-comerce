angularAppStat.component("filterByBrands", {
    templateUrl: "../views/partials/filterByBrands.component.html",
    bindings: {
    	categorySlug: '<'
    },
    require: {
    	parent: '^categoryProducts'
    },
    controllerAs: "vm",
    controller: ['brandService',
    	function (brandService) {
    	
    	var vm = this;

		vm.brandsInCategory = null;

	    vm.getCategoryBrands = function(slug){
	        return brandService.getCategoryBrands(slug).then(function(data) {
	            vm.brandsInCategory = data;

	            return vm.brandsInCategory;
	        });
	    } 

	    vm.getCategoryBrands(vm.categorySlug);

	    vm.choosenBrand = function(brandSlug){
	        for(var i = 0; i < vm.brandsInCategory.length; i++){
	            if(vm.brandsInCategory[i].slug == brandSlug){
	            	console.log("Selected = " + vm.brandsInCategory[i].selected);

	                if(vm.brandsInCategory[i].selected == 'false'){
	                	vm.brandsInCategory[i].selected = 'true';
	                }
	                else{
	                	vm.brandsInCategory[i].selected = 'false';
	                }
	                
	                console.log("izmena na selected!! " + vm.brandsInCategory[i].selected);
	            }
	        }

	        var selectedCategories = "";

	        var flag = false; 
	        for(var i = 0; i < vm.brandsInCategory.length; i++){
	            if(vm.brandsInCategory[i].selected == 'true'){
	                selectedCategories += vm.brandsInCategory[i].id + "#";

	                flag = true;
	            }
	        }

	        // obelezuvanje na toa dali da se pravi paginacija so vklucen filter ili na site produkti vo taa kategorija
	        // se praka do parent komponentata (categoryProducts)
	        if(flag){
	        	vm.parent.filtered = true;
	        }
	        else {
	        	vm.parent.filtered = false;	
	        }

	        var data1 = {
	            slugList: selectedCategories,
	            categorySlug: vm.categorySlug
	        };

	        // prakanje na podatocite koi sto treba da se pratat do server!!
	        // se praka do parent komponentata (categoryProducts)
	        vm.parent.data = data1;

	        vm.loading = true;

	        // stom odbereme brend, paginationot go resetirame na 1
	        // i gi zemame podatocite za toj proizvod!!!
	        // se praka do parent komponentata (categoryProducts)
	        vm.parent.currentPage = 1;
	        vm.parent.pageChanged();
	    }
    }],
});