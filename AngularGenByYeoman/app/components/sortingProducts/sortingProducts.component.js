angularAppStat.component("sortingProducts", {
    templateUrl: "../views/partials/sortingProducts.component.html",
    require: {
    	parent: '^categoryProducts'
    },
    controllerAs: "vm",
    controller: ['$rootScope',
    	function ($rootScope) {
    	
    	var vm = this;

        vm.options = [
            { name: 'Цена (ниска > висока)', value: 'priceAsc' }, 
            { name: 'Цена (висока > ниска)', value: 'priceDesc' }, 
            { name: 'Име на производ (A > Z)', value: 'nameAsc' },
            { name: 'Име на производ (Z > A)', value: 'nameDesc' },
            { name: 'Најнови производи', value: 'newest' }
        ];

    	vm.orderType = vm.options[0].value;

	    vm.sortBy = function(){
            // stom go smenime nacinot na sortiranje na produktite
            // na edna strana, go resetirame momentalniot broj na
            // stranata da e 1!
            vm.parent.currentPage = 1;

	    	vm.parent.orderByType = vm.orderType;
	    	vm.parent.pageChanged();
	    } 
    }],
});