angularAppStat.component("addToWishList", {
    template: `<div class="wish-compare">
                <a ng-click="vm.addToWishList(vm.product)" class="btn-add-to-wishlist" href="">Додади во желби</a>

                <add-products-to-compare product="vm.product"></add-products-to-compare>
            </div>`,
            
    bindings: {
        product: '<'
    },
    controllerAs: "vm",
    controller: ['productService', '$rootScope', 
        function (productService, $rootScope) {
    	
        var vm = this;

    	// Grab the user from local storage and parse it to an object
        var localWishList = JSON.parse(localStorage.getItem('wishlist'));
        
        if(localWishList){
            $rootScope.wishProducts = localWishList;
        }
        else{
            $rootScope.wishProducts = [];
        }

        var getProductDetails = function(slug){
            return productService.getProductDetails(slug).then(function(data) {
                $rootScope.wishProducts.push(data);

                // Set the stringified user data into local storage
                localStorage.setItem('wishlist', JSON.stringify($rootScope.wishProducts));

                return $rootScope.wishProducts;
            });
        }

        vm.addToWishList = function(product){
            getProductDetails(product.slug);

            console.log("Produkt za vo WishList: " + $rootScope.wishProducts);
        }
    }],
});