angularAppStat.component("removeFromWishList", {
    template: `<div>
	                <a ng-click="vm.removeFromWishList(vm.product.slug)" title="Remove this product" class="remove_from_wishlist remove-item" href="">×</a>
	            </div>`,
            
    bindings: {
        product: '<'
    },
    controllerAs: "vm",
    controller: ['$rootScope', 
        function ($rootScope) {
    	
        var vm = this;

        vm.removeFromWishList = function(slug){
            for(i = 0; i < $rootScope.wishProducts.length; i++){
                if($rootScope.wishProducts[i].slug == slug){
                    $rootScope.wishProducts.splice(i, 1);
                    break;
                }
            }

            // Remove the authenticated user from local storage
            localStorage.removeItem('wishlist');

            // Set the stringified user data into local storage
            localStorage.setItem('wishlist', JSON.stringify($rootScope.wishProducts));
        }
    }],
});