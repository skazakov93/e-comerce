angularAppStat.component("fullWishList", {
    templateUrl: "../views/partials/fullWishList.component.html",
    controllerAs: "vm",
    controller: ['$rootScope',
    	function ($rootScope) {
    	
    	var vm = this;

        // Grab the user from local storage and parse it to an object
        var localWishList = JSON.parse(localStorage.getItem('wishlist'));
        
        if(localWishList){
            $rootScope.wishProducts = localWishList;
        }
        else{
            $rootScope.wishProducts = [];
        }
        
	    //za testiranje
		console.log("component fullWishList");
		console.log(vm.categorySlug);
    }],
});