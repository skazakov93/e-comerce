angularAppStat.component("registerUser", {
    templateUrl: "../views/partials/registerUser.component.html",
    controllerAs: "vm",
    controller: ['userService', '$state',
    	function (userService, $state) {
    	
    	var vm = this;

    	vm.firstName = "";
    	vm.lastName = "";
    	vm.email = "";
    	vm.password = "";
    	vm.passwordConfirmation = "";
    	vm.phoneNum = "";
    	vm.photo = null;

    	vm.register = function(){
            var payload = new FormData();

            payload.append("firstName", vm.firstName);
            payload.append("lastName", vm.lastName);
            payload.append("email", vm.email);
            payload.append("password", vm.password);
            payload.append("password_confirmation", vm.passwordConfirmation);
            payload.append("phone_number", vm.phoneNum);

            if(vm.photo){
                payload.append('photo', vm.photo);
            }

    		return userService.registerUser(payload).then(function(data) {
	            if(data != -1){
                    $state.go('index');
                }
	        });
    	}
    }],
});